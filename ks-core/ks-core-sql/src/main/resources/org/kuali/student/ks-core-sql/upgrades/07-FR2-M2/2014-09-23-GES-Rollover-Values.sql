-- KSENROLL-14987
-- instructor include
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.instructorinformation.include', 'true', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '83c0f5fd-eeb3-418b-841f-65df7d9848e5', 200, 0, '0d58db7f-de33-4ab1-8d4a-7d8d9a7fce2b', null, null, null, null, 'admin', TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.instructorinformation.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '5ebc6f9a-8544-45f7-b831-a27c670f4a11', 300, 0, null, null, 'ENGL', null, null, 'admin', TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ))
/

-- cancelled AO include
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.activityofferingsstateofcancelled.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '36b4e369-18bc-4059-9389-7d6272c52101', 200, 0, '0d58db7f-de33-4ab1-8d4a-7d8d9a7fce2b', null, null, null, null, 'admin', TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.activityofferingsstateofcancelled.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '386d7b2a-d84c-4076-93c1-914fceeb2094', 200, 0, '8149aa68-9bfa-4d95-bcfe-b17b2d36d763', null, null, null, null, 'admin', TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.activityofferingsstateofcancelled.include', 'true', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', 'abba4c08-93d4-42a4-af8d-b3904ed8d522', 300, 0, null, null, 'ENGL', null, null, 'admin', TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ))
/

-- scheduling information include
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.allschedulinginformation.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', 'a67ac610-6fdb-496f-8f0a-e0a5fc6d7e61', 200, 0, '0d58db7f-de33-4ab1-8d4a-7d8d9a7fce2b', null, null, null, null, 'admin', TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.allschedulinginformation.include', 'true', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '98d4f4d4-0587-48dd-a40c-61bde1ece3b5', 200, 0, '8149aa68-9bfa-4d95-bcfe-b17b2d36d763', null, null, null, null, 'admin', TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140923000000', 'YYYYMMDDHH24MISS' ))
/

-- KSENROLL-15013 (GES values for term type kuali.atp.type.Summer1)
-- instructor include
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.instructorinformation.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '81496b32-2438-4144-b746-33f1a1151e58', 600, 0, null, null, null, null, 'kuali.atp.type.Summer1', 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.instructorinformation.include', 'true', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '9e3973de-80b1-401d-9b2e-1d0efc05e733', 300, 0, null, null, 'ENGL', null, 'kuali.atp.type.Summer1', 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/

-- cancelled AO include
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.activityofferingsstateofcancelled.include', 'true', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', 'ad1d3a4a-dd10-4868-8d9e-610cc8c4ef6c', 600, 0, null, null, null, null, 'kuali.atp.type.Summer1', 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.activityofferingsstateofcancelled.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '42ef166b-8389-41c2-a075-dd8cdd3bb898', 300, 0, null, null, 'ENGL', null, 'kuali.atp.type.Summer1', 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/

-- scheduling information include
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.allschedulinginformation.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '964a89b1-4118-46e7-9ed8-8d7e68321773', 600, 0, null, null, null, null, 'kuali.atp.type.Summer1', 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.allschedulinginformation.include', 'true', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', 'b08c498c-21db-40a9-9d64-c87f12fb84eb', 200, 0, '8149aa68-9bfa-4d95-bcfe-b17b2d36d763', null, null, null, 'kuali.atp.type.Summer1', 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/

-- KSENROLL-15019 (GES values for term 202005)
-- instructor include
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.instructorinformation.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '0ca85ade-89c4-4729-9b81-b499a9adcfc6', 200, 0, '8149aa68-9bfa-4d95-bcfe-b17b2d36d763', null, null, 'kuali.atp.2020Summer1', null, 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.instructorinformation.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '64457bc1-fd64-443c-bcfe-b9c9414ec91f', 300, 0, null, null, 'ENGL', 'kuali.atp.2020Summer1', null, 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/

-- cancelled AO include
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.activityofferingsstateofcancelled.include', 'true', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '478ecb0c-86bf-4579-8bb3-8bbe0155e1c7', 200, 0, '8149aa68-9bfa-4d95-bcfe-b17b2d36d763', null, null, 'kuali.atp.2020Summer1', null, 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/

-- scheduling information include
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.allschedulinginformation.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '761828fd-4d1c-45c9-99ea-a19caa5adb47', 200, 0, '8149aa68-9bfa-4d95-bcfe-b17b2d36d763', null, null, 'kuali.atp.2020Summer1', null, 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.allschedulinginformation.include', 'true', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', 'b233a63b-9e69-4d4e-a908-6bec89660c04', 300, 0, null, null, 'CHEM', 'kuali.atp.2020Summer1', null, 'admin', TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20140930000000', 'YYYYMMDDHH24MISS' ))
/


-- KSENROLL-14375
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.roomassignment.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '139c9a97-d874-447d-8ea1-e37e1a5a1f9b', 200, 0, '8149aa68-9bfa-4d95-bcfe-b17b2d36d763', null, null, null, null, 'admin', TO_DATE( '20141001000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20141001000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.roomassignment.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '5f60ccbe-2bed-4544-bd99-aa01ac77c867', 200, 0, 'd1c291df-38c6-4bb7-9c62-fec470897c8d', null, null, null, null, 'admin', TO_DATE( '20141001000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20141001000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.roomassignment.include', 'false', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '06d0c84b-4b22-4a9c-a5a3-db1a5b7640bd', 200, 0, 'CLUID-CHEM231-200408000000', null, null, null, null, 'admin', TO_DATE( '20141001000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20141001000000', 'YYYYMMDDHH24MISS' ))
/
INSERT INTO KSEN_GES_VALUE (GES_PARM_ID, GES_VALUE, GES_VALUE_STATE, GES_VALUE_TYPE, GES_VALUE_TYPE_ID, ID, PRIORITY, VER_NBR, CLU_ID, ORG_ID, SUBJECT_CODE, ATP_ID, ATP_TYPE_KEY, CREATEID, CREATETIME, UPDATEID, UPDATETIME)
  VALUES ('kuali.ges.parameter.key.rollover.roomassignment.include', 'true', 'kuali.ges.value.state.active', 'kuali.ges.value.type', 'BOOLEAN', '3e1fa439-0410-4a17-bf57-03b8c9e0e4ee', 300, 0, null, null, 'ENGL', 'kuali.atp.2020Summer1', null, 'admin', TO_DATE( '20141001000000', 'YYYYMMDDHH24MISS' ),'admin',TO_DATE( '20141001000000', 'YYYYMMDDHH24MISS' ))
/




