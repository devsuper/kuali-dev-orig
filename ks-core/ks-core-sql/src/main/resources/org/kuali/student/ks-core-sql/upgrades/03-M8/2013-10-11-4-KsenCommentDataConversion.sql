DROP TABLE KSCO_RICH_TEXT_T CASCADE CONSTRAINTS PURGE
/
DROP TABLE KSCO_REFERENCE_TYPE_ATTR CASCADE CONSTRAINTS PURGE
/
DROP TABLE KSCO_REFERENCE_TYPE CASCADE CONSTRAINTS PURGE
/
DROP TABLE KSCO_REFERENCE CASCADE CONSTRAINTS PURGE
/
DROP TABLE KSCO_COMMENT_ATTR CASCADE CONSTRAINTS PURGE
/
DROP TABLE KSCO_COMMENT_TYPE_ATTR CASCADE CONSTRAINTS PURGE
/
DROP TABLE KSCO_COMMENT_TYPE CASCADE CONSTRAINTS PURGE
/
DROP TABLE KSCO_COMMENT CASCADE CONSTRAINTS PURGE
/

--DROP the TAG Tables
DROP TABLE KSCO_TAG_TYPE_ATTR CASCADE CONSTRAINTS PURGE
/
DROP TABLE KSCO_TAG_TYPE CASCADE CONSTRAINTS PURGE
/
DROP TABLE KSCO_TAG_ATTR CASCADE CONSTRAINTS PURGE
/
DROP TABLE KSCO_TAG CASCADE CONSTRAINTS PURGE
/