--KSENROLL-14670 Add unmapped holds type data
INSERT INTO KSEN_TYPE (CREATEID,CREATETIME,DESCR_PLAIN,NAME,REF_OBJECT_URI,SERVICE_URI,TYPE_KEY,VER_NBR)
  VALUES ('SYSTEMLOADER',TO_DATE( '20110822', 'YYYYMMDD' ),
  'Indicates that the issue has to do with admission issues',
  'Admission Issue','http://student.kuali.org/wsdl/type/HoldIssueInfo',
  'http://student.kuali.org/wsdl/hold/HoldService','kuali.hold.issue.type.admissions',0)
/
INSERT INTO KSEN_TYPE (CREATEID,CREATETIME,DESCR_PLAIN,NAME,REF_OBJECT_URI,SERVICE_URI,TYPE_KEY,VER_NBR)
  VALUES ('SYSTEMLOADER',TO_DATE( '20110822', 'YYYYMMDD' ),
  'Indicates the student is out of compliance with a financial aid regulation or rule',
  'Financial Aid Issue','http://student.kuali.org/wsdl/type/HoldIssueInfo',
  'http://student.kuali.org/wsdl/hold/HoldService','kuali.hold.issue.type.financial.aid',0)
/
INSERT INTO KSEN_TYPE (CREATEID,CREATETIME,DESCR_PLAIN,NAME,REF_OBJECT_URI,SERVICE_URI,TYPE_KEY,VER_NBR)
  VALUES ('SYSTEMLOADER',TO_DATE( '20110822', 'YYYYMMDD' ),
  'Indicates that the issue has to do with compliance with immigration mandates',
  'International Student Issue','http://student.kuali.org/wsdl/type/HoldIssueInfo',
  'http://student.kuali.org/wsdl/hold/HoldService','kuali.hold.issue.type.international.students',0)
/
INSERT INTO KSEN_TYPE (CREATEID,CREATETIME,DESCR_PLAIN,NAME,REF_OBJECT_URI,SERVICE_URI,TYPE_KEY,VER_NBR)
  VALUES ('SYSTEMLOADER',TO_DATE( '20110822', 'YYYYMMDD' ),
  'Indicates that the issue has to do with compliance with medical immunization mandate',
  'Medical Immunization Issue','http://student.kuali.org/wsdl/type/HoldIssueInfo',
  'http://student.kuali.org/wsdl/hold/HoldService','kuali.hold.issue.type.medical.immunization',0)
/