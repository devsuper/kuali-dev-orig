--  KSENROLL-14636
-- User L.Victorp registered for 201208/CHEM699/1001 (picked 2 credits)
insert into KSEN_LPR (OBJ_ID, VER_NBR, CREATEID, CREATETIME, UPDATEID, UPDATETIME, ATP_ID, COMMIT_PERCT, CREDITS, CROSSLIST, EFF_DT, EXPIR_DT, GRADING_OPT_ID, LUI_ID, MASTER_LPR_ID, PERS_ID, LPR_STATE, LPR_TYPE, ID) values ('4cf1ad9c-5bec-48ed-bdf7-fdb6eda90233', 0, 'L.VICTORP', TIMESTAMP '2014-09-03 07:49:26.539', 'L.VICTORP', TIMESTAMP '2014-09-03 07:49:26.539', 'kuali.atp.2012Fall', '', '2.0', '', TIMESTAMP '2014-09-03 00:00:00.0', '', 'kuali.resultComponent.grade.letter', '0691bb96-4254-425f-88cc-c223bd659d44', '9a8b06b1-dd9a-47d5-85c5-42ec409842d5', 'KS-4993', 'kuali.lpr.state.active', 'kuali.lpr.type.registrant.registration.group', '9a8b06b1-dd9a-47d5-85c5-42ec409842d5')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('9a8b06b1-dd9a-47d5-85c5-42ec409842d5', 'kuali.resultComponent.grade.letter')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('9a8b06b1-dd9a-47d5-85c5-42ec409842d5', 'kuali.result.value.credit.degree.2.0')
/
insert into KSEN_LPR (OBJ_ID, VER_NBR, CREATEID, CREATETIME, UPDATEID, UPDATETIME, ATP_ID, COMMIT_PERCT, CREDITS, CROSSLIST, EFF_DT, EXPIR_DT, GRADING_OPT_ID, LUI_ID, MASTER_LPR_ID, PERS_ID, LPR_STATE, LPR_TYPE, ID) values ('33bbcb8c-9fe1-4ad4-bccd-54b387909de6', 0, 'L.VICTORP', TIMESTAMP '2014-09-03 07:49:26.539', 'L.VICTORP', TIMESTAMP '2014-09-03 07:49:26.539', 'kuali.atp.2012Fall', '', '2.0', '', TIMESTAMP '2014-09-03 00:00:00.0', '', 'kuali.resultComponent.grade.letter', 'ce30b18d-5003-430c-9b8d-07d320785f4d', '9a8b06b1-dd9a-47d5-85c5-42ec409842d5', 'KS-4993', 'kuali.lpr.state.active', 'kuali.lpr.type.registrant.course.offering', 'f09e7ad7-6001-4fde-866e-150260a040da')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('f09e7ad7-6001-4fde-866e-150260a040da', 'kuali.resultComponent.grade.letter')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('f09e7ad7-6001-4fde-866e-150260a040da', 'kuali.result.value.credit.degree.2.0')
/
insert into KSEN_LPR (OBJ_ID, VER_NBR, CREATEID, CREATETIME, UPDATEID, UPDATETIME, ATP_ID, COMMIT_PERCT, CREDITS, CROSSLIST, EFF_DT, EXPIR_DT, GRADING_OPT_ID, LUI_ID, MASTER_LPR_ID, PERS_ID, LPR_STATE, LPR_TYPE, ID) values ('9d390871-ab79-4486-b63e-3392c104d89d', 0, 'L.VICTORP', TIMESTAMP '2014-09-03 07:49:26.539', 'L.VICTORP', TIMESTAMP '2014-09-03 07:49:26.539', 'kuali.atp.2012Fall', '', '', '', TIMESTAMP '2014-09-03 00:00:00.0', '', '', '47fd1950-ac29-4a8b-99e2-1d2a329df74d', '9a8b06b1-dd9a-47d5-85c5-42ec409842d5', 'KS-4993', 'kuali.lpr.state.active', 'kuali.lpr.type.registrant.activity.offering', 'bc401138-c34a-45a0-977d-d8b8e138afa5')
/

-- R.IANK 201208/CHEM399C/1001 - added 2 credits/Letter
insert into KSEN_LPR (OBJ_ID, VER_NBR, CREATEID, CREATETIME, UPDATEID, UPDATETIME, ATP_ID, COMMIT_PERCT, CREDITS, CROSSLIST, EFF_DT, EXPIR_DT, GRADING_OPT_ID, LUI_ID, MASTER_LPR_ID, PERS_ID, LPR_STATE, LPR_TYPE, ID) values ('f52df6dd-09b2-468b-9bc6-ead728f0688c', 0, 'R.IANK', TIMESTAMP '2014-09-04 09:12:22.041', 'R.IANK', TIMESTAMP '2014-09-04 09:12:22.041', 'kuali.atp.2012Fall', '', '2.0', '', TIMESTAMP '2014-09-04 00:00:00.0', '', 'kuali.resultComponent.grade.letter', '4de87007-82bf-472a-96be-9fbf7f3b5712', '97341559-8dae-4cec-b004-e55aaabd3d61', 'KS-4483', 'kuali.lpr.state.active', 'kuali.lpr.type.registrant.registration.group', '97341559-8dae-4cec-b004-e55aaabd3d61')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('97341559-8dae-4cec-b004-e55aaabd3d61', 'kuali.resultComponent.grade.letter')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('97341559-8dae-4cec-b004-e55aaabd3d61', 'kuali.result.value.credit.degree.2.0')
/
insert into KSEN_LPR (OBJ_ID, VER_NBR, CREATEID, CREATETIME, UPDATEID, UPDATETIME, ATP_ID, COMMIT_PERCT, CREDITS, CROSSLIST, EFF_DT, EXPIR_DT, GRADING_OPT_ID, LUI_ID, MASTER_LPR_ID, PERS_ID, LPR_STATE, LPR_TYPE, ID) values ('971ed7b9-9a3e-486e-acbd-bb8beae1d9bc', 0, 'R.IANK', TIMESTAMP '2014-09-04 09:12:22.041', 'R.IANK', TIMESTAMP '2014-09-04 09:12:22.041', 'kuali.atp.2012Fall', '', '2.0', '', TIMESTAMP '2014-09-04 00:00:00.0', '', 'kuali.resultComponent.grade.letter', '26a79829-b42c-441b-b9cf-0d8f7a35629f', '97341559-8dae-4cec-b004-e55aaabd3d61', 'KS-4483', 'kuali.lpr.state.active', 'kuali.lpr.type.registrant.course.offering', '300c6adc-69f9-410d-b380-ce5805e81f23')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('300c6adc-69f9-410d-b380-ce5805e81f23', 'kuali.resultComponent.grade.letter')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('300c6adc-69f9-410d-b380-ce5805e81f23', 'kuali.result.value.credit.degree.2.0')
/
insert into KSEN_LPR (OBJ_ID, VER_NBR, CREATEID, CREATETIME, UPDATEID, UPDATETIME, ATP_ID, COMMIT_PERCT, CREDITS, CROSSLIST, EFF_DT, EXPIR_DT, GRADING_OPT_ID, LUI_ID, MASTER_LPR_ID, PERS_ID, LPR_STATE, LPR_TYPE, ID) values ('c9637726-4f7d-4df1-9438-626f14782f57', 0, 'R.IANK', TIMESTAMP '2014-09-04 09:12:22.041', 'R.IANK', TIMESTAMP '2014-09-04 09:12:22.041', 'kuali.atp.2012Fall', '', '', '', TIMESTAMP '2014-09-04 00:00:00.0', '', '', '85319f28-22bd-42c1-9572-70f546d30577', '97341559-8dae-4cec-b004-e55aaabd3d61', 'KS-4483', 'kuali.lpr.state.active', 'kuali.lpr.type.registrant.activity.offering', '0a98beda-1e09-4b25-8bd8-9c952ce2bad4')
/

-- R.IANP 201208/CHEM399C/1001 - added 2 credits/Letter
insert into KSEN_LPR (OBJ_ID, VER_NBR, CREATEID, CREATETIME, UPDATEID, UPDATETIME, ATP_ID, COMMIT_PERCT, CREDITS, CROSSLIST, EFF_DT, EXPIR_DT, GRADING_OPT_ID, LUI_ID, MASTER_LPR_ID, PERS_ID, LPR_STATE, LPR_TYPE, ID) values ('71e98869-6715-430d-ac1c-d15d1eba214b', 0, 'R.IANP', TIMESTAMP '2014-09-04 09:19:31.626', 'R.IANP', TIMESTAMP '2014-09-04 09:19:31.626', 'kuali.atp.2012Fall', '', '2.0', '', TIMESTAMP '2014-09-04 00:00:00.0', '', 'kuali.resultComponent.grade.letter', '4de87007-82bf-472a-96be-9fbf7f3b5712', '630fcea6-90d6-474d-baa6-5f37dfef033a', 'KS-8316', 'kuali.lpr.state.active', 'kuali.lpr.type.registrant.registration.group', '630fcea6-90d6-474d-baa6-5f37dfef033a')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('630fcea6-90d6-474d-baa6-5f37dfef033a', 'kuali.resultComponent.grade.letter')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('630fcea6-90d6-474d-baa6-5f37dfef033a', 'kuali.result.value.credit.degree.2.0')
/
insert into KSEN_LPR (OBJ_ID, VER_NBR, CREATEID, CREATETIME, UPDATEID, UPDATETIME, ATP_ID, COMMIT_PERCT, CREDITS, CROSSLIST, EFF_DT, EXPIR_DT, GRADING_OPT_ID, LUI_ID, MASTER_LPR_ID, PERS_ID, LPR_STATE, LPR_TYPE, ID) values ('db866a74-418e-4037-8a20-d6294204f3ae', 0, 'R.IANP', TIMESTAMP '2014-09-04 09:19:31.626', 'R.IANP', TIMESTAMP '2014-09-04 09:19:31.626', 'kuali.atp.2012Fall', '', '2.0', '', TIMESTAMP '2014-09-04 00:00:00.0', '', 'kuali.resultComponent.grade.letter', '26a79829-b42c-441b-b9cf-0d8f7a35629f', '630fcea6-90d6-474d-baa6-5f37dfef033a', 'KS-8316', 'kuali.lpr.state.active', 'kuali.lpr.type.registrant.course.offering', 'b959434f-a59a-450f-a8bf-ad22a477ea19')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('b959434f-a59a-450f-a8bf-ad22a477ea19', 'kuali.resultComponent.grade.letter')
/
insert into KSEN_LPR_RESULT_VAL_GRP (LPR_ID, RESULT_VAL_GRP_ID) values ('b959434f-a59a-450f-a8bf-ad22a477ea19', 'kuali.result.value.credit.degree.2.0')
/
insert into KSEN_LPR (OBJ_ID, VER_NBR, CREATEID, CREATETIME, UPDATEID, UPDATETIME, ATP_ID, COMMIT_PERCT, CREDITS, CROSSLIST, EFF_DT, EXPIR_DT, GRADING_OPT_ID, LUI_ID, MASTER_LPR_ID, PERS_ID, LPR_STATE, LPR_TYPE, ID) values ('e254b9eb-03fa-4672-abd5-05cf74cb4838', 0, 'R.IANP', TIMESTAMP '2014-09-04 09:19:31.626', 'R.IANP', TIMESTAMP '2014-09-04 09:19:31.626', 'kuali.atp.2012Fall', '', '', '', TIMESTAMP '2014-09-04 00:00:00.0', '', '', '85319f28-22bd-42c1-9572-70f546d30577', '630fcea6-90d6-474d-baa6-5f37dfef033a', 'KS-8316', 'kuali.lpr.state.active', 'kuali.lpr.type.registrant.activity.offering', 'd6a140d9-9074-4427-8cc8-4d959cb87a09')
/