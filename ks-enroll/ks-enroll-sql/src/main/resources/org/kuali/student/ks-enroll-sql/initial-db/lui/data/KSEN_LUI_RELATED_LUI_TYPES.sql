TRUNCATE TABLE KSEN_LUI_RELATED_LUI_TYPES DROP STORAGE
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('000de80f-f9dc-4b71-bb84-83a9f28c4883','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('000de80f-f9dc-4b71-bb84-83a9f28c4883','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0034f5ea-f40f-439e-ba2e-2d3eb190a4c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('003c07d6-8cb6-4ceb-9504-e8e5cb8ab218','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('003e4159-b8c5-4943-9906-54a2a4b152bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0092e7fe-99cf-41bb-a3a2-ffe5e0264276','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('009d5bf9-45ff-437c-9dbf-a4cdd0e5ee64','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('00c6fd6e-73a8-4992-b1e5-c115301188e6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('00c8db6c-f4bf-473c-a935-84953b5e0f52','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('00d95f40-9d9e-4624-a24a-10a0f778b046','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('00d9a655-274b-4b89-bf44-d4443a5b6195','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('00d9a655-274b-4b89-bf44-d4443a5b6195','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('00d9a655-274b-4b89-bf44-d4443a5b6195','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('00df7fff-09d9-44c5-ad30-16c34c1a6fd9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('00e0c247-7289-4eb2-910f-f9d4a5b5e09d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('00e2cb75-7429-41df-aec7-44756f430767','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('01113cb8-e695-4587-ae62-e679d24a39e4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('011ff3a5-5469-4a36-b09c-d04f0c00f824','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('011ff3a5-5469-4a36-b09c-d04f0c00f824','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('01305b00-359d-4df0-89cc-5c7c21e9193d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0130a94c-eca0-4126-816c-614d181e8cb7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0157bea6-1d89-41a2-9768-13ae463198de','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0160aa45-62e0-47a8-8023-eb11f8eb5038','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0191db13-39f6-4088-a8ed-eb518da71f44','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0191db13-39f6-4088-a8ed-eb518da71f44','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('01ab0433-8a18-4bb6-a648-102f148b8470','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('01b46bd6-e34f-44e6-8455-cbca53f5e6cb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('01b60ac6-979f-441a-a7c2-88273fee3c70','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('01c2c7dd-db18-4cf4-a81d-b62d59b4982f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('01c57581-a3a2-4acf-be0f-027e7b708c88','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('01e2f6e5-05cf-4541-aa0e-23c8fe6e7bb0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('020f8fce-4382-48c5-9e19-6aeb40da2f2a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('021b50e6-502a-47a7-91bc-8db998e18239','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('023fb84c-4787-4cb0-b80d-b79dceb627dd','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('02604bab-01b7-4070-aef2-578679e82c5f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('02604bab-01b7-4070-aef2-578679e82c5f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('026879d8-d368-46cf-b878-647fccdd3ab5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('028459f9-33f5-4cee-9794-80649e7a5a2c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('028b00d4-9f81-4bfc-b579-33c6dd7a7df0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('02a10511-65c9-4496-9592-3d5cbecdc68d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('02bcb2f8-a876-4c2f-b164-e9d1026496c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('02c26100-ad0f-40e7-9668-092425597e14','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('02c26100-ad0f-40e7-9668-092425597e14','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('02f4f3a7-8818-4c74-a142-c735e0037fb6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('02fd18f3-e592-4a61-8b6e-35251df4ea67','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0320a35a-9855-4f78-a9dd-3de23cf341ed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0345f37c-9be5-40fe-9dad-2caa34b3baa7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('034eea28-6deb-4c84-886f-466562bda5e2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('03676a01-3dea-47fb-8f81-a2623738b8e1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('036830c3-f389-4659-8691-64ac66854b71','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('03924ec6-49b2-401c-9d24-0388edacc4ae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('03b03f82-d7b3-416b-903c-7672c36c9541','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('03dc72e6-966f-46b8-a72d-8a8bb8198f39','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('03dd42fd-1bc9-4297-8567-25f855ae7c2d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('03dd87fa-b896-4a0c-bbf4-7ccb32aa4fa4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0404e2e0-b41a-4f58-9a71-ffd90dda513a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04084725-cf87-4fdd-be3e-924690a426c2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0415bf71-b8a7-49d0-87cd-c1c978c9876b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04209b94-3945-4e46-9778-b4f72aa63840','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('042d7fe7-fd68-411f-aafb-27d41be6541b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04463336-e810-4811-8830-93665a1fa285','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04544dc6-56c9-472e-bbb4-d5063c0e43a7','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04544dc6-56c9-472e-bbb4-d5063c0e43a7','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04544dc6-56c9-472e-bbb4-d5063c0e43a7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0460b3c7-da7e-489c-b5f9-7d74e323d318','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04637c77-89c1-413a-8acc-50a74dd0a1a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04653a55-1f7c-46b2-b7be-2fcf2e16cfdb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0495eaf0-853d-42a9-8276-9335d6e88ebe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04a1f7c5-c01f-4751-99d6-626c7297f9d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04b09503-7391-49c2-88b8-e6665a7b3d9c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04b76f1d-9433-4138-afc4-bf239ca91c91','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04bffab7-45b0-4c60-8dcb-8b9381b1ffbb','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04c7eee6-ba19-4879-9327-0457ba749631','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04ce2612-bdee-41d1-92d6-32ee85c5a5aa','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04ce2612-bdee-41d1-92d6-32ee85c5a5aa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('04f916a5-85f8-48a5-9fff-03ad3a8ff0c1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('055e55f6-47d2-482e-b9d4-7f1ebcb71136','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('05651d91-50d1-494d-ba00-12d4dc142bbf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('05675876-14e9-4fed-9790-9b1f637d70c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('05776ade-14fb-4abd-bd67-4677cfc2c802','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('057cdd11-bb9c-4171-bdf5-2c9db6d7f60a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('057cdd11-bb9c-4171-bdf5-2c9db6d7f60a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0596c69a-af5d-4c6e-a83a-0cc4b6d6732e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('05a1564d-de27-411a-ba96-c6d9054af33c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('05a1564d-de27-411a-ba96-c6d9054af33c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('05adbaac-3af6-4c99-990d-277e3011d6da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('05b5a853-d3c6-48f4-8757-75ba5e75ef5b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('05e4c56e-9fbb-48b8-8d78-37742ba8f970','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('05e4c56e-9fbb-48b8-8d78-37742ba8f970','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('05f1fc07-59c8-4619-8537-060af1f75f33','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('06015c60-bb13-4bd0-9830-30ccf1374402','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('060223cd-c9cb-46ee-831c-782abb97e0a7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0603b2b5-3ee8-4a5b-a013-33d9dcaa201d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0603b2b5-3ee8-4a5b-a013-33d9dcaa201d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('060ef91c-730d-49f6-9c1b-3fea11418ece','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('062c3ec4-489c-409a-8ca2-c081e80be840','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('062c3ec4-489c-409a-8ca2-c081e80be840','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0645cd50-b024-48d8-81a6-1e193686994c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('064874b7-582f-409d-89d3-dde6988a5407','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('064874b7-582f-409d-89d3-dde6988a5407','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('06503796-43c5-4f6c-a3cc-012d569c837f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0657d4d1-4a8b-498f-bb1a-af0ec9e41615','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('065ee914-2fbc-4a6f-b471-f23525b8cd48','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('06750d1a-43a7-4ce8-85ed-619053ab9d7e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('068da3a7-db96-4286-8e1d-7634eab01673','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('06a709f1-4403-478f-b9c9-e9f195b89577','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('06e885ae-8a8b-4147-a324-efc2fc33f1d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0705d188-47cb-471b-90b6-18dc6272496c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('070a16ab-3757-444f-b580-54b1cb019b3c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('072a3b04-1447-4b75-ba7a-a2570fdd8ff5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0751765f-01de-4a17-b66f-4cc2c8a7b984','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('077a0060-b8ee-455f-bd4d-78fe2200918b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('077dda6c-79fb-4af1-9e57-385897e3bf18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0786f0b1-efb3-4b85-b475-ea4ae424c120','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('07963603-96f8-421a-9392-3a45d4e54c63','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0796d8b5-534c-419c-850f-c7e47f346af6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('07989dfd-bdaf-46e9-9de5-214d77f76630','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('07ad4a7a-3c67-47d4-943f-2d5454ce240c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('07ad5a4a-5352-492a-952e-eb0e5bdc7a55','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('07b807aa-b067-4abf-aed0-5f5c5a7094ad','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('07ba261e-d762-4d0a-9946-f44e346eb721','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('07ba261e-d762-4d0a-9946-f44e346eb721','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('07ba7fb6-8bc5-4519-ab31-469370210a28','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('07c9119d-0723-4aab-bc2f-33c91d660738','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('07d6fb9c-f1d0-4606-b346-90735e20cf9d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0802df7d-69e7-466a-866e-06dbaf6334a3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0802df7d-69e7-466a-866e-06dbaf6334a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('08086cf8-e084-4511-939c-f2e8bb75fbed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('080f4cc8-9a24-47a3-93fb-5bf675edb92a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0848320e-1443-4876-936a-2ef65398e8e1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('084e14eb-ba5c-4e17-8b2c-6c2c008a55dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0878d28e-0bf7-4c0c-a61d-719a05a9a769','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('088266a1-c86e-4f4c-aac4-dc8c93c21c89','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('08872f51-8b0b-47bc-b820-709bb1e24441','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('088fc629-5a82-4a8b-82b4-122e15fa7d1e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('08a6fc1e-f3fa-4b70-b4f6-e2ee47e88c25','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('08b2d7d3-85a6-4526-b3a7-9cb32e10cb98','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('08e77568-b854-4510-91ff-866ea1eb74e9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('08e7c583-d22d-4a91-9301-839a44634a7b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('08f62a4f-0907-4e2d-bee3-00f8e45aec2a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('09400ced-1ce0-4baf-8bca-331b69b35e40','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0958c64d-f365-46ac-adf9-5da3b1719e98','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0981c187-821f-4892-b3d9-b9e279d018b6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('09974685-3345-4a14-8055-1823e38eff23','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('09b4cb3c-4857-4aa6-8a77-49f48b5aacf4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('09bf8963-2918-4ba2-9322-7ea9d08e3bc2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('09c3e1ca-a341-4f2d-af71-7a807f80bb5b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('09e02fef-3e34-417d-8c3f-6775ef67f72c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('09fc55e2-51ff-4861-a3e2-daa23854b0e3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0a4a74d6-04ea-4fa9-bdd1-c78f88e5f55b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0a50549c-5ffc-4fa0-9c20-88c0955d1c50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0a7a4c09-b368-488d-b464-3d8c4636b365','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0a7e853c-1261-4342-b290-30b7886b0729','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0aa4c7f7-7043-4cbc-8116-572d35b64c3d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0aa8b9d5-4b76-4a90-b7cf-ddc08079426f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0ab9641a-2128-4c7f-8d6a-0c9fff71e971','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0abe91e7-1cfe-4949-b93f-bcdbb4f537d5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0ac1a8ed-4bda-4bda-9685-d6336262bd70','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0aca1866-ff7c-4038-97b3-5d99355d25b6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0ada2995-96cf-4bd4-ad17-a8fd1d3869df','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b029101-6ed9-4c45-94f9-58b2433d7824','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b0978ee-6080-43b7-8d79-6677e54b2959','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b0c60c3-1f9d-4ef6-bd8d-2c7b6ccdffcd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b1fe7ba-dbce-4c09-a835-14ceaae871af','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b2179d9-0fc7-4a91-b242-a7ac1f9e4154','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b2b0f57-5f9e-4b8f-a954-635d5fb123e1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b35632f-dc53-47d7-9974-adbe1af3a4b4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b465350-4825-42b0-9668-b4fafd9a78e1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b4a6e71-42d2-4e95-b433-b56a51c07524','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b97afca-2b36-42a5-8b36-ec0d8908bfa8','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0b99f6a6-d00b-40b6-9daa-5220251e7a65','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0ba966b6-1af4-444e-a75e-cd5939e3dbd3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0bb24492-3e37-4662-95f5-46ae287ec7fd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0bf43c13-28d1-470b-a68b-fe1237927e8e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c164262-6206-4b2f-8044-832cf55b8117','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c1b9be9-691b-41ed-9696-e71c57baa044','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c20f3e9-59c0-4180-8c85-07ffd15b667f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c338717-e046-4acd-8f31-940e4cfe615e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c3546a1-8983-4a6a-8947-e3e2c9803d09','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c5024fa-19e8-47ef-9d7c-691630e23ba2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c5024fa-19e8-47ef-9d7c-691630e23ba2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c6ae676-5a4d-420e-8b36-e2d50a94ac48','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c71767d-0031-4dab-9c81-c1bc80378daa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c8b9f01-e103-4ef4-baee-b8c85268228c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c8d880a-0ded-4607-8d4b-23fba2a111ca','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c8d880a-0ded-4607-8d4b-23fba2a111ca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c920d95-d36b-4961-87f7-56eb18c1d91b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0c920d95-d36b-4961-87f7-56eb18c1d91b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0cd5795a-2cd8-42f1-8472-494732d818b6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0cfe4bff-ac46-44b7-b50b-5dd6ab0b132b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d034b6f-5d71-4a25-b64f-1247c59925d6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d034b6f-5d71-4a25-b64f-1247c59925d6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d11c3e9-5f64-4762-b9fa-88da278324c0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d1aa87e-3485-457c-8398-09ee14e5c995','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d385252-84d8-4fe3-899b-6080a59d7e46','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d385252-84d8-4fe3-899b-6080a59d7e46','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d48adc9-156d-4eb0-8a10-bd58375a1686','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d549f09-f35b-4da9-a38c-338eeedaf81f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d549f09-f35b-4da9-a38c-338eeedaf81f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d6c0244-49c9-4863-a7e0-4addd0290e78','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d8c77b3-f63b-4420-af2e-85a2b7098025','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d8c77b3-f63b-4420-af2e-85a2b7098025','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d8c77b3-f63b-4420-af2e-85a2b7098025','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0d8fc804-02d9-4382-86b6-a4163153b555','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0dca22ef-aafc-4a98-99d9-287ed174fa12','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0dca22ef-aafc-4a98-99d9-287ed174fa12','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0dccd980-12fa-48ca-8811-8bce7948cac5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0dd63818-6b77-4285-bdf4-36dd5d7cc4ff','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0dd6e654-53ec-4b76-b4a5-10210160cca5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0ddb14cd-7124-46d6-bc05-514d668a98b1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0de5650e-8f3b-4be4-b699-bbf8951006ec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0dec5bcf-72a1-4a01-a8d5-617f7c7d9786','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0df22733-135d-447f-bfff-1ba515360e69','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0df5d934-b69a-473a-b824-d7d55e6c45c3','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0df5d934-b69a-473a-b824-d7d55e6c45c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e144692-1482-402d-94a7-101fffd822d2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e1d2689-2459-4c25-b22e-564f076e9408','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e1f42fa-2052-4e9b-afd1-7f1e6b4ee1a1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e1f42fa-2052-4e9b-afd1-7f1e6b4ee1a1','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e1f42fa-2052-4e9b-afd1-7f1e6b4ee1a1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e2f1da6-a1d0-456d-b1a6-e8c8f163f8bb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e3cf2db-1b7e-45ec-ab9e-617228a0d242','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e60cfe5-fce5-4de2-8245-2f5527a1e911','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e7a76e2-2464-4ff4-b351-3b3f1ea16104','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e7ae594-9c97-4822-a52a-dc3d0049e521','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0e8d9dd3-e711-4a7a-b075-9d17b1cb066a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0ea928d9-a430-4045-ae21-e3e1634ab4a7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0eb019a6-ca5f-494d-81dd-94e4ac4320b5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0eb019a6-ca5f-494d-81dd-94e4ac4320b5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0ece6a36-d290-4cd7-818b-9dcff3968b1c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0eecb73a-ff54-4393-933f-709c24771aaa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0eeecdb0-4bbb-4815-99e0-a0eea563bc48','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0f1ce703-64b5-431b-b841-1c12a12c401b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0f3bb1c8-00da-4c79-92c6-1072e370bd42','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0f4652aa-03b0-4ac8-9a4b-3b371455500c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0f4bcac9-fb28-4cd5-bc77-289abee87b2c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0f528f47-c74b-4a14-9d37-4d7469233d1b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0f528f47-c74b-4a14-9d37-4d7469233d1b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0f6f24d2-1ebc-41c1-afe7-d6c671b26b75','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0f7be346-24a4-4342-8934-12cac46bd044','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0f9c6218-e1a5-47a4-a55b-f923ebe7d87a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fac24a9-c145-4376-a2aa-ac8e42f6b126','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fc74878-313b-4ed2-a329-95807f0c327e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fcacaca-0dd0-4920-8726-9d00321cbe67','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fcacaca-0dd0-4920-8726-9d00321cbe67','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fccc805-73f3-4db3-b37b-69b64eab38bb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fd0ebbd-c807-4668-91f9-bc5a593e409d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fd0ebbd-c807-4668-91f9-bc5a593e409d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fd0ebbd-c807-4668-91f9-bc5a593e409d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fd6edf2-2923-4201-bd33-b69dc90a9d50','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fd6edf2-2923-4201-bd33-b69dc90a9d50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fd87f1d-19ac-4727-8b42-03cbb59808f4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('0fff17b9-4554-48e7-9202-97367da4c3fb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1000ab2c-e6ad-4f2a-961d-aec39fd8d37c','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1000ab2c-e6ad-4f2a-961d-aec39fd8d37c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1039c90c-4047-4634-8b5a-34ab5e90b921','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('10446eb5-6afa-4a98-9cfc-b3d93bb512da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1053c32c-bec0-4660-8104-3975138222f3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1069964b-52e8-43c3-9176-423904389651','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1086e373-0d0c-49e0-9f7d-a87f120b612d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('108d043f-1ab8-47dd-a723-61e36d0ff0a8','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('108d043f-1ab8-47dd-a723-61e36d0ff0a8','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('108d043f-1ab8-47dd-a723-61e36d0ff0a8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('109d5be5-1489-4e4b-ad6f-6104069f9766','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('109d5be5-1489-4e4b-ad6f-6104069f9766','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('10a6cf7c-fab4-4644-acc3-34803f6e0e9a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('10af823f-a59a-4c54-aef7-99739d088a90','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('10bd365c-5a6a-4462-b0e4-b075dc6c49c7','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('10bd365c-5a6a-4462-b0e4-b075dc6c49c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1117ab0c-e88c-46e0-91f6-808107bd27f2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('111986b4-da25-4a38-80e0-d14e789e6244','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1124e5db-6ee8-4a21-a550-28198b4bbeda','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1124e5db-6ee8-4a21-a550-28198b4bbeda','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('112c85ce-a8fa-474b-8118-866930aec0ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('112de102-b70a-4b84-a182-898f0b268213','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('112e2157-e828-4171-8d13-2d532e1da1e0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('11313890-ca68-4257-a6b1-552d1337d78d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('113218b0-64d5-4267-9ca8-bfc6cb717e9e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('113218b0-64d5-4267-9ca8-bfc6cb717e9e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1135da44-bc49-4f93-925c-4975f862425d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('114b9dec-039a-4b2b-9e68-7c52ba21a687','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('114b9dec-039a-4b2b-9e68-7c52ba21a687','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1169e74c-c702-49bb-84fc-8cbc55c91ce7','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1169e74c-c702-49bb-84fc-8cbc55c91ce7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('116e3fc1-470b-4ce8-ab8f-a5fb1f0594a4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('11a80477-df70-4cfe-a1a5-d54983887eb7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('11e39989-eaf2-467c-add8-4a917a592283','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('12000f76-9a0a-40ac-b3c8-20c075522b71','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('120369b7-20ed-4aab-a3c5-24fac4913948','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('12055e5d-b941-4fb9-b453-b820c977799f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('121d9ae1-7095-4d1c-aba5-79fa193ce4a2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('123f4434-9c2a-4348-a3c0-5d95cb807771','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1277cfd4-f932-4090-83fa-651328cdb377','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1286a672-3f6a-4366-a45a-45108755a412','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('12b0cfc3-d7e5-4185-8896-f3e8444c50bf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('12b8f958-a922-4ffa-b3b8-f15d90b41588','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('12c37695-fb73-454b-a020-898be37bc8f8','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('12c37695-fb73-454b-a020-898be37bc8f8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('12cd4e1a-c879-47a3-bcd8-160d23b23c00','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('12cd4e1a-c879-47a3-bcd8-160d23b23c00','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('12d29972-6145-4f71-b097-52e875a9719c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('12e05215-aa96-4709-ac3e-baf1bb890e76','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('13087dad-149f-467a-9a64-5376a30ddfe8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('133097eb-8416-475d-85f1-3333459eaf47','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('133097eb-8416-475d-85f1-3333459eaf47','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('134f1cf5-94b1-43c2-bfa3-69ae80b92af8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1353c214-866b-4d7b-ba8c-137d06143eca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1379fea9-928e-42f2-ade8-b164733ee676','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('138ac01d-94a6-4bf5-8dd4-0f32c9401d75','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('138f5cd4-7e12-4d1e-80e3-401a783399e1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1394a6c5-e5b5-4243-a7fa-d3d1808a3c69','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('13967598-87e4-459a-95bd-990a2f39f34f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('13a89447-4b52-459b-9096-7b35f3308221','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('13ab0cfa-e5a3-45cb-95ea-1882d9fbef2d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('13b21abe-dc4a-4e79-a27e-8529c89d5a24','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('13c2db4c-a0e4-4160-9bdf-2638a9d399ac','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('13c98ac0-3960-4c39-943d-3a4171c3b68f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('13da2c47-9049-4046-a59e-eb6e542709d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('13f3c3ea-6a83-494d-b676-8cd517fc0b09','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('143987b6-d4b6-4336-8035-e4611148d8e6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('14488f70-49bb-4500-924f-2970b69408bb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('144ade63-94fc-4f18-b284-364f7754ef5b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('144b4023-56cc-45b7-9e51-06b14ca81b27','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1478ac0e-d379-4578-9034-5124a26fcec9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1480b242-c048-4239-9a1e-ce8cee2352a2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('14aa598d-8988-47f4-be80-e2e07dcae60a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('14cc4b46-c5a1-49ff-87c5-b0f59786beb0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('14f19860-9549-4c03-a96c-077c2e25fcba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('14f68a6a-9565-4b18-b037-75f626e2abf7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('14f7db15-a80c-4a41-974d-9434a08efdc9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('14f7db15-a80c-4a41-974d-9434a08efdc9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('152ac4b6-e487-4a67-9d4d-d599a87b747a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1533937c-8def-48a4-a785-9fcb02420299','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1534c91d-263c-4be1-ab31-5aa9b6b81e4d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1549ced8-a15b-4301-92fd-3ad6272ea173','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('154a666d-1a04-478d-8278-43e88cfc554d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('155bbe53-e7ad-4d76-8d10-1d98005c8505','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1593c99d-68b0-45c3-8c5e-6e7bc4f37e2a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('15abdcd9-d38c-4757-a462-3216ed5a021a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('15bd2468-a775-40bf-a4e4-938612cf0dc6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('15bd2468-a775-40bf-a4e4-938612cf0dc6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('15c0445a-551e-4061-a379-05a216fc68bd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('15de4e9e-a191-4ec4-95a8-70bbc43a90e6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('15ffeb96-a15a-4d1b-b6b3-413a49573752','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1643db8a-9c33-46c2-9a2e-845d069d8899','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('16452a25-75d8-446e-8c32-799e9a1ec787','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('166fca54-8012-467d-9fad-6697827448dc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('166fca54-8012-467d-9fad-6697827448dc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1674005a-4f62-4814-b2b8-9be5df9718f6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('167b7508-ae8a-42e2-87f2-7b779dc48110','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('167c485b-e984-46fd-ab5f-3c2ad3e1b281','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('167e50ac-0014-4967-a091-17e1893c1be4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('168425dc-5a8c-4bba-a898-8c163f5bc986','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('168a366c-0902-436e-82e9-fdce137ccf6d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('168c9376-527b-4959-96cb-c2d8f6410223','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('168c9376-527b-4959-96cb-c2d8f6410223','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('16a1dab7-0ff7-412d-8c97-2546d41a5cbb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('16adb743-c94f-4483-ba6d-d24c8dd81623','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('16b09010-55d9-4738-bc00-d6ce71deefd6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('16b394a1-c9cc-4344-8a08-b2e0f1e9345d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('16c4c936-3d67-4bcb-a069-2aa34482d120','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('16c80685-08f4-4e57-b1db-d9e90a16696a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('16f9a41c-e43f-4994-8254-41765777f221','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('171dbb3d-2fbb-4e0f-9fb8-3e18edcbf0fa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('172d1b29-461a-4bed-b043-d52cfd411bbe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1730eda4-febe-4a01-82f8-207d86eaa654','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('177e6097-6e81-4d8a-bf0a-2482a67d123a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('178d1470-7501-49b6-917b-fc5427285ffc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('178d1470-7501-49b6-917b-fc5427285ffc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('17b539cc-0174-45ed-b282-efa61a4c85ef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('17ee79d0-8c0d-4da0-b2a9-5e91a220841e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('17fec6f5-14a7-438a-b5e3-8e0148ef27f7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('181cdc36-7967-4220-9bf4-0b01dc3ae75b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('18261deb-e251-4f33-8f6a-d5a14d77f375','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('18399f2c-9dc4-45e4-aa17-87f57f7ec66d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1846e154-44df-49fa-afcc-2fc49d10acd1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1846e154-44df-49fa-afcc-2fc49d10acd1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('185d1613-2e86-43a1-b3c5-f7786dfc3c07','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('185d1613-2e86-43a1-b3c5-f7786dfc3c07','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('18abc49c-1e5e-4368-9895-4ed2f81b1a98','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('18ae3793-989c-49f2-8d8a-8f9abd4761d6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('18b70482-0c91-4cac-aeed-8045cabc77f1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('18c715dd-c8bf-44a3-8e2a-1995719c1cfa','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('18cb5959-5f4a-44b4-ba07-1b6a6403098c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('18dcfe2f-3dcb-4ea8-972f-a25e8dd34caf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1903436f-b247-486e-bdc2-984befc8fc50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('19101f3c-14b2-4f27-a6ef-46d1d8727a55','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('191e2121-de21-4f7a-a7c8-ba6fe877a8da','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('192ba01e-1c36-4e0e-b84a-0c44b8c4f675','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('192fad34-169c-4ae3-ac5e-2bf02741f690','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1936a404-2391-44d6-8d84-0a95fc644de1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('194d8a1b-60ec-420f-a468-441a5e592129','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1956950a-af2f-4902-b281-d7f6acaf733a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('195954b3-18c5-423d-9fa8-351400619ffa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('196fcb50-a2ab-45e2-861a-bcee9b1e1c71','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('19741a44-3421-48b9-a0c2-5b308c098ff2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('19adadc7-122c-4736-a269-c22ec589bbd1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('19c0c665-703e-4418-ab97-2cd1f7431354','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('19c51ec2-535a-4dd5-97ee-23e016b9b37d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('19e94f21-6051-48d3-a4e7-377e83dc23be','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1a16a5a9-0703-4301-9fb6-9114b9267d98','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1a19dfca-9b50-4dea-8d7b-bca8524aaffb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1a2da56a-0b22-43e3-80d9-fa0fd3676bdc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1a80eb16-c2db-4bcd-9344-f2072bf37290','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1a80eb16-c2db-4bcd-9344-f2072bf37290','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1a91d8cb-d9dd-4acf-815f-e3160310ac36','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1a994a2a-0ceb-41fb-b9f0-d88e07fb540e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ab411b0-9d3a-4a7a-968b-d17c759e4e90','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ac39500-c326-428a-b09a-59e13a842af6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ac71366-9843-4a73-b4f4-67e6ac44fdad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ac8f656-29c2-4caf-87df-e31257f58edc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1acba701-aef8-47f3-9457-fdd5e3076080','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1acba701-aef8-47f3-9457-fdd5e3076080','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ade06c0-6159-46aa-8f22-772146a7401b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ae255a4-984b-4b90-8026-a20e8ebdc9ea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1aec7b76-2cdc-40c5-bf4d-bd1397f296a4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1aec7b76-2cdc-40c5-bf4d-bd1397f296a4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b0f0e38-1b6e-4211-9d5c-c6f39b5fadba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b16c57f-f5ec-4a8c-9c7b-de0cb1fe6d4e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b4335e4-b807-4161-8a8f-e698ad7b0aa6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b439125-9224-40c7-afd6-726f0eb02819','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b439125-9224-40c7-afd6-726f0eb02819','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b52eb43-a910-47f3-a5ad-44f7de1d3dab','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b52eb43-a910-47f3-a5ad-44f7de1d3dab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b5fd680-5736-4e18-a007-49d3e6ee42c8','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b5fd680-5736-4e18-a007-49d3e6ee42c8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b63fc72-4f0b-4a43-a650-3b7260382f50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b6a573b-e691-4c87-9897-8324666ced89','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b6a573b-e691-4c87-9897-8324666ced89','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b71dd1f-b8f2-456f-b736-576083bb8efa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b8f77a7-85db-438f-ae10-581a672439f2','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b8f77a7-85db-438f-ae10-581a672439f2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1b988a76-2043-4ba0-abae-fd0a6da9312e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ba6e78a-ecde-4157-8d62-f85d13c43e7e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1bab7c19-661f-4270-8501-7fd1d3c5aa33','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1bb4115f-66f1-4e71-ad0e-3f4e77020031','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1bb99b04-6a58-4923-9af5-ed4c498ad0ec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1bbe36f6-aa9e-40cf-bf4a-0cbcafcbedcf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1bcb7eb0-8950-442f-ae85-6968ed662b19','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1bd56a7d-6b26-40bc-bcea-3b2aafe8d4bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1bee889c-acb6-410f-89ed-992cd6168ac8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1bef6929-4f28-43c1-bc74-4fa4909931f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1bfaa2ee-5513-4425-8fb0-85cd65d6c351','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1bfd1387-20b8-4587-8678-37140b04f48f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1c094727-7670-4e30-91ae-b49dc90781f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1c12f1c3-4741-48cb-8564-b201e02964a9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1c1d390f-d3e8-44f5-aa0e-57a58c2a7c39','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1c1fc637-d883-4ed5-8a05-a526bedf8493','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1c228637-7d42-4c03-ba6b-0137b8e3bf94','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1c3937bf-968a-47e0-8b64-f2572d3cf727','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1c3c9ce9-a175-4c1f-a0eb-23c48bbcea6e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1c43d093-8850-4c5a-9729-edcaa34f282b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1c628e9f-0d3f-465a-aa81-6053a26d9891','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1c6d43e5-cbcc-4f52-a7c0-2a6117dfd7af','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1cab145e-4669-4ca8-857c-a6dfba09d8c0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1cb4843f-2255-4b33-9ead-740855ed857a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1cbc5934-0d09-4e36-83c0-c5a4c572dbc2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1cbdb133-af2b-49a3-9317-d7554ce0c2df','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1cdbac59-6738-4e12-a8fd-a94cb0010401','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1cdbac59-6738-4e12-a8fd-a94cb0010401','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1cdbd4ff-188b-4086-9c2d-dbbad1facb95','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1cdbd4ff-188b-4086-9c2d-dbbad1facb95','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ce08333-a007-42f7-9675-d775931519bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ce50754-46ee-45f6-bfc2-fe2e458a3279','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ce50754-46ee-45f6-bfc2-fe2e458a3279','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1cee53c6-588b-4de5-b76e-ef4bd46c8563','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d06b3c2-79b4-46f2-a3b8-624ac0806c5e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d0e4481-fa56-4eff-a9bb-03dc441f54c5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d1179cd-bf7b-4350-8530-89479f1566cd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d1f510c-dd8a-49b2-8982-5482ae6804d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d365f1d-3bb7-40db-8e30-19d4629bb0c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d3bbd56-ebae-4e17-a353-b2d75eb6eb49','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d4569f4-e7d2-43ef-8640-16265e44acce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d6becb7-056d-406f-846f-ac9bef40f0ad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d6fbcde-2504-47e0-a505-3dab5771bc6d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d739d7a-83c5-4164-acf7-a4c66b15282c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d817dce-2d42-4770-9255-0df698d18fe2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d8ef808-6d88-4b5b-be73-8e629c5176bb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1d97ead5-e0a1-425e-8204-b93a5c878bdd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1db26f93-aa0a-49d0-84a7-b754c551a948','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1db545bb-31b0-4e24-9d57-8aa52cd9f8f3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1df3f432-fe5d-4108-be4d-f6f2c6a727f2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1dfdfd76-1b5a-4eeb-8c7f-284b4d51206e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1dfdfd76-1b5a-4eeb-8c7f-284b4d51206e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1e0950a3-9029-4a42-8079-ba53e52a1708','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1e098117-e53d-4a74-9572-1d2f23d71a31','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1e0c353a-eb0f-4969-a6ca-f58433232949','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1e0df0c2-3c52-47f0-bfd3-48fb677f6755','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1e366d9b-6ef8-459d-afcc-5d518174155b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1e366d9b-6ef8-459d-afcc-5d518174155b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1e3fd8d3-5f20-4287-b5dd-5b8a73e92614','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1e762ec0-2bb1-4d63-9f16-aabe598e5635','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1e8f6bf6-40f7-49d5-8252-66d0869ac87b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ea9490a-d33e-4ba5-a081-afc0b04c1405','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1eaf7835-79df-4999-aa66-366179fb7749','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1ec5e73a-82e2-4ee4-a13b-69b5e5bee005','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1eca9eca-2ec2-450c-a8cd-765b61055be0','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1eca9eca-2ec2-450c-a8cd-765b61055be0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1f035805-9fc3-476e-9fff-b763e210f8f0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1f158cea-fd09-44b2-92ba-72ce77feca40','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1f4b60a5-79f8-4b74-bd8c-a908737145b1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1f7b5946-5432-4ead-8fb0-5344ac81b27f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1f7d51d0-9cbc-4edb-aaaf-0342e3d207c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1f8a0af3-718f-488e-a62e-40b8db709367','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1fa228d3-96d7-4947-afd5-d1dddcdfcc3d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1fbc3eba-1705-4de9-9ff6-bbf196315454','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1fbcc295-a557-4584-a9a9-f0fb70b67992','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1fc44e9b-f60a-4ee0-8056-ef18372a4869','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1fc44e9b-f60a-4ee0-8056-ef18372a4869','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1fd1b8f5-c635-4890-9f08-4be516b5b1f6','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1fd1b8f5-c635-4890-9f08-4be516b5b1f6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('1fd275f8-b863-49c6-8cb6-0e013575e995','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2007883b-77df-4259-9f96-3c7fcb2d8ef7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('201539d1-0394-44f5-ac4d-76f799036d72','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('20184537-8df7-4042-81c9-e1ce4b615950','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('20184537-8df7-4042-81c9-e1ce4b615950','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('20227312-946b-4190-a7c5-da47f577a757','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('20520e00-37aa-4148-8858-e885cac819b3','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('20729bff-f68a-4c2b-9345-99b207087ed8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('207e6e4f-4746-4535-b92b-a96b3ef6e7c8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('208c1108-d145-4399-965c-ba6e3d675eca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2093cb89-d558-4bd0-bf70-b4583f156cdd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('20b1f571-6533-44f1-b189-3f2a7d2250fb','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('20b1f571-6533-44f1-b189-3f2a7d2250fb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('20c3761c-b325-4c34-9c33-e53e4efd21c0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('20fd5379-a18b-47b0-bf4e-e93dd469bc33','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('20fd5379-a18b-47b0-bf4e-e93dd469bc33','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('21087f7e-b310-40b0-a012-6b585b67610d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('21087f7e-b310-40b0-a012-6b585b67610d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('210e0fde-5546-4282-8c65-3a4a49ce1283','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2127bd53-b18d-4161-a0e1-bb59d09328a8','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2127bd53-b18d-4161-a0e1-bb59d09328a8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('218a705c-6908-4b80-a58a-5f8d53c9886b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('219c6389-9d21-4a88-97b6-1b27202aa8a5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('21a44e79-ec45-414e-9f1b-d0a4cab00c94','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('21c80ac9-3d77-475b-9605-9be3c928eaf6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('21d570a8-129a-4ddd-9942-44626ac83c74','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('21e55cd8-cda1-4e5d-b8af-bdc3f7f8aed9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('21f9450f-c798-4d05-a873-f66fb5f2375f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2205307a-9e1e-457e-9004-32cf7119884d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('220f104e-f171-472a-8b02-a0f403d55907','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2217afac-049b-4986-8c91-3719c60ca464','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2217afac-049b-4986-8c91-3719c60ca464','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2255a6d0-ec25-474b-a796-1bb640d5540e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22602a5d-9f02-4a7c-af7c-b5010ab1a00b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2267a571-cf38-4892-a1dc-93a3d122d742','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22779753-40a5-4ceb-8ea0-572d79f974a1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('227e53eb-244c-4979-a1e2-bdcb6883f838','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('228a9e0c-daf1-4dfe-bf24-d0fb07b3a5a6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('228b6819-ceee-43fa-9760-f472a880e887','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22981a95-6aff-4e2c-9ab7-1cfef4013e92','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22afb17f-c257-413a-9f7a-b5476b1d5b1e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22d4e8ff-e801-4546-8848-893fb9d83618','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22d712b4-b406-4c98-a50d-f91ac918c04a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22ebd21b-c286-4fcb-9e8d-4e7830e163e9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22ec55b9-b4a4-491c-a240-31490417a8ef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22f605e7-0f48-418a-919d-ea419bca61da','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22f605e7-0f48-418a-919d-ea419bca61da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('22faae60-e82f-46b5-8461-9e163ea3d5f8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2317765b-6728-4692-8300-c84249c0d051','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('23240a5f-246e-48e1-aa86-b210ff6a3977','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('232eab43-4a47-4dee-87af-f9b2f08d2dee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('232fa8a2-2ad4-4771-b65d-8b3b9cf29424','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('23318a54-b53b-4371-9e56-b560cfdf51a7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2354645e-7457-4905-b7fc-cfa697ba1105','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2356b53a-b88d-471d-adaf-46ce5f3aca17','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('235a44a7-79ea-4559-b926-1878bc8fc6a0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('236624cb-2945-4b07-ac55-ab8ec30e7cf9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('236c8653-8619-4af8-a635-399ef4d4f884','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('236c8653-8619-4af8-a635-399ef4d4f884','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('238ce306-2667-4a32-99b5-d89bc28b4c60','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('23cb086c-7f99-4664-8ba4-b5eb7596e244','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('23d627a2-4cc8-4021-93f8-e51457b3efef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('23e8925a-4660-41a7-a8df-27e3227d2321','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('23f6b3e4-1714-442c-910e-78e6a2de71b2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('23f6b3e4-1714-442c-910e-78e6a2de71b2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('241a6c89-48c3-48b0-a341-367a8083aa6a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('244756d6-a556-4039-90d1-b7ccd286effe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24594cb2-9eb7-455e-ac6f-7a95b3a4b0de','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24594cb2-9eb7-455e-ac6f-7a95b3a4b0de','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('245d86e4-c0f5-4a17-87ba-aa6b3d74c5ab','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('245d86e4-c0f5-4a17-87ba-aa6b3d74c5ab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('245e86a9-dc4d-4609-89f9-37bee84bdeb3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2462ec9d-7458-4e9e-a47c-6bc363f7b74d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2472b691-8bd6-4b00-932a-484866de4bca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2472be6a-3a54-458c-9253-fc454e602255','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24773ae3-fb69-419c-ba4a-4d86070b1f3c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24846ff0-d7ab-49f9-891e-f61e862fae79','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2486378a-1152-4e2f-b403-dc4cbd6bedf5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('248997bd-e4cb-4d21-939e-04bf17629317','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24b0cddf-aa79-4410-9c4a-84c524123a5c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24bbf3f4-6ed9-403f-b171-2a88e988e338','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24c3b325-81b9-41c0-a173-a163cef62175','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24d5ad8d-377b-4c01-8825-a0498a1940b4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24d88cb8-2a76-4684-ab5d-eb9449146f52','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24dcee51-4faa-44de-abb5-93c290784d4f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24dd1c60-eb96-41a7-bc83-ed217378fed2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('24e8fe61-3e23-44dd-a260-94443513991d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2502275e-4b25-45ef-b518-e946aa91a3da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('250c5a68-3908-4e7f-8d4f-58499b08b7a9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('250fe8dd-c58a-41f6-90c1-a49497dec951','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('252b064c-11af-4f56-bca5-a25f61b1db9e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2542092d-adcd-4bed-a176-def3a4dd6010','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('254495f0-4935-499d-aa01-eadcc6ea11bd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2578c2c4-600b-481b-8337-356e04e29427','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2578c2c4-600b-481b-8337-356e04e29427','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('25793483-1aeb-473d-b6bf-dd0943bae176','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('25c0ec40-4dec-4b33-a267-d035ddbc2793','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('25c906cf-b944-4b92-934c-7361619185ee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('25d62b6a-f763-42de-a43c-068d5e0e742d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('25e038d7-f2b6-4191-a5fb-8fc57f30fd01','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('25e0b0a3-c581-4f76-af24-3cce36f3954a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('25e0b0a3-c581-4f76-af24-3cce36f3954a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('25f14d57-8972-4ea5-9f6d-789ee186d54a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('25f42787-22cb-4183-af00-5d9e7f0d4091','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2600efd4-516a-4512-bdfe-efaab063156d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('260326df-5179-4396-be9a-ea866ff13874','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('260d858b-c6a9-4bf7-9871-55b7b1b14ef1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('260d858b-c6a9-4bf7-9871-55b7b1b14ef1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('267721b1-90ce-49fd-b8d2-4ddc2d8b77d9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('267721b1-90ce-49fd-b8d2-4ddc2d8b77d9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2688b9ee-56d2-44ba-b72b-28ef2bc32a32','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('26b2ecce-1d70-4921-9666-2c7da812587a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('26c958aa-9d5f-466d-882d-c5830339eb87','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('26c958aa-9d5f-466d-882d-c5830339eb87','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('26cda226-a0cf-4ab9-9709-5d7a789d0544','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('26cda226-a0cf-4ab9-9709-5d7a789d0544','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('26ce7c16-a7e8-4020-aff9-f4c52936ab28','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('270054d0-be0a-4a49-8164-39908cb116b8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('270d8244-3e24-4c23-9b02-a9607bd9b53b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('271b67ed-a21e-445a-a3f9-bb4f5f3bd990','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2737f37f-70db-40d2-8667-3338e4308269','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('27648458-d175-4982-b205-1f5d124ea06f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2764eff4-000b-473c-bdc3-547fa47c4d62','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('27781f12-e00e-4191-8cfb-5f88d2637bf2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('277c5a8e-82ab-4cc1-a419-392b914d6b50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('27987bae-fdba-46fa-a929-d27ea656918a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('27a5a23f-4c46-4938-8234-4c50336fbe43','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('27a94083-954f-4041-ac6f-4708446e8ae5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('27a94083-954f-4041-ac6f-4708446e8ae5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('27ebdc97-70b3-4a69-b93f-3071f83ca369','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('280b21a6-0793-411a-bfca-0fd4302d5331','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('280f5997-6d05-4edb-82cd-5e78a575ba44','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2836ea93-325b-4354-850d-3d1349fad159','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('28657ef1-9270-4150-99c0-86ef353447f2','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('28657ef1-9270-4150-99c0-86ef353447f2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('28cac864-9607-46f7-9eeb-d59d2d1b47d7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('28d289d5-d55c-4d3d-aa03-8a2bb1c268d4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('28d289d5-d55c-4d3d-aa03-8a2bb1c268d4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('28d9ea0d-191c-4bca-8c54-eb7185eb2357','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('28eef37f-c349-4d70-916f-8ba220794998','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('290d6276-2603-48cf-9710-b33b4419e9d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29103511-55a8-44eb-af1d-45b89feda733','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2912c327-7507-447d-a499-04214d40e8fc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29232fc8-eab7-464a-891a-c93d408b840b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29232fc8-eab7-464a-891a-c93d408b840b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2925ce6c-68d7-4b86-a5eb-802cb5c8a921','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('292f61ab-5641-44d9-9da6-14489ff7102c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29323914-c545-44fd-9e46-8821bc7d3e02','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29572e55-f6a2-44d8-a522-dcabfd61f8fa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('295f0e85-e9ad-4007-b96c-f999824a5133','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29778124-21ad-4c64-8d7d-f8e87cec386e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2978e799-3d90-44f6-8430-9f29bb90a8bb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('298ad9f2-ba8d-43d6-8103-5d0caa3f1384','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2991722c-af5c-4d10-8998-954b2b994d5f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29a58ad4-50a7-4584-a12c-46551659484a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29aaa6cb-9e0d-48c7-b98b-4e8da884e925','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29aaa6cb-9e0d-48c7-b98b-4e8da884e925','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29ba284b-2254-4cd7-bdcf-55ae09e45c25','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29c49e47-c04d-4097-9284-56b1baf375af','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29d5b931-5832-429d-86ea-cd259ebee619','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29f311e3-3e2c-4dfd-9f4b-c5af762f7a60','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29f311e3-3e2c-4dfd-9f4b-c5af762f7a60','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('29fd3a93-7375-4d57-971a-85ce862fa119','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2a03ad83-3466-4cec-99ff-7420562871c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2a1447f1-2488-4cd4-a1c1-b9396ed43d6f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2a1c326b-2503-46a8-ae03-6d864d43683f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2a2cc8f7-e8b5-410c-bda4-4e7c038d5e55','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2a423489-d9f0-4df1-b803-9cdffdadcae5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2a441b11-d183-4912-a7e5-68b1ad8a6236','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2a67f176-58e3-4824-9319-6d65c33ce4ba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2a83cf5e-dfbe-4a78-b371-01e369341660','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2a85fb77-2236-4d2a-8a52-3c90b9f1d004','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ace4239-cf26-4427-ba84-8f80004c4b3b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ad8f197-abd2-48ba-830d-baacfd36a16c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ae108a3-c53c-459c-ada4-baeed1f0ea56','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2b04f880-684a-4b1e-bca9-bcf6167ad728','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2b092287-fd00-45ae-80dd-6424444dd3d5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2b092287-fd00-45ae-80dd-6424444dd3d5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2b1043bf-040c-4fdd-84c0-666f7236b394','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2b4773f3-a12c-471c-9318-7a2d1ae5b0ac','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2b480151-7c77-4a26-b90f-7289542c82a0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2b4f588e-0dab-413c-83af-bf5a94b8d3ea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2b54c965-192d-43b7-8412-21a7a0d0dba7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2b9a852b-b59e-43e0-8356-16fb670dad06','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ba768c7-50fc-4005-84cf-f84fbe7d9726','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ba768c7-50fc-4005-84cf-f84fbe7d9726','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2bbd71db-1d82-49e6-94ce-abd090257a5e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2bc19c86-a46e-4dfa-944e-7dc0b33fd426','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2bc2621e-2698-47ba-8007-2320fbc8b628','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2be31ee7-e32c-476b-b01d-5e8bd9648fd0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2bedf134-09d5-4d13-8dd5-ec30d58d4d7c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2bedf134-09d5-4d13-8dd5-ec30d58d4d7c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2bf7d8fd-48db-4883-91d8-5d1865de3a1e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2bf7d8fd-48db-4883-91d8-5d1865de3a1e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2c05e9d7-4c24-43c8-99ff-f1c9f83306dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2c23168d-b0ef-499d-ab1a-abd1f096fa40','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2c3fd524-458c-46da-9022-e88d446c4c41','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2c41cd11-e96e-4632-b4dd-caab2f97e6fe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2c4e5390-e918-4ba9-9bfa-d4c6ac71ba24','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2c6dacb3-3023-4077-9dde-0c8c1afb9208','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2c71d0c7-d069-4f27-8dee-dbc98a25f87b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2c9ebaf4-a3b3-4ebe-baf4-bb3c2934291b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2cbb5fb3-4784-417d-b07d-d57f54e77d74','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2cbd3d00-bf44-471e-b1c6-f460a2b6acbc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2cc0b7b5-a272-4f61-9e00-f1f50c9dd63d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2cfa6c08-c64a-446d-af48-063758052996','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2d0a572e-ee53-4fc7-98bc-21288f73a2f1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2d1f6082-e20b-4209-8cc9-f7823049b169','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2d2fdc64-ee64-4311-a7ff-43301ce979ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2d48f724-e907-4ecb-b255-984e703b59c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2d4d70ff-a023-4652-86e3-d83c72ce8d93','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2d56c4b4-1f3c-4221-9173-c9037538f87b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2d90d7ff-58c8-4e2a-b3c1-05919edf635f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2da32594-3198-4575-aefb-b64305f7366c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2daf8a25-3854-4326-b948-26ac107e8095','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2dc3ece3-51b9-4717-a05b-8a0f037bd344','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ddda013-cc8f-4948-a7c9-a25e0876afdf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2dea402c-99f8-4648-8f2d-1378307710aa','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2dea402c-99f8-4648-8f2d-1378307710aa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2dec1ab7-ff87-4e57-9a5a-2b34f2a8bc7e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ded2b50-b28c-480a-8750-01ee7ae1483a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2def4411-1f8d-4461-afe9-69131e63832c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2e269a51-b6c8-429b-b6cf-f94ea0026b01','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2e5776a6-5539-47d7-9838-0dde129929b3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2e5776a6-5539-47d7-9838-0dde129929b3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2e7e0d26-5c61-4b7f-ba34-5d2be892982d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2e7e0d26-5c61-4b7f-ba34-5d2be892982d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2e839c1b-2c13-4a36-9c4a-0fea87f2a9b8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ed02862-2ffc-488a-94d3-01b7d1fda40c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ed33d42-e1f3-4376-b187-dc507fcd86bf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ef4a843-dd8c-4e2d-ac13-2171805d2822','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2f0ead07-a7bb-4a88-9639-069620c92f25','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2f116e36-ede3-446e-97c7-1f6cc6166061','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2f17d31d-47d7-452a-b706-e6a5268b2add','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2f2b0c67-5d88-4911-9fd3-f4554c2906cc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2f2b0c67-5d88-4911-9fd3-f4554c2906cc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2f7c3610-8cec-4f63-ae2d-15c195906207','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2f7c3610-8cec-4f63-ae2d-15c195906207','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2fae2426-5b7f-42ad-a3f8-f3dea9661842','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2fdcc8d2-cd2c-4530-ba54-06e1a4960f22','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2fdcc8d2-cd2c-4530-ba54-06e1a4960f22','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ff413e0-c56e-4f0d-9c87-38d4fa0eb97c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('2ff413e0-c56e-4f0d-9c87-38d4fa0eb97c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('30095f23-e541-4fba-8d24-fd62c2d1c180','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('30095f23-e541-4fba-8d24-fd62c2d1c180','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('30225121-f721-4bf8-9a27-a67b30bcc5bb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('304e0792-f6ad-4c66-8d4d-103a03d9f951','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3088cbca-f4c0-4398-878f-e4a9d6b8d9f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('309951ae-b573-4eee-a578-b6e08f868fdb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('309fc4be-7a27-498e-b565-1f9899a54a82','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('309fc4be-7a27-498e-b565-1f9899a54a82','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('30aa27fd-9053-4900-b90f-ffbdaa0b8942','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('30aaa432-5fc3-4603-8049-37c115043510','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('30b50573-adf0-4af8-b7a1-3f770b8dd095','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('30b6d7c7-ae30-4fdf-bfce-3df4a118bdf3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('30b6d7c7-ae30-4fdf-bfce-3df4a118bdf3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('30fb95a6-adc3-4af4-92db-507a2fc9a660','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('312afd8c-6ac7-4595-a719-468e21036814','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('31324f1f-c642-4a16-a83f-2aef7d517a8c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('314c117e-2b10-4f18-a91e-495d652d7111','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('314f19b6-7f9c-4b3a-b9b0-309c037f9cdc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('31766bb1-2cdf-40a1-870c-bed359e675a5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('31978b2b-7b38-4ae3-aa35-544472fdac8d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('31d98506-c404-460b-8210-da758e3dd81c','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('31df982d-7e36-419c-b6ec-65ae2a874cc7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('31dfd55c-1431-4b30-93f6-80120f15238d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('31e01d7d-e81f-4d66-ac67-3d27e510e762','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3206c751-4c6f-48a2-af1b-389e8d67bfa6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('320a3c6b-e104-437c-aa1a-31ae3bf29dfd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32124083-32e0-427b-acad-33d599519dff','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3228e606-a039-4599-b2f0-23342e19a14d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3239d9b9-d351-44fb-ada5-38cebec9f438','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3248c81f-1ada-4638-9c1e-b5f2ae4a7e63','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3248c81f-1ada-4638-9c1e-b5f2ae4a7e63','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('324bc575-28f6-45fb-ba6f-284970e39cb2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32611431-a95c-416a-8d8a-453002dabc83','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('326dbdd5-7701-4af1-867e-349386dd28a0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3287855f-932d-4089-971f-00a481cdb5d7','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32a9978d-e94c-4815-9875-22bf977dce57','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32a9978d-e94c-4815-9875-22bf977dce57','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32b9a79f-f6b0-4e6e-84d5-e1d2b9267a32','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32caeea3-6f6c-4284-9175-4f970857884c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32cecd31-1dd6-4b57-b16f-5b175e96fdb0','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32cecd31-1dd6-4b57-b16f-5b175e96fdb0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32cffbf7-1e61-4ef8-8918-328ac6cd15aa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32d87ff0-8d76-4bac-a114-9ce4fc15cfe5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('32de5c23-19a8-489c-b95f-4f40371ba126','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3309ce4a-0afa-43ed-9c10-474f5173124d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3309ce4a-0afa-43ed-9c10-474f5173124d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('330daa26-1782-4860-880e-54ef161b387b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3310fcf7-ee3b-4823-81c4-b5c524fadb8e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33128f9b-7ca2-4a84-bf31-0a644085f0a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('331eb168-01d2-4385-8b42-aade75f75dd6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33539ec0-7720-4de8-b32a-95399b0f41a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('336f19ca-09dc-41d1-ad8f-d74d6b6eaebc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3381d862-788e-49bb-8372-1f9803da07c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('338b86a7-b8b8-412c-9828-c6951be23f38','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3397206c-b76b-4b06-bd85-335bc4d0b542','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('339b59e9-4f35-4837-a0ef-97af7da4f6f8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33ac3d56-6847-4408-bdb7-09ceecab0b99','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33b7c79e-3821-4e77-8233-6c1b3f7f715b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33b9140c-b577-459a-9dc5-2e066811bf90','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33c2b006-ec14-41f5-b570-52e2ca8b0c31','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33d16a2f-66f2-47bc-aa51-62c705fce4be','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33d736fa-d2e5-4772-96ec-0e3e2bc148dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33df68b1-66b4-4a0e-b390-3a433a2ada1a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33e3a2e7-b396-4a90-b6c3-499092d9e2fe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33e9cc24-c45f-4180-9c0e-298d1837502d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('33f8aed9-177a-4247-9464-616cb717cf67','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('34157b17-b9ce-4464-b7d5-cd05367758a2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('34157b17-b9ce-4464-b7d5-cd05367758a2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3444dfa2-d6f4-4ebb-914b-9148ac66d215','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('344d6d9a-50cc-4363-b9de-8c6a3f969dd9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('34510573-3b77-4d67-bb8d-697e1a277978','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('346612cf-5f14-4093-af21-600953b4ee11','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('346612cf-5f14-4093-af21-600953b4ee11','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('346c9621-b86f-4ddb-82a3-e98b2772bf05','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('34b1ca9e-3577-4ffc-9c32-464f5c39bf4c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('34bdab7d-392a-44d8-8860-2c2b3238df77','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('34c3d2b5-14d8-416a-bbd3-743a2109263b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('34c560a4-61ab-4c6d-a117-acf16b3e4145','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('34d9cf03-3bfa-4cd2-b8da-67aafb230002','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('34f020ff-293f-4813-b3ec-be113aae4cb5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3506577d-f421-4974-81a1-96cb0e3c8b52','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3506bc46-0b1c-4009-b574-971a71666ed9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3506bc46-0b1c-4009-b574-971a71666ed9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('350782e0-9052-4885-93a0-9bffbdf62b27','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('350dfa33-7b26-48c7-9921-3ea57a906bc4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('35156469-68c9-45e9-bf7a-49326935ce8e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('353462b7-f2c8-4c22-b788-0d381e9ca1ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('353506f6-287b-4361-bb89-cdf4e202a586','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('354f01a8-3711-4b3f-8413-bde0c8fb7296','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3563ad88-6699-41a8-bb86-b2133a1aaa35','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3563ad88-6699-41a8-bb86-b2133a1aaa35','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('357707a7-4dbe-415f-8b41-5f5c9e593ba5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3579eab4-b18a-44a7-a4cf-372f38c07a25','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3595ab5f-1168-429e-8c3e-aa23183ee261','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('35c35ffc-a1b9-40c0-9085-082dfb67fdd8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('35e0405c-ee93-460d-bb80-84cce82352f7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('35e98362-4b7c-4d3c-bcaf-fe4ac57cbba2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3620676d-d187-472f-81d7-bac1c7f52e33','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('365b188c-8910-4269-9797-a9009e25b679','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('36632e27-2144-4e77-a915-f726265c1f90','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('369d0177-00bc-47f6-9a1b-c57508a9f38a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('369d0177-00bc-47f6-9a1b-c57508a9f38a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('36a2145f-0709-409d-bd4f-1308428ada37','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('36ac5b17-0b32-4347-9f38-30e0bd40cf57','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('36ad5e44-a6bd-4561-b582-70ce1ba267cf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('36d7df5c-7712-4a2c-bf86-3aa7140cbb18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('36eab321-c0ab-4f61-879a-543c8fa1eba1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('36eab321-c0ab-4f61-879a-543c8fa1eba1','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('36eab321-c0ab-4f61-879a-543c8fa1eba1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3728d3fb-c0f9-47f7-a31a-febbc072f76b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('375b539a-2b67-4feb-8aed-32b855505525','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('375c90ea-54f6-4192-96d9-cc5f060f9a72','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3774534c-7415-4220-8780-ea4700f5615f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3774534c-7415-4220-8780-ea4700f5615f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('37a67afe-db7d-4c4d-9628-46a73ccc5a39','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('37ba42a9-00fe-43ab-b50d-ff3c345d7861','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('37be20ea-b548-4d10-be77-c73e8cbc5ee8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('37dad357-72d1-4aef-9833-e7b34bc31420','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3801b550-01bb-446a-8019-8187ac1f0e63','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('381bf79d-ac1b-4b81-949e-1e3d0cd65003','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3821aaa8-4104-4447-8091-4428e151f556','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('382b4fd7-13fb-4356-86f1-2e8d1883f78f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('382b4fd7-13fb-4356-86f1-2e8d1883f78f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('385e5d4a-0bfa-4b8c-9315-394fb5ee03d7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38714daa-99a6-4d56-979d-49cdf5209999','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38714daa-99a6-4d56-979d-49cdf5209999','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38743f6b-97fe-42cd-a20c-13f03e5266b9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38796172-e21d-43c3-a339-2340f3eb734c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38812797-0084-46b8-9b45-c644f2104ea3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38812865-ffb6-4213-a173-adc34eb7bd22','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38903b1b-c428-44d6-9231-a92f167498dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3898c086-0e39-4d9b-93c0-e0ec7da62582','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('389d9a86-8695-43b9-8251-a774b2712d71','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38b21b7e-df13-4882-9e39-aeb3639357dc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38c50938-c21b-4474-b162-60a79cbca7a5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38cbc4d7-4534-4a90-ae18-00791d477dd0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38cfd7a5-d970-414e-ba32-8e17d3d73eb0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38d73168-b6b5-4d01-becf-c08bb769d26c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('38fca0d3-52fb-4f48-9cf3-27973536a52a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('390ddf2c-8001-4727-86b4-45dd1aada88a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3923a151-47da-4f90-86d4-0c3598b6796a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3937ba7b-0948-4c68-a4b1-db54e4ce8e86','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('393e49bd-69a6-4204-9f00-f8919423fe7d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3940d393-86f3-42be-8e89-fecffe900a6f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('395cd499-e63b-4043-9d59-b1544f833bd0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('39600a65-311c-4e61-8f7a-44d762a0712e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3980e3ed-4fe9-41ce-9b3c-c56cbcc1bc1a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('39897f20-c4b6-49bd-8e34-496b8e782473','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('39a25427-a1a9-4678-96df-8631c0351432','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('39a25427-a1a9-4678-96df-8631c0351432','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('39d3f02b-d6b3-49be-854c-8f4f98aa8469','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('39d9087d-c2b2-413b-8a43-f2296083f62b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('39f84b0e-2fd2-4f14-90e3-10d8e793eed4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3a00df71-0f9d-49cc-a39c-7c4d72ef4226','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3a0890e6-a0f3-4e1b-b367-18c6adc9f8d2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3a1d41f0-88e0-479e-be71-0bbb77fe9b56','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3a395d90-642c-48e3-945a-5051e7c5a920','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3a51457a-250a-4ee4-8281-d6d6aaa78fa3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3a748a2e-679c-4ad2-bd82-63c526a46122','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3a7661d9-b850-4b5d-b954-2a6cf1e8a1d9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3aa53c93-7d09-4bb0-bead-2d6fc8256038','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3ab893cd-2cae-465e-b000-8effa990b218','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3abaa731-fc03-47f9-aefd-6f7e96d489e8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3ad3d218-10a2-429d-baac-e231801d74f4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3ae4672c-317f-4cd9-92bb-b4cb2df3397a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3ae4672c-317f-4cd9-92bb-b4cb2df3397a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3ae4672c-317f-4cd9-92bb-b4cb2df3397a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3af14277-4b18-4179-91af-63ff50bee5de','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3af1b6c2-d221-49bb-8ce7-a5f9b3f162f0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3af5c378-957c-4d8c-b948-78c1916b9895','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b0a12ca-d508-4393-9efb-686cf98849b2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b0abb47-90ef-4c70-b2f5-c0f9992c5c08','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b32345b-7f62-47ff-9567-3e4f07aabd80','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b3a4888-11b9-41e9-8a51-7b0aca77e4fb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b3dde31-95f2-4e21-b080-a1fdb2a2ba6f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b65e1d1-bc59-45e0-b5b7-bca682869aa1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b6923ee-ac45-45db-97b8-233968138978','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b7a1665-77ff-416a-8513-6ac92738617b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b7a1665-77ff-416a-8513-6ac92738617b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b7a1665-77ff-416a-8513-6ac92738617b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3b8c085a-ae3c-46ea-a0d9-bdddae8c12a9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3babdd9a-0945-41a6-bc35-0645d2e37c07','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3bb2bb69-f5c9-466c-9142-1c5dcd1e6422','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3bc73b0c-e564-4d46-9fcd-073c811e8716','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3bc7caa4-9ea5-4eb6-9060-91813bb4d32f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3bdbeb95-842c-4871-8399-c16eb125254e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3bdcee5d-83d1-41a9-a665-1c56fc9d1293','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3bf69e6b-98d5-42ba-b8bd-32f8255d98cd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3bfeb1b8-2d39-48f3-93d1-1fe001cda160','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3c24baa4-4c05-45e2-8cf1-d3e1a2d9ed44','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3c675778-2cfb-49a9-bf2e-bed8062f4a07','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3c7660be-9b1a-4406-beb1-a8ab36360a39','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3c7a8ca9-ffbb-4623-9d5c-6777479ad0e8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3c82b613-bbd9-4cd9-b734-24213800cc25','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3cab9346-4c93-40fa-aae5-4338ee6b7f02','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3cc08aca-a1a6-492d-afeb-731f68055af8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3ccab1cd-5cf3-435c-aee2-dfed1ea84215','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3cd37fb1-bda7-4bb3-b491-f4fd8185b81e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d087c63-a1f8-4cba-8989-cc8242cca6cd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d15db17-c6b7-4c72-98c2-8130c01d4a21','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d183ab1-61ee-41e6-9cec-d0d38b8a1389','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d183ab1-61ee-41e6-9cec-d0d38b8a1389','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d183ab1-61ee-41e6-9cec-d0d38b8a1389','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d370692-32e3-40d1-8cc8-3a16702aadbe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d385f02-8308-4275-b79c-0372c51f8c8a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d5ddae4-753c-4f54-a695-eb2a3286128b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d5ddae4-753c-4f54-a695-eb2a3286128b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d76e3a8-8291-4dc1-8c1d-de7b3cc6aec2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d76e3a8-8291-4dc1-8c1d-de7b3cc6aec2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3d95c0e8-a1a0-4561-92b1-695a1796e4b5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3da57d7f-6c84-41bb-b8e6-3d62395300d5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3da57d7f-6c84-41bb-b8e6-3d62395300d5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3daa84db-5394-40ea-ab78-9695b55f5fa2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3db85a18-af1e-4760-8a6f-e59f8085bc2d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3de228a0-32d3-4d9b-b4a6-7d618875e58b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3e03526c-d386-4d8c-a181-8fd146452a0a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3e0fa3cc-9b85-419f-8307-bd538fd5a6f0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3e1bd068-d30a-474e-8bf6-3dd17e405d57','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3e254219-c86a-4294-b8ca-fa1ab27227cb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3e298709-095c-49e2-ac7a-7d8a7afd6b17','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3e495402-c9c3-4237-a14e-d62b53493c0e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3e6031b5-1700-496e-b7fe-3f39073368f6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3e6ef716-e90a-4349-812e-4418c3b63947','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3e82165a-2452-4322-8a5d-f3bb45efdd58','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3eb3bf67-e136-41a5-9b44-c550352f9216','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3eb5df3b-de75-4e8b-b5b2-9b02bcf751ca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3ee6dfc7-6a96-446f-a6ee-3c369182057c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3eedb396-cfce-48eb-9410-335fe2e6b55d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3ef43525-f87a-4cba-8c97-7710ff200cc3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3f285682-8815-4e8d-bb43-752beb517319','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3f51d4ef-b5d3-4acb-8812-b8e01ee746b5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3f51d4ef-b5d3-4acb-8812-b8e01ee746b5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3f6f545c-09f9-4f52-bc2f-c42a5b20b21c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3f723f1f-fb7c-4bd7-a1c8-1eb5c20d5be9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3f87fe1f-c593-4be6-9094-0371e242ce9d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3f8a91fe-1bf7-4f8a-931c-711fe2da1e56','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3fb2f4c3-8595-40a8-b0bc-5abde527240c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3fe5a465-ba34-4327-9b43-2e7927344a13','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('3ffa8e0c-adf8-45ae-8198-7d5b9c6d691e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('40041cbf-5ce3-48f8-ac3d-fbb44b89233f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('40199a69-6fd6-4687-8dce-e038b1012b55','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('40199a69-6fd6-4687-8dce-e038b1012b55','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('40285255-feff-471b-848c-f29d220efb17','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('402bffb5-5d94-4cb3-979a-e8a9ea3e1a8f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4030d89c-400d-44a5-9010-3e5743c7d140','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4056c15c-5355-4827-925d-85e5a20cdb4b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('40605dda-3975-4ae4-8685-7524a296c878','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4068ad72-23e5-49c9-b550-06890ceae743','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4070149c-aee7-4a70-b5bc-3b7052ba7eeb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('40acc30a-c241-4cb7-9b26-65e1ace46177','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('40eed8f6-c50d-4d73-97de-a25220a3e287','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('410292af-7985-46fa-a0f2-346877674ead','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('410cb392-5e67-489c-a033-777b8983981a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('411ca2e4-e2a3-4af9-9e8e-835daa2d5e28','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41313089-3d51-4d42-b87e-232adb03da18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41573f09-159a-4a56-957a-fd351a5e3239','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('416f9ff8-b29f-435d-8000-e49a34c5bf6e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41c0b181-04eb-4c04-b5ae-3ef169bcbdf0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41cff62f-0db7-4b29-a5f7-c4f007afba4e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41e54447-6d06-40e8-aeec-6c9f8bc86568','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41e54447-6d06-40e8-aeec-6c9f8bc86568','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41e54447-6d06-40e8-aeec-6c9f8bc86568','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41e965f6-7179-4a9d-8eb9-62ad235976f8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41fb77e7-f933-4885-9780-7046a64f1ceb','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41fb77e7-f933-4885-9780-7046a64f1ceb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('41fc1c53-4c20-4f1a-abce-fce472e0eeea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('420bfce1-c00e-4e2d-b786-95c4c2b18d20','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('421e5c23-0e85-477e-bdf2-9f39b3325b9f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('421e5c23-0e85-477e-bdf2-9f39b3325b9f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4220e3dd-4c8f-4ddb-bb07-aee70729a610','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4242b5ef-79ff-4487-9c33-68caeefcafa1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('426b8b60-d0a4-4781-abe0-33d7b656e481','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('426e3921-c867-469b-9fa2-6b1fa6ec6376','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('427f7ab8-fa8d-4eeb-a61e-c78c3769a8ca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('428e1c41-e78b-4e28-91fd-592be575390a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('42a167d6-4f4d-4115-8c29-abe7085b1eeb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('42a386c7-e7ff-411d-bc78-da94bf439f54','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('42a40d3e-f325-4c2d-b686-27cc69eaa8ed','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('42a40d3e-f325-4c2d-b686-27cc69eaa8ed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('42bcd499-a6e8-49a9-a75e-c6294eefd98c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('42c68ad9-09d9-4409-8567-47982033a38a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('42ec5bde-743f-4d7b-a034-d7ec4e2c5ac2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('42f556db-6160-4f3e-8c80-9bf581f8c63f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('430d0c28-e97a-4cd5-a911-03a0122b47c2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('432aa773-1e05-4da6-9916-bef0a648a4c9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('432b563b-8f8a-4b88-9ab4-47d9e195652f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('433583f8-4efd-49fc-86be-47c295f9cf6a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4336fed5-c804-4cac-9c25-f9ace638a586','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4336fed5-c804-4cac-9c25-f9ace638a586','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('433e8d58-623c-4971-ae4a-a33a8226119b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('43472a7e-0d36-47fe-af48-7068058a0d20','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('43488319-363b-4de0-876c-dc52d4e9be33','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('43493fcc-e55d-4e9f-b8d4-43915b3e051d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4359f108-175a-4263-a635-9f52f874908e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('437cec19-8021-4dd9-91af-32fc7a7dd205','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('437ed230-513f-48eb-af84-6530ddc3f19e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4392d51c-453b-431d-b223-928ad0843720','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4399c8b1-8981-4bff-aab3-9b645eba0553','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('43b4e169-1d26-4b85-9101-307a5b95f4da','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('43c7094a-fc9d-438d-9bf0-4588b2aec9a0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('43e451b3-b4a5-4df4-bcc6-766179b1b4d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('43ec9534-a65c-4433-9bf4-a90d9c4ead6b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('43f96fcd-1113-41c7-b7a2-a6b4d0e5b745','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44013ab6-40e7-4f8e-a6fb-8cd0074a7a4f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('441db229-b766-42fe-8d12-19f9b48e5deb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44459994-8d10-4482-aa10-0573e1398e2d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44459994-8d10-4482-aa10-0573e1398e2d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('444cb090-1eac-49ef-b977-a076ce913224','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44781009-5b9b-49d4-80a8-8b9ee8f1381e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4493e6af-7fde-4eac-9604-ce5afbc5c652','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4497d362-3c1b-4a52-9662-1deb2259bda3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('449f719e-0049-4307-9afa-83ed723f2196','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44a84342-4415-48b5-bb8d-d185eededae1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44aa009f-dac6-406b-a4bd-ddb45ba83d85','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44b92d38-c394-42b0-8c62-61d4d4df09ff','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44ce3ca6-24a8-40f4-87fa-d3ffa0d31549','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44d20406-799b-41be-9e12-4e9ce6dab3a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44dd1b98-f7a5-4cee-b30e-9f54ecc55dc3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44f2275d-3a56-428a-87c4-dc946d410f49','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('44f2275d-3a56-428a-87c4-dc946d410f49','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('45175df8-b9f4-452b-bdad-ae04c52bbd04','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('45296876-a9b6-4d1c-9296-a339405e834e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4544913d-9b2a-4a0e-92f1-e80bcc192b42','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4544913d-9b2a-4a0e-92f1-e80bcc192b42','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4544913d-9b2a-4a0e-92f1-e80bcc192b42','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('45543eff-0b8d-4ab8-a89f-860dadfc64ae','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('45543eff-0b8d-4ab8-a89f-860dadfc64ae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4589fe29-09da-415d-b954-a4d74e6149f2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4599ca65-e549-49fc-9a36-5541df629d76','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4599ca65-e549-49fc-9a36-5541df629d76','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('45a4f08f-4b26-4f26-a1f5-902dd27708cc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('45aeeaad-1212-4cad-a975-3c4d6b57742a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('45c70c50-010a-4845-90ac-cb7fa13b965a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('45e57b01-d711-49e6-b77e-e710733e8a7a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('45e67f54-8750-4002-9f1c-53c338535e93','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('45e7c80e-af82-47ea-bbf0-a0edfd9a7f57','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('461812ea-b000-4ca5-9112-cd9f92b1f24e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4645ec4d-9d6a-408a-88bd-28e57aa827c2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4668bfbd-d4d5-49c2-93b6-a3a604882969','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('46740142-c02e-4665-a7af-cfb67ac3ada0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('467d1f8f-ffd5-4d11-b919-583bdb6fbb79','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('46b6f249-8ce8-4421-8971-cfba76dd3e69','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('46d5aa04-24a1-4c74-aa69-516432c5a409','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('46d8e038-b5f3-4bae-9af1-e230f824f567','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('46d8e038-b5f3-4bae-9af1-e230f824f567','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('46d8e038-b5f3-4bae-9af1-e230f824f567','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('46db6847-7359-42d1-9760-2f7c6ed8f7a2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('46f71e77-939c-4014-a179-8b6bbd5cfddd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('46fde146-901d-452a-9600-90a0f75abf13','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4710b5d6-f0e4-4b8a-a314-82ab5ae74266','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47168366-e2f1-4b43-8e07-8f23be8ddfc0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('471c6fd7-e4e9-4766-b5ce-4287d9c5bed8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47245037-e25e-47cf-8c95-af03684b49c9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('473ad613-8eb7-474f-b68b-aef93b9a24cb','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('473ad613-8eb7-474f-b68b-aef93b9a24cb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('473cd03b-d8ca-4950-925b-6100edc19246','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('474dc135-48e0-47cb-b6e0-f66973acfc32','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47543c49-c74a-42ac-b59b-b3814ce294de','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47582157-3a5a-4baa-9040-408f77101e79','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47614f5f-3fed-4475-88e2-578dd9d5e046','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('476b4d41-8fbe-42db-b90f-1a5739f69673','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('476b4d41-8fbe-42db-b90f-1a5739f69673','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4784e9c7-aaac-41a1-aaef-9454f24f5127','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('478f8dcf-738d-46f6-943f-f3be7d8a402f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('479ea81f-1fc1-4be9-a0ea-84090d9ffc76','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47a857bb-095f-43ad-b4a6-d3b72ce56b6d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47bce193-13bb-428b-abe8-1402a8baf07c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47dca179-02fa-4fc9-9d16-66371e0bdb89','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47dca179-02fa-4fc9-9d16-66371e0bdb89','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47e958ba-541e-47d9-b417-7d1b3fa0943b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47e958ba-541e-47d9-b417-7d1b3fa0943b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('47ffb6b1-b2b7-416b-91ff-c25a73877681','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4800de66-4b3a-4220-87da-edcb91ee1a91','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('48447672-b7bd-43d2-a768-68f4e473384a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('484e6b93-079f-4401-a868-c7052ceda763','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('484e6b93-079f-4401-a868-c7052ceda763','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('48613826-f784-44b4-9335-b95cd33b861e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('48b5333d-e437-4462-ae0f-c9170fa65907','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('48c6c916-a9eb-40dd-8f82-e4bce7cdf36b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('48f30e6f-4bb2-44dd-962d-49b351c5d030','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('48fc8c56-0330-4304-8632-590eb5f72eec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('48fe0ce8-09df-4f49-b695-cd5653db18ef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('49014dc9-a6f8-4315-9882-7694535d86ac','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('49158011-ace0-4d54-89a9-aff6084531f1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('491ba19b-09b5-4025-8f61-598e4bf015a7','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('491ba19b-09b5-4025-8f61-598e4bf015a7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('49247b40-6a23-49e8-96f9-18df89ba06f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('493a946d-0ca9-4917-aec0-8e3471d3076c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('493a946d-0ca9-4917-aec0-8e3471d3076c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4941244e-91bf-4c88-8a1c-496177beea26','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4942e2f9-44e8-4c59-929d-4cb8e9a66409','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('49487141-98f6-4ec0-93d5-b3869627490e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('49752058-9ec9-4827-b50a-6042222c8756','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('497f2746-f6f2-45e7-bd60-fcd8a92e162c','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('497f2746-f6f2-45e7-bd60-fcd8a92e162c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('49d5663d-269a-48a8-ae3b-edd5aa132ac0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('49dfe38e-5f5b-4b0a-9665-0b6581ad1e1c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4a4629a0-cd6b-4554-a42d-77ba5adbcd53','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4aa25dd2-c7ac-427f-8f60-e6187dce25ea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ac235bd-fb56-47b4-addd-7367082d0646','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ac58610-c92c-4ddf-8e37-c2862a571bb6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4acf13bf-6641-4595-80b1-80a4e202a21c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ad9772e-950b-4be6-8754-cb20c3bd8c5f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4adc384c-ab54-4ee6-90e6-a97b6644a3dc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4adc384c-ab54-4ee6-90e6-a97b6644a3dc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ae2e301-6ab4-41d3-9825-dd5c4c056638','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4b2cfe4f-056c-46df-8502-a0d925fa16fc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4b6d2007-f2da-4f95-a3ab-894edf603887','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4b7fd056-e74a-43aa-a681-24a86fbd9565','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4b96b21e-d807-4749-bbc2-9fc75645b7d7','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4b96b21e-d807-4749-bbc2-9fc75645b7d7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4b9cb1cd-c726-48ff-86fa-853472064ac8','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4b9cb1cd-c726-48ff-86fa-853472064ac8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ba90281-eca9-4b8b-9e91-d4e29a17b8c1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ba90281-eca9-4b8b-9e91-d4e29a17b8c1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4bea1b95-b60f-496c-82a9-e9ebf3c267c1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4bf75561-41f4-46a4-8f04-1e18e6721184','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4bf87401-80b6-421f-8a6d-11492b8d48c5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4c1958fc-887a-4b17-aad4-881b66f34d7f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4c2a7fdd-5477-43d8-a57b-cd06c183ecfc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4c32ec98-0ac2-4fa5-ac83-0a470413b48e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4c33abab-1c17-4af1-a68b-734a9ea2acde','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4c396ef8-8d76-4e3f-9796-d27b11e6f1c5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4c648469-d066-4e88-9dd6-5d02fb077ea9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4c648469-d066-4e88-9dd6-5d02fb077ea9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4c7402a6-9a26-498f-b2c9-ef7d8702c454','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4c793167-485b-49a7-b442-6f602c3f047d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4c996edc-26b4-452f-b96d-af5885e8433f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4cb3fa9c-9a00-44cc-a78e-e2f0b37beaad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4cc5dd7d-bb45-4810-b799-ef623ca4654b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ccce82f-ce4f-45a2-9d3d-39bfd941441c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ccce82f-ce4f-45a2-9d3d-39bfd941441c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4cd307d0-a60f-4fed-a833-50476fcabec5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4cd307d0-a60f-4fed-a833-50476fcabec5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4cdbba8d-f3c8-4fdb-9a0c-fd69e7300e19','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d065cfe-2379-45ee-bd7c-857aaaac4a21','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d065cfe-2379-45ee-bd7c-857aaaac4a21','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d0ecc88-14fd-4a7c-930e-76450b0ef291','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d2026a6-47d5-41e6-80df-e74147e2fc12','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d2026a6-47d5-41e6-80df-e74147e2fc12','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d2db499-bef8-4cf2-a317-c0c93c74a0c8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d35c02c-e9ae-423d-94da-df5e2c083f4d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d3ca87f-779b-4ab6-8c5a-27395b4ca4f2','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d3ca87f-779b-4ab6-8c5a-27395b4ca4f2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d3ca87f-779b-4ab6-8c5a-27395b4ca4f2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d6473a8-e1c1-4f21-81e6-32dda53d89e9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d6e0d64-d047-4355-89f6-f621248e4b2d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d799dde-9549-4ca0-bad9-20dd2577d778','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d8a6365-fe88-4fb3-9b38-1deb60fa02d4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d8a6365-fe88-4fb3-9b38-1deb60fa02d4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d9c497e-70f1-4b35-9d44-03c92c054b42','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4d9ffd28-52c9-4ce0-a753-0e17760c73a8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ddb16b8-332d-4c82-a69d-788a85d92b56','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4dece39c-f7db-4a07-b63a-7285c52e92e9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4dece39c-f7db-4a07-b63a-7285c52e92e9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4e024f52-bed0-4b4e-9757-b4d4045f352a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4e681d57-c49a-4dcf-a681-fcdf4f1d50f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4e88b88e-e945-427b-8115-51a19d13198b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4e88b88e-e945-427b-8115-51a19d13198b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4e8d4dc6-4c83-4f49-aa84-b41acd34586f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4e919301-b395-4e80-bd76-6791ce207019','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4e928b08-e170-43ab-86a7-ebb6d1a65904','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4e99e69f-4ee9-4db5-9cd2-6bd7adf5f898','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4e99e69f-4ee9-4db5-9cd2-6bd7adf5f898','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ea755e9-191e-4cf7-bb03-7db90214e333','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ec00812-24a3-4bb6-87a9-1b641357e4ed','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ec00812-24a3-4bb6-87a9-1b641357e4ed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ec2810a-57fd-4e65-a92f-3289d4bd98ab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ec317da-e50d-41f6-89ab-224eb890ea6e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ec317da-e50d-41f6-89ab-224eb890ea6e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ecbe9ea-fd18-45d2-ac2f-cda9697cac1c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ed5bdf1-b44a-43c5-afa2-4475b9917b8e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ee719bc-a0d7-4b7a-8497-7a65602dbfa8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f072d35-e208-4347-b617-652562e380ca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f16edd5-935c-4ce8-9fdc-4b21d83926f8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f1832ed-ccfd-4743-9a2c-99d1c2ef398f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f222c38-90c3-47d0-b13d-6c2ec00c63d7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f3ae416-c645-4f60-bf96-e876d21ee2ab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f4a3126-6bc5-471b-9cb1-036b5e1518e9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f4a3126-6bc5-471b-9cb1-036b5e1518e9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f50be85-54cd-4207-af56-fb2347eb0564','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f678b20-6fae-444e-9f72-6fae854fb2a6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f6bc69b-338f-4615-9576-56dce936f7e6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f7f7598-3efd-47db-ba39-0adaa2b89c58','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4f982b46-4cfa-4f04-9ac1-380d433de23f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4fcf183e-78d6-4b03-a03e-e863ac55115d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4fcf183e-78d6-4b03-a03e-e863ac55115d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4fd1a0dd-8c72-4ec9-bc40-217056d2d9d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4fea7865-ca63-46dd-83d1-f546e44dc6c1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4fea7865-ca63-46dd-83d1-f546e44dc6c1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ffabebe-169e-449a-a021-5a1357cb185a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('4ffca8ec-52ba-4a72-ac36-81a55df3a074','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('50159d4c-df2c-421b-84af-8bfd9475f72e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5027135d-1cb5-4cdd-a07f-5e794eb6f7a4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('50336cd1-2ecb-49ff-a0e7-851284560638','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('505d15ff-98f8-4c15-abe0-82978399cb0e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('505ec093-bf62-4ba1-b94e-db1c2b57b8ac','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('509780fc-d3c4-4232-a380-50def1cbaf29','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('50c48fb7-b5ef-4a0b-90fb-80f5c063cf2c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('50c93c0d-dcc5-4555-9b4b-321ee93a891f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('50ca2068-a3ba-436c-994d-054676711fed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('50d2a239-7407-4945-9515-769f277dbb0d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('50d4c822-44df-4a79-91b5-0be1fde6aba6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('50d5188c-813b-48a8-a0e3-e4df7984bb3d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('50e2d82d-a22c-4f90-add5-538e20d1aa09','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5106f1a1-6fd0-460f-a6db-e905b0284641','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51113b81-361e-4cd2-afcc-86a20e73133f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('511d22d3-5f71-4512-89ef-c848ceecfc3f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('511d22d3-5f71-4512-89ef-c848ceecfc3f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5124a960-a003-4f48-b4fd-9cb01cf8b524','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5130bcc5-4cdd-44f4-9dd7-d89ae9fc9b99','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5130bcc5-4cdd-44f4-9dd7-d89ae9fc9b99','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('513a2b38-9a12-4366-94ee-84fb31c28cb2','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('513a2b38-9a12-4366-94ee-84fb31c28cb2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('513b9e54-b6e3-4143-894c-a4771e5e5ead','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('514e6ad2-dd90-45c6-8a49-091147f27ee6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5161cd57-0d01-4281-b8e4-d1f2f4a045b7','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5161cd57-0d01-4281-b8e4-d1f2f4a045b7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('516266dc-9099-467c-bf8b-07d2914672a8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51638a36-bb5e-4643-8e82-56b6d3fcc0b9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51a4573a-a80b-479f-9f47-c3aa64a86929','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51b87e2a-2085-4953-999f-b1879e99c43a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51bdc458-c5ec-4f47-a4fb-a7617b1f4767','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51e4716b-cbad-4907-a0ec-06ab21f4f7d3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51e4716b-cbad-4907-a0ec-06ab21f4f7d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51e6b3ec-c7ef-4044-b32e-cc79c043e6fe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51eca738-ecfc-48fb-998e-ab7baf6bbfa5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51f5d023-0a31-432b-84e7-7f81536502b6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('51f5d023-0a31-432b-84e7-7f81536502b6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5217751f-8f77-4001-9189-bfaa64e5d440','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('522c38dd-b4f6-4701-8371-fcf165bf24dc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5236aa09-34a7-4e12-8636-26bd34952416','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('523814d5-a564-43e7-b90a-dc6fa92b0bfd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('523814d5-a564-43e7-b90a-dc6fa92b0bfd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('524d160b-fe59-43ee-aebe-ca4e6cad5106','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('524d160b-fe59-43ee-aebe-ca4e6cad5106','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('526f5beb-0910-4c60-a628-daf760dd7058','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('527ff536-b1fc-4d10-ae76-f58ac7adc00b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('528c91a0-9e13-4caa-b250-5c8e31a1bc47','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('52949226-9893-40e0-80ea-178995410e8e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('52ac430e-0ab9-4157-9087-31ab9c884004','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('52f07ef2-0f1f-4cc7-a9af-09659cd3dfdc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5303abed-2b4f-4b6a-afb2-5ea2aaf9af97','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5304c3b3-17de-4486-8c49-1f192c4642e7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5334ae49-bd60-4253-922f-c750ff3eacb8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5338235f-c6aa-4ae8-ba25-4cef9468200e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('534182e1-c611-4d13-ad54-f5b9f87a8cf2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5352a46a-83fb-4368-98da-a00c541e10c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5369b847-da2b-4188-b8ff-c3097ec71791','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5369b847-da2b-4188-b8ff-c3097ec71791','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('53884d5b-b8e2-47fa-b37c-900adc7df9dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('53a34769-54e9-4706-b287-6c6c1f17161c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('53a922fc-1de5-421f-8b71-0030bc43b402','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('53b3f8c2-301d-48d0-b897-0aeae94c3688','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('53c3f813-80cb-43ec-962f-4f08fa0b1507','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('53c4007c-c6ef-4479-a120-eb1eeb63c17a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('53c91abe-1cc0-49c2-8aa5-c8e7385821e6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('53dfd8d0-8b5e-4261-be5e-67b975756a70','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('540e42fd-fec5-42f3-a1b7-651852b06230','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('54272f9b-7469-4e82-852c-6ee4e91ba49e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('54300e73-b6f4-4b43-8b15-5d9e500da7dc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('543aaf14-f879-4a85-aa0e-ab9c403a6360','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5456d48c-c667-4156-bc35-d90063078f20','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5456d48c-c667-4156-bc35-d90063078f20','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5459cccc-a700-4fe7-ab6c-9be706c2a30c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('545b6ec2-94cb-4c8f-b3eb-6f36d5ac2f7c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('546b1eea-9742-4160-92ed-098936fb1a51','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5479df6f-ad19-43e8-a718-3bbe2fda2eb4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5489b642-df4f-4848-bd5a-ad04047534f0','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5489b642-df4f-4848-bd5a-ad04047534f0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5499441e-aeac-490a-a02b-311813b7970b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('549b01cc-8226-46f9-938c-da9f2a2847bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('54a1b342-a993-4880-8724-3230d1e990ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('54b04c56-bb90-4dff-87cc-691cd8e29ade','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('54decb44-9530-475c-8c34-7e1bd583df1d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('54dfcdb5-b043-47b1-bd3c-70081098b5a9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('54dfcdb5-b043-47b1-bd3c-70081098b5a9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('54e6a9fd-4dbe-4cd1-be62-d4b77ef76b98','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('550a0fa1-79e9-4622-a434-f59c0a230d88','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('553a3bfc-7c04-4e4b-857b-97ebb9a59f3e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('553ce871-4749-4271-a037-09e7d062c2d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55427985-3284-41c4-9e78-e65259fef675','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55427985-3284-41c4-9e78-e65259fef675','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55559b04-633b-46af-a75f-3f9aea7c2dbb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5558dd72-89d0-45b8-aa2d-56463509a0db','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5558f6cf-dacc-4ec0-bfd5-4b8a10ad523d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('555d57af-25af-44cb-a290-487f6b874a26','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5565ea73-c53d-44ee-9c02-20475f58ddac','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('557f45e1-365b-4b54-876a-29e99f66a26d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55831a1a-6f6e-4562-8b54-92996f67f2da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('559947bf-16fb-45be-9a03-9d9d5ba46e22','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55c025e1-eabd-4752-bc2a-05d89e93bcbf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55d5985e-a60e-48d5-8c72-33b0d8c0f721','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55d5985e-a60e-48d5-8c72-33b0d8c0f721','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55e09df4-a08a-4bc5-9c4a-360375b4d11e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55e61c08-463d-45ad-99ce-1aeb1e42593c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55e61c08-463d-45ad-99ce-1aeb1e42593c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55e7816e-8215-411d-a7f8-34c5c016642b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('55f91a1c-7586-4cf0-abca-f74f268bda09','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('560e2ee7-6fdb-4db1-a996-e06816651541','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('560f7939-2dd4-431f-82e9-3b1e3e1c4c5b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('561e6af4-aec9-43e9-8f3e-6f89b7a3e50e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('56336a55-457b-4dfe-8c1c-c66031307320','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('56336a55-457b-4dfe-8c1c-c66031307320','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('563c6147-c6c8-463b-9561-f2dfa90d58f7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5660a598-4705-45bc-8657-c1c3f55688dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5665bbdc-5692-4766-8299-7009b472154d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5670185c-df41-487d-9af8-1dde4c9b7004','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('567cf537-08ea-473f-b5ef-5be9b6c26118','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('568b1181-c5f5-424b-a5eb-fe27418f842b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('568f1f39-bba2-4845-b152-63ea4fe33dd9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('569cb0fb-49e4-47ec-abb0-7a86b0656ac6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('569db7ce-e025-4515-a4c9-e0daa9dcd1c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('56c4fb95-a0e6-433a-9f52-149b794feaf5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('56cf8224-a228-4415-9fea-46a6c713f45e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('56f32f48-3bb5-4f40-887e-c3f036f4e20c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5703e7fc-13b1-4bcd-87b6-39900c07b5c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5711279b-2408-4e77-bd52-fd6225a1f690','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57136450-9da1-4c16-9c46-b743e4a952c2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57136450-9da1-4c16-9c46-b743e4a952c2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5715decd-dd7a-48fd-8621-b93757aade66','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57245489-e150-44e2-a800-20be3305b5d5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5735f7f3-6193-4f48-9939-5951e079d12d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5735f7f3-6193-4f48-9939-5951e079d12d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('575b4a72-b903-4b18-8138-4aedd52fa81b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('576f34df-cd59-4020-b6a4-be1c4122b6ef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('578a5775-5e71-4b60-af65-d25988b5dd81','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('578a5775-5e71-4b60-af65-d25988b5dd81','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57c174d9-215b-466d-883b-f12b5c60e15b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57c174d9-215b-466d-883b-f12b5c60e15b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57c2680b-457f-413e-90e5-f9eec97f97e0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57eacd84-6ba1-45da-9a38-f7b28c6269b4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57eacd84-6ba1-45da-9a38-f7b28c6269b4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57ec34a0-a524-4726-8eb4-96bbf0ba9117','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57edc07b-4649-4571-91e1-3d68ee36ad3a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57edc07b-4649-4571-91e1-3d68ee36ad3a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('57f2f7fd-4596-46d5-bb8d-f090d0762604','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('58009659-0fec-492e-bcd1-3de5906d6b0f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('58171285-a62e-48b8-8844-b30913a746d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('58478853-fac1-41dc-888d-2e5da630ba02','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('585840c5-46a4-4bb4-bfcf-87928c610c0b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('585840c5-46a4-4bb4-bfcf-87928c610c0b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('585c25c4-70be-4c67-9516-fdc1c666dd10','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('58882db7-291e-44bf-aea9-e560b8c63fc5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('588e5620-0fde-4819-8eab-bae0848763c9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('588e5620-0fde-4819-8eab-bae0848763c9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('58c752ad-7166-45e9-b179-591e0929514d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('58db5f34-d757-4287-93aa-a569c2ffedc1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('58f1ad3b-80e1-4f10-a2ae-95dbd71f52e3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('58faaf32-b7ce-43d2-8cd6-f8c914315668','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5928b6fe-8759-4ad5-9ebb-466ea8fbb160','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('59471aa3-b59f-468c-8c0b-c230d1f65013','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('598873d7-4605-438e-85cc-a0ab1875a4e0','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('598873d7-4605-438e-85cc-a0ab1875a4e0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('59ec8d23-90fb-4d92-87cc-8a4b82070655','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a00de6c-fa64-4bab-82de-742d761b06fe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a033e06-aa96-4e5a-b732-dc727e7c66da','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a033e06-aa96-4e5a-b732-dc727e7c66da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a04a980-8ab1-45de-b853-9726b0885a0d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a0bc3b4-185e-4d86-8e50-155032ec1169','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a12e53c-0e95-4d27-9f64-95c201b4427f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a35fd4d-2893-4dc9-9bb4-c2f441b06875','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a35fd4d-2893-4dc9-9bb4-c2f441b06875','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a55c5eb-75e3-4186-b2b9-0ea80242b3f3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a6734e2-c73d-48e6-92bf-6960363feb85','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a6734e2-c73d-48e6-92bf-6960363feb85','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a69e50f-5c00-4820-ac46-543f054d0a94','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a6eff75-30fe-4816-b462-b9d75c4c2e5e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a79af38-01cb-47aa-b151-8c221b855dae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5a98d26b-352b-49af-80b9-636b9adf3df7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5aa512d0-f9dd-49e6-9486-db3c311cb293','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5aafbf51-a679-4a69-b35e-56fc811aed6e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5aafbf51-a679-4a69-b35e-56fc811aed6e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ab400f3-59c4-407a-b9e8-427b1a5f740e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ab400f3-59c4-407a-b9e8-427b1a5f740e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ab92d25-c43f-41e5-89a8-34684624b60e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5abd1344-7851-478b-8df1-0c1d08fe763e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5abd1344-7851-478b-8df1-0c1d08fe763e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ae70172-cc3b-4375-b0b5-3db5625769b1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5aeb7104-62d4-4c13-b868-fd3a14ff2d09','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5aeb7104-62d4-4c13-b868-fd3a14ff2d09','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5afc574e-9ab5-4802-a7c9-8b21098124e1','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5b044fdb-9c96-410f-91e1-2252212c4aa3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5b1d68dd-c384-45bb-bdd7-e2d0129fdb02','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5b2e65cd-a613-45af-8589-29642e82cb90','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5b32ebec-1f22-4d09-9d4c-b778896f0d50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5b50fdb6-05aa-4dfb-a134-9fdce0454efc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5b50fdb6-05aa-4dfb-a134-9fdce0454efc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5b8f8d5e-ac6d-466f-855e-4e900562afe7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5bad2371-31a9-4faa-a48e-64f18881e2dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5bd980db-49a3-43d1-9679-d8c55a2b222c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5c172d62-45ee-4a3e-8192-0ca5d3c4885b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5c2df380-a8d9-4918-af82-fbd7f542e1ad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5c5074d4-b645-47ff-9dbb-976160ba5726','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5c59674b-9a8b-4aee-84b3-fec96dc51b63','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5c692f32-36a0-4a06-8df2-42f5570431e2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5c761b0a-7e33-4ee7-a3db-a09b287d7138','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5c9d5fe9-c8f0-4e43-a2f0-a77da752508b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5cb634f9-917b-4689-b141-ee93812b31ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5cc2ab2c-649c-4f33-a735-0d00a8f2295a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ceaf6bb-0d01-4e99-8dd6-5f6ff1ea0ba1','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5cf304f3-a50f-4593-97f1-55ed59fc2473','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5cfdc983-de6f-4ad9-8a2c-61874f4a6ad9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5cfdc983-de6f-4ad9-8a2c-61874f4a6ad9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d0b066a-a0ca-4558-a561-9260f181e906','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d11678c-13ac-4b6b-915a-7ec82f16d5a7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d307237-5567-4d8c-920f-db72804f80cd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d4b6f6a-8140-42b9-aa19-f29b2e386c51','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d4d2df3-5d65-4a6e-ac6a-012108e8921d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d4d2df3-5d65-4a6e-ac6a-012108e8921d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d4d2df3-5d65-4a6e-ac6a-012108e8921d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d65749b-cbbd-4e4a-929d-aaf7bb305d1d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d65749b-cbbd-4e4a-929d-aaf7bb305d1d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d8998ef-1a35-49a3-9beb-69030a920bb8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d8e3f61-dd9e-4a98-a429-e6b0725a8bbb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d97c1cc-fe2f-44d2-89e8-b915616a08eb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d9901df-2b8e-4f5b-802d-2b988156b941','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d992ca4-6ba7-490f-bc12-84e0d82b5ed2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5d9e8a40-32f3-48d9-a82e-896dff13da7f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5da03e7e-a253-4a12-9db2-96a41016ad5c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5da93222-8e82-45e7-abbd-638eaf2c21e7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ddd4e2e-69f4-4c53-bb48-885ba0dc0357','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5de1b5a2-4500-4dc6-aa36-6d49c1073208','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5e0346f0-9412-40f0-b293-ef3b236d19d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5e0503fd-c5d0-4939-8276-f0d16a8b746a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5e1a36de-057f-4a3f-9795-a0d1fb96d651','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5e1f74c9-e173-43b4-aedb-e0a67d7221f7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5e2d3fcf-a69a-4520-838d-1f4ef54e0b75','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5e3939ac-df5d-4ea0-847b-880039dd99ab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5e52af4b-59f6-4213-ac95-65a2c338cad8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5e8a0064-304c-4ac9-a9d1-4b423f7df014','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5e944dd8-0973-405f-b6ce-2f2e149d29c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ebde00c-f567-4d3d-b077-b22cf1d13f71','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ebde00c-f567-4d3d-b077-b22cf1d13f71','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ebf359d-5a86-46c3-a7ac-0af079fcd85e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ec2e166-111e-4f12-9885-f208625e9580','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ecebb4b-3e02-4a14-b040-b249268d6548','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ed8c011-9b94-450f-afaa-58ecd40ac1a6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ef9d9fb-78e3-4863-b95f-432fd2e58c82','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ef9d9fb-78e3-4863-b95f-432fd2e58c82','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5f396d72-0578-4467-9f00-590e51a2b8fb','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5f6d874b-29f5-4bc8-9d15-3460d1c22e7a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5f736844-3740-48a0-a686-e5d3d38c5a47','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5f74975c-5373-41f3-b780-4109c698bd6e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5fa05ca3-4e53-4f4b-9af6-17d4481a7507','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5fa53732-4f20-4015-93be-dab3f5e1bff6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5fb807eb-5d50-49b4-83b6-3f7115203608','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5fbb8143-8d70-44ef-9832-b0aef16e5d38','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5fbf9c2a-5969-4e71-a25a-351097509e63','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5fc53d5b-8df3-4196-97f4-9dbf28316b6a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5fd4ba4d-7e3e-4499-bbb9-5689c0a4d8dc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5feeab1e-d85e-46f0-8514-bdc537c6567e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ff14d9a-d8fd-4ef2-8bd2-46e148a2331c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('5ff295f5-2603-46f6-9604-ce627cea180a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6012fda3-65df-4596-95eb-5bb9b82ff0d7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('603d4a49-e8cc-48b3-b217-89ab2a56f96c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6054a224-a02a-4525-856c-8f46f638d025','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('60604568-1b77-4462-90e6-926006bf0bc6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('60604568-1b77-4462-90e6-926006bf0bc6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('606505b2-a4ba-4c66-9e44-d510cd1146e3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('607d4734-cb55-41bd-be0e-d9c352257693','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('607d4734-cb55-41bd-be0e-d9c352257693','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6106b166-bc93-408d-bc3b-950764cb5b6e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6106b166-bc93-408d-bc3b-950764cb5b6e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('610b10b0-30d9-49c7-90ce-639ffae916a1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6116cbb0-c6ae-4a99-939f-6dbdcb98256c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('611f5acb-aa8a-467e-8d48-baa52e34fa45','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6139e081-f615-423c-8789-f2118b7a70e9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('614d9f96-b23d-4bee-90cf-e04c638284ae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('615b5489-7a8f-416b-8e5f-f1eebc9e98b2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('615b5489-7a8f-416b-8e5f-f1eebc9e98b2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('61b3ec19-2e2f-48a3-9ca5-68da1a105bd2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('61b6cd0d-52ba-41c8-acbd-4c8ff2e9d4f3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('61edbfed-a08f-4fc8-a0af-a07a5e7068cb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('61fa55bd-2d6a-4fca-bac7-8c46c97d4b33','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62033c90-6d3e-4f7f-8875-50f4b0912a78','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('620f591a-eec5-4ca0-aa27-431733cc8f97','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62112d0f-e4cb-4064-acc8-9b7a31cff7e7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62351d8b-2075-45f8-a957-4042853ff19d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('623beed5-6c65-4889-866e-1fcaef482c18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62466302-1321-49f9-b4cc-85374af02590','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6248aad5-330a-478e-ac23-8e50a7b2e47b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('625f7ec6-8c0b-450c-b951-cc497d8fc34d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62773c43-ca3d-4f76-9f73-7bcd82f4e932','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6289d617-f844-4e1b-8950-074c204395da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('628f4c14-2315-4a2b-8e2f-f9c78e04b5ad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('629e622a-632e-4c8f-b423-4b77289348f4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('629e622a-632e-4c8f-b423-4b77289348f4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62a5507d-7c2c-4ca7-8d92-f92b255b8991','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62aa7943-e2e9-4f8e-a045-6a13acf6fa88','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62aa7943-e2e9-4f8e-a045-6a13acf6fa88','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62d06772-558b-475b-821c-5b4f2a2815a0','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62e0bac4-db03-4659-9537-bb68a9aec8d2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('62e966d4-a607-46b9-a9d5-33d9cd5b94d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6300cb98-3247-4a01-a3e7-d1bc505605b3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('63099f28-0fdc-4c74-8600-0e27df484e0c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('63512e21-6d3a-41fa-832b-ce59363b42b6','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('63512e21-6d3a-41fa-832b-ce59363b42b6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6351a858-7847-40dc-883e-9d7874c97e28','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('636afbd9-9015-4a67-a23a-c69db2a38729','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('636f50ef-b7f3-4be6-b5a6-a356d83e326e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6387999a-628c-4642-8c4e-5656c6e9d531','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('638f4a57-571a-40f9-81ab-974ff44e1c4a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('63e9b1f4-84e6-4bf0-af9f-64e6bd8b92f3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('63fe6acf-2b8d-4550-a9b7-e5d60adfe596','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('63fe9f96-370b-4761-a7c1-c8dd75b78a57','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('64180f7d-ebbf-4d99-a5d5-d4c54483d43b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('64184436-8203-4597-9326-3923e4497605','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('64538092-290f-47ae-98c8-fcd4737ac358','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('647c187d-1452-4ba8-ba4c-f197cde17176','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('648fe070-7429-4a68-8603-0454c77e3773','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('64b185e1-3288-42db-bd57-71652a8f5452','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('64b3d7b5-2914-47a1-abcf-8cbf1b0df9af','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('64bf1a96-2110-4625-b985-e4898186cda2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('64c00de7-2dc5-4f6a-878e-41070a3c4334','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('64c00de7-2dc5-4f6a-878e-41070a3c4334','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('64f8ae1a-8805-446b-99a5-1a25677d37ee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('650b0f71-1000-430a-b384-5a1363ca65c0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6514f62b-16df-4e77-9b5c-d0b7e02e8f2b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6523951b-1fb9-48b1-9207-936ebeaa4d76','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('65268f50-3f8b-49c6-ba7d-ecc1e4b0672a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('65925f90-2849-48ac-9879-471874cbeafb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('65a28a7c-0752-4d7e-97b1-a10e3f6c5de5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('65b5101f-ecf3-4f5e-8bf7-93cdfa913889','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('65b5101f-ecf3-4f5e-8bf7-93cdfa913889','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('65bb4aff-ba63-4e89-a268-bab9883dd806','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('65bc097d-cbdb-4fef-ad3d-f6c9a1ad7382','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('65f6c4f3-e773-4a0c-96fd-97606629a39f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('65f80af4-aafc-4bb1-bd4d-6a307d4bf42c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('65ff526a-2e51-489e-aaf5-45fc0899a269','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('663dd554-4056-4141-bc95-1c3e3d266126','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('664e454c-ab56-45c6-86a7-24d085cadd18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66534f7f-a131-4bf4-ac7d-24e43d2f9499','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66534f7f-a131-4bf4-ac7d-24e43d2f9499','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66534f7f-a131-4bf4-ac7d-24e43d2f9499','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6662ff35-9128-4aa6-b6bd-5a6107a5b9d5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66889ee4-983e-4b07-b9e8-8df587fbf5d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('668bc37f-773e-473b-95b7-fb3574fe6872','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('668bc37f-773e-473b-95b7-fb3574fe6872','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66a4f752-7688-470c-bcc5-4f04905c5ecf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66b46cd5-c375-46b3-9525-6d999d87283e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66b46cd5-c375-46b3-9525-6d999d87283e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66bb72a9-f91b-4bc3-90d0-a6ee71c25b0f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66ceedd5-8f9a-4688-85c0-786c2cc15ef9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66e5cdea-f0f3-4685-907b-deb6dd336f3c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66e8fba8-5371-4d9b-954c-124b75dd9296','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('66fecc1b-6085-41ae-85bf-df3799cc6874','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('671d49e5-5510-430c-a58f-001e0347b4d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('67316350-ea86-4d6c-b038-1514e11b042a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6749ea31-a6ca-459a-8c61-277e19aeb0e3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6763a835-e57f-4426-ad46-940d674a927c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('677998a4-7219-49cb-958e-6e004df7899d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('678455a1-c1e3-4f0d-94a5-70e6b2926de9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('678455a1-c1e3-4f0d-94a5-70e6b2926de9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('67aaf7a3-7719-4f38-a0f6-44df9a0eea89','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('67aaf7a3-7719-4f38-a0f6-44df9a0eea89','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('67ae7bc6-f21b-4961-b236-aa3e71452554','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('67bd3ff3-cf6c-4ad6-b0aa-d81ea4cdc039','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('67c4ddd9-5f2f-42a5-8d60-0e73fbe4661f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('67ea7f49-9435-436c-99a2-1aae4fef4705','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('67eb0491-518c-4bda-8c1c-dc2db62fff09','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6816a0c4-972b-47fe-8b06-ffc7e580c6c0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('682455c3-7d0a-462c-b94b-e76ce6b4555f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('682455c3-7d0a-462c-b94b-e76ce6b4555f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6850c5f0-c27d-42e2-93e6-413e76e4568b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6854c039-f8fa-475a-81e4-a6114b663b91','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('685d321a-8d05-49c0-a46b-864ee12774ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('686c0383-14d6-40c1-a112-e5eb63f9318d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6887751b-3e09-4332-a588-ce86d25544a0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('689805c4-3562-4e45-a88f-3c284edc26ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('68b0a434-93f1-453d-a359-a4061ed8e1b7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('68bd046c-489b-4397-979f-89117b1ad5cd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('68bd046c-489b-4397-979f-89117b1ad5cd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('690aba19-5c76-43ff-a328-a7e886e1a022','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('690e2ab9-dd34-4bbe-afa2-5cd4ebc87297','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69313c85-9800-4dbb-b9f5-05f0c503f0ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69391540-b108-4718-90e2-61af9eb552bb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('693daab9-defc-4cf6-bb30-076ccfa4e880','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('694d66f5-cde4-4703-bbbd-82f287d79d73','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69544d36-1bf9-45d3-8261-8acbb8678f39','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6955415b-d0f2-43bb-87c6-08d2bd9d2853','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6955415b-d0f2-43bb-87c6-08d2bd9d2853','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6967a6f1-92d4-4075-8d28-bb381f7dfdd0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('698827a2-de71-4490-a98a-fbc6eeb5a8f3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('698827a2-de71-4490-a98a-fbc6eeb5a8f3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('699678e7-236b-4a53-b182-2ae91d272eee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69a00fa0-8ab8-4dbd-ab9a-e7e020de7942','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69a756c8-915b-419c-b51d-021af4fcbaf3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69b3041d-a244-4d56-aeaa-ada3d47c40d6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69be46a5-34a2-423e-8e4f-27a9c795c173','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69be46a5-34a2-423e-8e4f-27a9c795c173','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69d75f4e-b08b-42b6-8386-50dc9c87a2dc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69d75f4e-b08b-42b6-8386-50dc9c87a2dc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('69f3cb86-c624-4c98-a53d-2301945a5362','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6a029e35-8baa-4399-92be-bd370330c8c1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6a03c872-fe33-4339-b62e-b61bfb595bf9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6a0862aa-6051-42ce-93c1-999ebd0ac70f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6a21a1b7-b235-4ecb-bfba-217d02b0eedb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6a81c0c6-1586-4acd-a891-fe03595f7afb','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6a81c0c6-1586-4acd-a891-fe03595f7afb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6a8fa708-6644-4f05-b85d-ee9b84a79bb8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6aa353c8-87f5-40c4-becf-59f660ce7cdf','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6aa353c8-87f5-40c4-becf-59f660ce7cdf','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6aa353c8-87f5-40c4-becf-59f660ce7cdf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6aa5cf32-a305-4f80-a68b-99cbbc1a120d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6aa71251-6281-48bd-a4ff-0fe666e98fbc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6aace693-8c15-4b5e-94fb-50091fc81e5d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6ac3c8ab-a822-43c6-97de-a3edb57a1422','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6acc3cf8-1444-4f54-bf0d-cfd233e395ce','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6acc3cf8-1444-4f54-bf0d-cfd233e395ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6ad809b7-dd6b-408d-a161-18f2de37bcc9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6ae4d647-7490-4751-a76a-9d1ba7c8d8d8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6af12ad3-2650-448e-b9cb-a1891c0d2335','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6b16eb94-0b5e-4c02-87cd-51b9e95a2de5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6b25fd95-1b7e-4aba-b93f-68eb1e139358','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6b2770ce-49e7-4f4e-8571-dced617e54d3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6b2770ce-49e7-4f4e-8571-dced617e54d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6b2bd326-0ab1-4d00-adf0-a843cfa9e074','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6b489566-6648-476f-9b97-f082e287528b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6b500189-b987-495b-b2f8-18effc4ed7c2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6b6ea4b6-4b5a-4525-b74b-04d5cd4a0e81','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6b933674-02a5-4c24-ae1a-21fdcb4ad984','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6b94e9a0-3e12-4b91-84fc-22b1f61725c8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6bb20859-a1e3-43d5-bb4f-ab12ce4d2554','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6bb20859-a1e3-43d5-bb4f-ab12ce4d2554','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6bc0efb1-2d6d-446b-b520-cd43679bd846','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6bd666c0-00be-44c2-9998-02f8e24587ea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6be49676-ced1-4943-bcc2-9e7ea7e17001','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6be49676-ced1-4943-bcc2-9e7ea7e17001','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6be8070c-5ad0-4c21-8a77-36b2789a555f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6bfc69b9-4418-4589-8480-c0dc98cd6926','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6c01f2ce-03fc-466f-9a7d-2afb2681c793','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6c1d49c3-0633-4e5f-af6a-008efb1a8dd1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6c2177fd-588b-41cd-ad6d-d365770c6fec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6c2266cc-e8fd-4f3c-bb2d-916519b4907a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6c2e1832-1526-4a9b-9954-75488362f894','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6c44c9ba-86c3-4461-9eab-d625de76116d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6c4b1c52-7e99-45d7-a318-2fba20e4a1b7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6caf5999-1705-4353-ab2b-c7fa22b9b8c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6cd1cc8c-b956-403b-ad10-10b0e003b389','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6cd1cc8c-b956-403b-ad10-10b0e003b389','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6ce4ae6a-cce5-4b0c-a884-af5484101290','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6ce5cd14-7b1f-498b-b966-3c9107f7164a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6cfbd8b1-e033-4fd1-becf-baa0c8e37fc5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6d132795-7bf4-43e8-9865-6be8cafa05ac','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6d418ef8-909f-4f90-bbee-1a4318867c9e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6d46acf1-ac1e-499c-be4b-6657acb01e7d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6d6259ed-1244-45cd-a8f9-7a3a5cf43f47','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6d688d14-63b5-48b3-8e30-9272ffa1d157','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6d812769-f496-454f-962c-a46b708d601e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6d84493c-c199-4579-a5d4-15e791d3235b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6d8c226c-c3fe-4fec-9d7e-eaaf1fa37b4c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6d8eb0d6-9ee1-4d08-b863-ad3cf5a5cdf1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6dc0e06f-5e06-496d-b972-bb3f0918b811','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6dc0f83b-bbf9-413f-8af2-12bfea6de4db','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6dc12b0a-7c57-474a-86f6-0f8a29ea4c19','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6dc7aa40-1b47-4174-9859-071dfc6e0ee7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6dea7138-3f7c-4372-8c61-dd9795e826f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6dfd4cb1-08c0-4bb2-bb7e-c40ba65799ef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6e3460a8-a80d-44a4-9b0d-7d24a1a5926a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6e3460a8-a80d-44a4-9b0d-7d24a1a5926a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6e3a2bc2-ff54-4558-b944-423a22a8c40b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6e457af7-ae5a-4f59-a359-c5a3295a66b6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6e5781da-c3fe-4765-890c-37f2106276ad','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6e80f1fb-ef42-4409-9adf-0dbc412f7a4e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6e841148-2cd4-418c-b1f4-b4bfb3a75be9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6e8881f5-a643-4ff7-8d00-88e01374ec09','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6e88d5f3-eea7-471a-b95c-ccb304223496','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6e8b3e10-d8b7-41f2-8263-c7375c1d69d7','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6ee06bd5-d22f-4b25-95af-8680ff6f2745','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6ee2afcc-cd47-401d-b914-9937aa253cd2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6efb7576-ae69-46f2-a716-0e9d4f1d718d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6efb7576-ae69-46f2-a716-0e9d4f1d718d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6efb7576-ae69-46f2-a716-0e9d4f1d718d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6f0ca4cc-bfac-45cd-83cb-e860b5d4918a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6f0ca4cc-bfac-45cd-83cb-e860b5d4918a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6f3a07e5-e70f-45ef-b739-1c0ae4e87769','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6f3be063-31e1-447b-89c1-86fff444c3ef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6f567b7d-df76-4376-b3d3-145bc9edd32c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6f7d3b7a-8752-414b-9c2a-f66d6dd0ee99','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6f7d3b7a-8752-414b-9c2a-f66d6dd0ee99','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6f8e8262-bdcb-4109-8063-979e1f9f5411','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6fb61092-f667-4026-be97-3b2215377d03','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6fb99bb2-0823-45df-b9aa-767c718d54f0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6fcbfbde-7f25-4457-bfec-ae207fefef2e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6fd910c1-3d3d-4dc1-9852-aaf749545209','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6fd910c1-3d3d-4dc1-9852-aaf749545209','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6fe05716-9995-4b01-a849-42c0044c5220','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6ff340b7-390c-451f-96c9-46e40cba4bab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('6ff499fe-df2b-4180-81b7-38a580a0a171','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('70029e01-42a5-4262-adaf-4d0a93136f57','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('70029e01-42a5-4262-adaf-4d0a93136f57','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('70115e0d-c822-4358-b616-351f8054da59','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('70189249-2c15-4c0f-a7d7-6be2b7f9635a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('701ad907-edfd-4798-96d7-0f3bffd25d30','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('701ad907-edfd-4798-96d7-0f3bffd25d30','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('70495ac3-a905-49c7-b092-947989bfe77a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('704aa87d-0f6f-4a11-9ea3-31edbb6d397e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7055e0ca-2c8c-42cf-a12b-d99e3eb99360','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('706e7c97-cd71-4a01-a3b2-f462ff9c75aa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('70799461-8031-46d0-85e2-ce07c4747321','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('709ac6fd-f820-433d-9d10-ae6260d32f30','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('70c1edc0-7519-415d-b058-7dd6cb1c6b36','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('70c1edc0-7519-415d-b058-7dd6cb1c6b36','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('70c5646d-894f-4e60-acd5-156eb533474d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('70f08b38-aece-432a-9fa6-d9c39b9c651d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7104dc3d-e516-43f4-9fa3-9be9fd2cc92b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('710faebb-ea08-44c9-9e73-13281afc24af','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('710faebb-ea08-44c9-9e73-13281afc24af','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('711db70e-a115-47a7-b836-cadbba777824','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('711db70e-a115-47a7-b836-cadbba777824','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('712b119c-93c4-402e-b23a-0f84ae0564e0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('712de5f2-b541-4849-8e74-3f518d9a5b00','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('713c3480-dd7e-4a2b-9fa0-107ca71f6b6f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7157dd2d-e773-4dfc-a7e0-74545f1fe762','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7165f14a-fb30-46de-97b0-1145ede46c90','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('71ae1124-04f0-47dd-8c57-b41ae8711b6a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('71b49080-daaf-4e63-a504-a0edbb1d7d5c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('71ea7fc3-08b5-4772-8626-108b9a7da0dc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('71f3d604-e287-4579-ab3e-6afa3676bf84','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72016618-5d9a-4476-8d94-df903468188f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('720dc746-3552-4e3d-b2fd-bd11addc80cf','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('720dc746-3552-4e3d-b2fd-bd11addc80cf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('721287ae-11f4-419c-8fe1-5c835a8a4fbd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('722e0d4e-3d2e-438e-8397-f67c2030ba7a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72329447-b4b5-4b23-b0ec-059dbc0c6adb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('723dbd05-36d9-4be1-b959-03ebd06d9db9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('724c7dec-0d14-4d1d-ade5-2d66bb5e96af','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('724f605c-b079-4e7e-af8d-164dd98ec655','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72544a2a-0179-4f43-9428-e45f7792ac4d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('725b0d66-a694-42bf-8fef-1a74c30d1f63','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('725ff8c8-8405-40b9-9441-2d2b659145c1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72609066-ee39-47b2-a352-57bdd1a97bd6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7266f504-42c7-42f9-bdb1-25b84b9d89ba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('726eaf48-7f7d-49de-8986-53c474ce13ee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7283f07a-59e9-4fcb-959f-3c562845d65c','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7283f07a-59e9-4fcb-959f-3c562845d65c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('728c0ba1-640a-4122-b761-1690a31a5e9e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('729abbc4-fcef-424f-b489-4566fe5e591b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('729abbc4-fcef-424f-b489-4566fe5e591b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('729abbc4-fcef-424f-b489-4566fe5e591b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72a07813-6eb0-4974-b7be-354520b6d4df','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72a13c78-812b-4d9f-a504-b1bde895dbdc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72d45508-3b65-4f8e-9344-3289e0600540','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72e58965-bb80-43e3-a57d-8d04188d0bd9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72e58965-bb80-43e3-a57d-8d04188d0bd9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72e6553a-ccf0-4598-a1a8-1be88ce64c6a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('72f2a1dc-3423-4645-98bb-220ca0ac5af9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7309c17c-9b7b-4be8-a588-caad63563a4d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7314a16d-f58d-4c72-9910-d68b7d9fc8e4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('73199af1-2c8f-46e1-9de5-12b807dcced3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7326d3dc-239b-493b-aa54-411abe5ab277','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7341cd3c-7b8f-405d-bd34-4359efb92f03','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('736cc6cc-469b-47cf-8eb7-9e39b642ed4b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7379d0b2-036c-4104-bd8b-fc1bf599ad75','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7389fe26-3e36-4d58-8c75-7ec787adb205','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('739dca21-3ef6-40f5-9f1b-6fdec1604dd4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('73a89fed-2c17-4237-9837-1ea391206d0a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('73c96350-1edb-46d7-b686-4f57417b4e2a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('73d5af7e-220b-410f-9be6-58ef96c5c6ba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('73d615b9-7514-4d87-bf24-7ba223559c25','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('73ee8838-804c-4a62-8c76-bd80d5fcb9bf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('741a6c31-04d6-465a-b7c7-e2c58ba807d4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7434e9f7-8014-4d0c-b613-f972773d452e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('74366036-afce-4588-9662-fedab0402b09','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('743ba594-68a6-4315-85c0-a1fc1f049dc2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('746bf3ca-39a0-4d84-9809-13a6ad7082cf','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('746bf3ca-39a0-4d84-9809-13a6ad7082cf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7478e61c-8675-4f93-bdf1-2d039c3e4cea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('748410ef-4dd8-4ae4-acd3-20bcb9ed205e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7485c606-a58a-41a2-b42a-823d06021de6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('749caccd-e9a9-4f99-a057-ccc7a6fd4e38','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('74abacfb-0669-40b6-8e22-5db5dd2ed09a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('74d094f7-f344-4660-8355-9cbce2bec5b3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('74e3ad73-6cc0-4031-bc9f-f518a3c87be9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('74ec73ea-ebdb-42ac-8a24-d206997e1a40','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('74ec73ea-ebdb-42ac-8a24-d206997e1a40','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('755396ec-7b31-49f1-902a-f3cb0fe12109','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('756fae13-b182-4648-8993-9127833b37fb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7572e067-86cf-4d11-bdc2-80b3e1288a3b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75745e7e-7476-4dfb-8810-f9bad11b51e1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('758da01b-6c20-441a-83e0-4680fa71de17','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('758fe4f6-d056-4075-acf5-00de5b211cf0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7591ea30-e6cc-4ed5-9f6a-9cfb675e1433','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75a9e3e6-3054-453c-8d39-64caa7e6509a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75b296a7-14a0-4135-864f-f1f50a5ac7cd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75b296a7-14a0-4135-864f-f1f50a5ac7cd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75d19f18-636c-44df-a598-e2dc1c3c40be','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75df805e-62b5-473d-8ff4-d6d7499abf18','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75df805e-62b5-473d-8ff4-d6d7499abf18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75e64277-2821-4791-87ed-6a42f878f661','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75e92839-20bd-4fec-89fa-864c71e6c1d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75f2ef3e-53cc-434e-8980-a2b040d48b3f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75f8cfed-e1dc-4202-83a3-a08ae8d7c497','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('75fb1587-b02c-44a4-a238-47fadbecd1c9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('760451bc-563f-4c20-9be3-5c5b3795223b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('760451bc-563f-4c20-9be3-5c5b3795223b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('76149b6a-ee8f-42ad-9a73-7396524ef6c5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('761bfb29-0113-4ecb-8d68-2ed1fa09eb48','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7626f95d-3435-4903-9b02-059cd9008ab6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('764b6eff-d53f-4f2d-aa57-e314db4e8ad7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('765e9c48-d47e-4ac6-a70c-ef540463cbb8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('76624c07-d7da-4796-9094-78fe3e2cbc9d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('766c13a8-f2c3-4220-8bd0-330542e1a42c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('766c13a8-f2c3-4220-8bd0-330542e1a42c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('766ce5db-c040-4b48-ba29-6d2ce7867ca3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('766ce5db-c040-4b48-ba29-6d2ce7867ca3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('767a4a73-43b1-4d17-ac57-2f6d1aa17f58','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('768f8f18-5ff2-4ef9-8b83-812a0f910d12','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('76982257-9eb6-446c-94f4-4aeb3897f3a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('769a5a31-90cd-4f49-8215-d6d90d153504','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('76af8e79-b1e2-43ad-8101-34291cf94caf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('76b967ef-e92d-4d05-b69f-553f6930b45f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('76c172aa-7d41-4afb-b43e-6d92163ed2e3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('76ce3b56-0e5c-4c62-9a7a-ff2ae5abe538','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('76ec2398-5e55-4a9e-b081-2fc331df41eb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7722b596-e523-4f4c-aa87-d56af5bad58e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('77275ba1-b7bd-441a-acc2-aff500e14b3e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('772a40c1-be80-49ff-8a89-a54c6adb9ead','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7730b847-f73d-4f13-bbe2-1ce303ae3b91','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7730b847-f73d-4f13-bbe2-1ce303ae3b91','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('774b95b5-f74b-4526-a226-3d353ab434c4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('774b95b5-f74b-4526-a226-3d353ab434c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7756660f-e73e-409a-a02a-22b73388c893','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('777736a2-08ff-4d3e-8c47-acba0fc15103','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('777e90b9-4baf-4d3e-943d-4e55b69260db','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('778421bc-00a9-4b3b-96d0-7f6c2c8b77a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('779a7daa-134d-4965-8b14-1b48aa709c0f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('77bc5e1f-6980-43ae-8ce6-4dd047ceeb9b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('77c09fd1-268e-4a2b-9c41-81fbca58c336','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('77ca0758-153e-4c0e-a61a-29df7b9c22ca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('77d9e7f1-127c-4ec4-9a70-50bc06b79114','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('77e68de7-7fb7-4939-a30d-ef2af92a19f7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('77f6912b-04a1-4ae0-88e7-67e7e941232a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('77f96783-122f-4a4b-97e1-92f2dd41add0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7827a14e-9311-4f8b-bee0-ba8f5244faef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('787de95b-f50b-44b8-ba97-e0807586fca1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7887fef6-f2fa-4c6f-b26c-241b8cf746c3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7887fef6-f2fa-4c6f-b26c-241b8cf746c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('788d7560-ed37-4989-8e8b-390d36658336','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('788d7560-ed37-4989-8e8b-390d36658336','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('789032d3-de7a-41d0-a1fd-0534afb09bde','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('78a00930-a7be-4cb3-b090-e06e498f5f5f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('78a0b252-8a17-4e94-96e3-04df10a70900','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('78b09e53-b542-4e6f-93d9-c2c2b58278ec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('78c0e3f7-f618-4b18-a521-82ccedb2f4dd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('78c0e3f7-f618-4b18-a521-82ccedb2f4dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('78d1c124-4599-4b81-9835-ee33472de171','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('78d8b206-a7ce-4bce-ba8b-8f71a16c17c2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('78f9292f-3458-46ec-b4c3-dbfb53348fbd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('78fc648f-e414-4348-b86b-4f36c7647bf8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7908b6d2-b8d1-4d35-867a-d5319903dc80','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('790ca15b-79fb-45b7-9b34-f4e1199104f0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('791a3cb5-4e64-4bac-9950-acf90e0f256f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('79351235-44b6-4931-b79c-c98b6a909bfa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('794452d7-904f-462d-ae11-7765f0f666c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7944b3e4-b07b-426e-9491-d694964e4793','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('79584820-7451-49d7-a3a2-71ef9a5d9aa3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7968c820-0967-4b2d-82fe-aea3f63c7a40','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('796ef926-5281-4065-bf10-20c5859d1c3d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('79b65ac7-729d-4b0d-90a5-cf4e970c708b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('79ca2341-83ae-48f4-8739-bb91ef2aa9bd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('79ca2341-83ae-48f4-8739-bb91ef2aa9bd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('79d59b7e-586f-4b5e-8151-9f50d19a0648','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('79dfe973-94e4-46b8-8493-1c5fc286c260','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7a0b8f26-e240-4e8b-9f09-ab76d26006de','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7a496fd2-ce2a-4388-8904-0d2be8a3b38e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7a7251fc-c2bb-4096-8cba-7db28c725726','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7a8cfbd6-e4df-419d-b1c5-6db5aba2708e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7a8fc970-c832-4b3a-9e1e-39265f65af68','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7aa118a8-a2eb-40c5-bb56-7f8363b0a95a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7ab2a160-62ef-401f-bc9e-409f10601289','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7ac1238d-8a94-45af-85e8-49fdf4d5f380','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7b007ba3-dfbd-4102-a638-518e5a53111e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7b1ca948-a852-46c1-89e8-a99218f40706','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7b3f1455-f21c-47f6-a18e-6410c476df8a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7b48d6a6-dcaa-45b6-b4ac-8f38e702db42','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7b48d6a6-dcaa-45b6-b4ac-8f38e702db42','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7b5f05d5-d5ad-4185-ace3-f4dd5305aeb1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7b612f16-3e17-4ab6-af8e-599021912257','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7b612f16-3e17-4ab6-af8e-599021912257','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7b7f64ad-7730-42b4-9a48-187ffc78d36b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7bac7736-1d80-4bbf-a2e7-35150f55d4f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7bd22250-9183-4fdc-ae83-6ac85083cd21','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7bd22250-9183-4fdc-ae83-6ac85083cd21','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7be1b840-9677-4f0a-82cf-0af12609c7e2','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7be1b840-9677-4f0a-82cf-0af12609c7e2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7be9622b-78fe-4046-84e2-e0506b3bdb62','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7bfb37be-17cc-4c1b-aaf4-add3a0a4236d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c026137-ffa6-454f-bc35-2e3253d5e7cf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c03a003-0fd2-436c-be52-fe3e43807f8f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c0bc201-2449-4620-8e05-d047164753bc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c0bc201-2449-4620-8e05-d047164753bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c251097-26bf-4cb0-b75e-52d71465a259','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c251097-26bf-4cb0-b75e-52d71465a259','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c4f7709-7387-416f-9939-6999d54ca092','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c4f7709-7387-416f-9939-6999d54ca092','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c67768f-e55b-41df-a129-dcf2c13075a8','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c67768f-e55b-41df-a129-dcf2c13075a8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c67a50b-a99d-4bbe-afed-708a339ee6cc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c67a50b-a99d-4bbe-afed-708a339ee6cc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c793bd7-6202-4920-aa8b-b945fb32748f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c793bd7-6202-4920-aa8b-b945fb32748f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7c7e4292-ddc2-4e43-8cd6-817c824213d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7ca1f7c0-cde3-4419-8ed0-acbccfdd800c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7ca4b0b0-98a6-48e7-9f7f-986b84b97ce6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7ca99805-2340-4c2c-a1f4-1b1518fdcabf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7cba242d-d994-453c-be80-ab8db08a2a15','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7cde6374-ccba-4fb2-acee-02f084c31cec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7cfaa47e-ce6b-4ea1-8aaa-9f4ced060702','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7cfaee80-5c59-4866-b8f1-e4bb2cb20f3b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d073a5d-3525-4205-b552-fb0e21c94a0b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d073a5d-3525-4205-b552-fb0e21c94a0b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d1e8684-2b18-4646-b7e9-9750a12d21e6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d221337-533f-441a-aef9-85bddef49565','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d2f378a-bb34-49e0-8570-e4e25a379bff','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d32afd2-7fc5-4d1e-889c-cb14b00953a0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d353866-5233-4b92-9e05-957016e83dc8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d394f68-e8ab-412d-9df4-dae4efcfda99','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d5e5d2e-6cad-42d4-85ca-c3bd07253d34','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d7cf760-8326-46fc-9b9c-388cd58b97cd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d936cfd-55a5-4f38-b851-1f08e36b1df1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7d936cfd-55a5-4f38-b851-1f08e36b1df1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7db0541a-9d9f-4d49-9494-344bf914a2b0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7dbb9acc-5b91-41e8-87c2-ff74c604d662','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7dde11b3-9d90-40b5-8358-a8b75f976fee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7de49561-07be-4a70-9ba2-ee3e33f4909a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7df27bad-5459-41f0-96a0-a841cb746411','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e02174f-1194-41c1-beb4-578bcac0b4c4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e02174f-1194-41c1-beb4-578bcac0b4c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e167322-eaf6-4a26-9906-03ef2804f4d6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e17d323-d6c1-4dc9-bf8d-9c460c3abb2e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e17d323-d6c1-4dc9-bf8d-9c460c3abb2e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e19c137-a055-4a19-9cf5-6bc6f71c0b5b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e19c137-a055-4a19-9cf5-6bc6f71c0b5b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e2fd85b-d3bb-446b-9a0c-a52951271339','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e2fd85b-d3bb-446b-9a0c-a52951271339','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e307176-8eec-4838-b9c7-b4bd54c68546','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e3cee7a-d7ae-469a-a202-1b2f5b1ee40d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e4c21f6-c185-45ee-b9a7-1a504aa04d97','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e4e8bb9-9536-49b6-affb-d30083c72327','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e4f2e21-9255-4809-8a10-42332155af51','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e53e653-d782-4d56-8cf1-d363ef4cd2b9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e53e653-d782-4d56-8cf1-d363ef4cd2b9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e58a634-c98c-4b71-a351-de32c1f0b4bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e62c752-64a7-4d02-82d7-1bde5edbf536','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e7599cf-840f-486c-ae8e-d869eef8c51f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e7599cf-840f-486c-ae8e-d869eef8c51f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7e8fefe9-0bff-4e78-a4a7-c4b45560d931','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7eab5433-86ed-4a91-9811-fece728e69b2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7eab5433-86ed-4a91-9811-fece728e69b2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7ecf69eb-5a67-4588-a736-2f8b0b688331','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7ef08854-bc6f-4afa-bbe9-6990ff526c7a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7efa1b5d-aed7-4586-b90e-7094e95d271f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f03273a-4ee5-4cac-9cab-215b65d597b0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f039c96-7cca-4a9d-935a-7c5c94e644ed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f0890ca-12d6-4595-874c-6eeca9971b82','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f1e8791-efdf-4478-90ce-251c667216eb','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f1e8791-efdf-4478-90ce-251c667216eb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f311761-6f9f-477f-ab4a-ad4d83203a34','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f5cee71-2005-49f0-b3a8-d62839349bea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f6b80ce-c4f0-48d9-85c7-a010c7737c45','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f75c6ab-f8ec-4008-a29d-0f6501386115','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f808bde-282a-402e-b969-a1f0da9d8e36','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f8f70a3-7faa-4c8f-b6c9-1a982b8d97a9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7f9ade68-c0da-413a-8dca-d18786b0ffb0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7fa54ca7-630f-4eb9-9cc0-be4881a3fa2d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7fa83f6c-cbc5-459b-86b5-17c0405ff57e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7fabf331-b1a4-4525-ac22-29b9031b2db5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7fb31608-a857-4677-9cc9-1dfb268b7e63','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7fb31608-a857-4677-9cc9-1dfb268b7e63','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7fbec356-8b8f-4192-8fb3-578ed671d14f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7fbec356-8b8f-4192-8fb3-578ed671d14f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7fe60254-3513-4c00-955f-4a22bad00083','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7fe8d564-feb8-45d2-98ae-98bff4831193','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7fee2eb6-0100-4a85-8444-03672c51e21e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7ff71b20-6e8e-488c-9446-490dfc678587','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('7ffcbc15-4543-4569-83a3-ecc2d60f4284','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8010a76d-7a11-48a8-b434-40041d288f69','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('80132646-4e37-44ce-b74b-d14be43a1d07','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8014b64c-bd41-4c56-8c73-f0ce8b379d06','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8014b64c-bd41-4c56-8c73-f0ce8b379d06','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('801d7456-586c-4c33-84c8-3e24c3ed2ef9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8021fdc4-0a52-471b-8552-458fad17b93c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8021fdc4-0a52-471b-8552-458fad17b93c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('803c0a54-4714-4f9d-892f-d80927ab75f0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('80413f2b-d4d5-48c0-968d-1a993b9c673d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8049b524-e5e0-4599-9fbd-98835e276c81','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('804b4837-943c-45c6-b504-fc9fb43c18e5','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('804b4837-943c-45c6-b504-fc9fb43c18e5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8050f78e-95bd-4f97-876e-e143eb94a997','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8052b4be-2f3f-4a08-9f27-f52b89ed32d4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('805804f0-4a62-4ebf-8c85-db45fbec1c95','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8077ad11-e91c-4691-a815-841c5c77c27b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('807c54ef-1588-45ff-a2fa-f7db188fd51a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('807cf464-b739-44ad-9e01-5755a438c34b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('80b9686d-23e2-4b1e-a9bc-b6315161573c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('80b9686d-23e2-4b1e-a9bc-b6315161573c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('80bcf5d2-47cb-4853-8c8b-f46fdf1486e5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('80bcf5d2-47cb-4853-8c8b-f46fdf1486e5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('80e18690-9547-4063-b3e1-8f8f1a266f2a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('80fe0bcb-2fc0-4bb0-92e0-9b1fb7adf25c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81121aad-0dff-46ad-87a6-e5b28b20f19c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81121aad-0dff-46ad-87a6-e5b28b20f19c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81158f0f-9d66-45e3-b97d-2d006f2a8019','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81158f0f-9d66-45e3-b97d-2d006f2a8019','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('811f14b1-48be-4fb9-8c55-0a89e5809f7e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8156f1f7-5e3a-47c3-a738-da78e38f3064','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8156f1f7-5e3a-47c3-a738-da78e38f3064','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8158033e-dcd7-4e79-a7ec-421b335a807d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('815f880a-1e3e-47e5-b1c7-783398d3fd54','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('815f880a-1e3e-47e5-b1c7-783398d3fd54','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8168120a-e216-4c99-8602-a90d0a37a8dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81778287-262c-47b3-afc7-fe954eba5966','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81778287-262c-47b3-afc7-fe954eba5966','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('818d4b30-dd17-4a12-9a65-d8f60c5f80f6','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('818d4b30-dd17-4a12-9a65-d8f60c5f80f6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81ab338a-2222-45ae-99c9-083483a481c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81b5a18a-7aad-4547-b512-8856e2b55c83','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81b5a18a-7aad-4547-b512-8856e2b55c83','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81bbb997-8c4d-4ca5-928e-bd9f2772d323','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81bd246d-24fe-43fc-9ba2-1dc2e7acb167','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81c8f426-bada-4082-9001-c88d4cc4be8f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('81dbcf79-a0d6-4e75-a58c-c4d27f9f767a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('821497a2-96be-421f-8dc4-a64ee082432e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('821497a2-96be-421f-8dc4-a64ee082432e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8237c459-f433-4e34-aa9a-cfdf79b7b2ca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8240d246-35f7-4b07-a3c8-aeeffda124de','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('828cdfcf-fe24-456b-bd3c-8c3719027db4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('828cdfcf-fe24-456b-bd3c-8c3719027db4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('829bbac3-b447-4d0a-8b89-6b41652c1001','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('829bbac3-b447-4d0a-8b89-6b41652c1001','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('82a1b5af-ee8c-4810-80bd-932478011c23','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('82d836f6-be69-4c8c-a28e-9146ffa7f080','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('82dd82bc-de3b-4cbb-9f43-64162aef2c57','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('82e4b034-d4ed-41d1-b6cb-f952b5d6586c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('82e6cf29-ae9d-49e5-9d58-f4d6955c93f6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('82ed42db-8bb2-40ab-bac6-0478dfe9942c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('82fa0ed7-0850-47b7-a8d6-c3d2fdd549e3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8309aabb-29be-46ae-b3d2-4a6f6a4af7cb','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8309aabb-29be-46ae-b3d2-4a6f6a4af7cb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('830a9a36-1f84-4aa9-8431-c9b0bdf4d66b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8312037b-bbb2-4c84-bad0-3b058b9f9666','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8347bb04-5452-4a56-90ff-27578da3c90d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83663888-53a9-430c-829e-dfd0fd204e9f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83800117-4000-4ca4-8a52-812a5b82ad4e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83889ab6-62a2-4154-b77e-39c62c421c2e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83889ab6-62a2-4154-b77e-39c62c421c2e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83954708-e9fb-4328-8aeb-fbba1ebac5b2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83966a53-3350-4e76-a863-8c640cf2d0ad','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83a42948-0f6f-46e1-888b-459b587846b2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83af5d16-81b4-4fde-92da-3719c83210f3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83c91de4-2539-47a7-9caf-bf698d0c5666','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83c91de4-2539-47a7-9caf-bf698d0c5666','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83ccd01c-c3f2-4dc8-90b8-793eed41c5fc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83d2d2cc-964c-4366-8d84-cc800c9bcba5','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83d2d2cc-964c-4366-8d84-cc800c9bcba5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('83f272dc-3fd6-48d2-94f7-af7270b321e0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8423c8a9-6b39-483a-aebb-9d7db9ad8310','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('843cb83b-e724-4bea-8b4c-1e5b7a4fc853','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8440aeb9-efeb-4031-835a-877ba07b84c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('845db1b9-f324-4da4-902a-bfdf15d1acd3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('845db1b9-f324-4da4-902a-bfdf15d1acd3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('84702ff9-1e0f-4f18-b61d-b84f578f6aa5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('84bd9ace-1df1-4ba7-b63a-324ca3ae7dd4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('84bd9ace-1df1-4ba7-b63a-324ca3ae7dd4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('84d49177-4760-4693-9a3e-ae1f7312d8b6','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('84d49177-4760-4693-9a3e-ae1f7312d8b6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('84d49177-4760-4693-9a3e-ae1f7312d8b6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('84dcef05-0d87-4689-8166-d5723b2f6cd9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('84e09be0-e4b7-4360-9c88-74a14ecee98c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('84fb3f74-2734-43eb-9892-a0e6573000d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('850f0db8-de64-4fbc-9d20-312548b312b7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('852177e5-7c61-4709-abe5-493acaed93e6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('85304599-78e1-4654-997e-2406bc764bec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('853a6a8c-bce2-4bf1-9871-3188ec015ac4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('853a6a8c-bce2-4bf1-9871-3188ec015ac4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('854a6944-4709-46aa-9dcb-9a32fe0e5a74','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('855550c4-a268-47b4-b4da-b2b9652f1cae','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('855550c4-a268-47b4-b4da-b2b9652f1cae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('85637d08-0aa5-409d-b2d6-f3efb8ecdfa2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('857d9b0d-1087-4a39-b68e-d16344d31420','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('858d2fb3-a554-4428-9827-9becfcf7ee1c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('85920345-ce01-439d-be45-c0c306e57099','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('85920345-ce01-439d-be45-c0c306e57099','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('859d56d8-8211-4b75-99d0-de2cfc522294','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('85b8ebea-5a3a-47d9-9972-f20131bb03bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('85c0876d-0208-4d77-9514-6ff318d11762','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('85ff0b04-1838-4942-bf74-4942843f332b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86042ef4-84f3-4a71-a524-f0cba17355c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('860b2153-baba-4dd0-b561-558988be957c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('860c5cf2-4dc3-40f4-a052-ebb9d0f5d38f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86163f29-9e81-4e03-9d8c-db300928c13a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86194ec3-aeb1-4708-80be-be020ab594a1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8643b53a-9641-46c4-a30a-ac5bef9bd3e8','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8643b53a-9641-46c4-a30a-ac5bef9bd3e8','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8643b53a-9641-46c4-a30a-ac5bef9bd3e8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8650fc95-b671-4518-9d6e-db0d62b9d94f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8655f62f-ff78-4e20-bff9-6740ead7ba1b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86924721-2607-4ca3-9959-5177548fddb9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('869ac813-26c4-41d2-bf0b-7213d2e1dc6d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('869b45b0-fe9c-4f70-b7a0-26f63fdfc695','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86aa8fdf-2070-4a43-a612-f9eda7181890','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86b5f73d-1a99-4680-b77c-148ec80e2fb6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86bd2748-e554-4c87-b5d2-6f9309299d51','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86c67a89-9f0f-46ba-bfa2-258cb87adbcc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86d422f3-6a0a-4c8c-9b3b-1caf722c1edc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86e33fbc-9948-4af4-ad53-9b030f8f119d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('86f5b131-a7ae-4d20-9dc3-a75a6078bbc1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8713175d-09e8-49d4-bff0-f196222291b9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8713175d-09e8-49d4-bff0-f196222291b9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('87159da8-6ed0-4437-b29e-bf4b7232dccb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('872f3d81-3bc8-42d1-ac7a-3e653c7e6080','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('873790ee-96ba-4890-b7f6-92a481e0ab1b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('875a5af7-00f5-4573-b0df-9b39751948da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('876eb6bb-1bf1-4441-8247-dd1a7feca27a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('877476d9-6d7a-498e-b714-69aaf4da5686','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('87763b39-6677-4468-8bec-7e1770a22cb5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('877b8531-513d-4f2a-ac9a-1f8ea9f0a8e0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('879f7e17-21c9-490c-be2c-946ced980289','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('879f7e17-21c9-490c-be2c-946ced980289','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('87a1c1f2-6f2b-4cd6-9393-ae64ac115483','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('87a3b609-60c1-4677-9067-c2579da2b905','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('87c3fcb6-894b-47d4-a62d-a05ffe2c6472','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('87d19ed8-5ad1-4bbc-9502-ea8dbf239206','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('87f943b1-43d2-45fa-b54d-1521b0d2a852','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('88276cd4-2c4f-4d32-b4be-fa749dcd5671','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('88276cd4-2c4f-4d32-b4be-fa749dcd5671','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('88282ba5-6d71-459d-9aef-46e2b447dfdf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8830454c-e3ff-4b22-a10f-19869ac37840','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8831f82e-f3b6-428d-96fa-8ff0e999cddb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('883fc71a-c9d5-4620-a4c1-4144797c7ce6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8840dc34-b551-4faf-9d3b-39314d5ad459','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8840dc34-b551-4faf-9d3b-39314d5ad459','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('884320c8-7ce7-40ad-84b4-8c7bbfdfcdbd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('884320c8-7ce7-40ad-84b4-8c7bbfdfcdbd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8843bd57-1be1-4791-b9c1-fe8c7fd1e4e0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('888bc328-e883-4257-89c7-70f89539179c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('888e1a12-d55c-4c78-89d8-50f48fb7161e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('88a25791-5639-4fbe-908c-6bd461ede68d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('88afca2c-8163-4d76-9a3b-87c1968c0072','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('88c32684-298d-4a53-897f-0df52ec7b1a5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('88c620f0-0f40-492e-9dd8-692984fe8f56','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('88fed1bb-308a-4ce8-8d26-7a33d65039f6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8916213b-3e9c-477f-a8fb-3958c8483026','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('891ba257-d701-4799-8b65-03d7bb3d343a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('892e5db3-5db1-47d9-83ca-1c1fc1c820ae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('89401561-4a38-4224-8766-05993b0ba3be','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('89536cf0-fb4c-45a8-8c23-33989dc71dd1','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('89567f27-1231-419c-9b11-608da018da9a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8963fe81-4b72-464c-aa9e-686fdd7b5b36','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('896541a2-8521-4a70-88ba-8fd336cf24a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('89896f31-7302-4b2b-93aa-a0255172ddb5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8995ea9a-dd5c-486b-89b2-182726c23360','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('89962e1a-70a6-4c3e-bdf5-8ac83c425a68','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('89a645d8-8db0-4823-a956-174828bede37','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('89b77c40-cd07-4f9d-a02e-98ac576a37b5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('89cbe303-b0cb-4e52-8757-89d9c861507c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('89e93fa1-c378-456a-99c5-34b91b565bd3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8a1b034a-96c3-40c8-bdbc-f5505f3496f1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8a1b034a-96c3-40c8-bdbc-f5505f3496f1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8a2c602c-9f8d-4cb7-8362-e4a299fab1e0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8a32f5c4-fbc4-4ed9-acee-50ae6c09fd35','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8a444a0f-d220-4389-831a-bd6f53a38378','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8a444a0f-d220-4389-831a-bd6f53a38378','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8a75a164-599e-458a-bd38-cdc1809b089d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8a8ca6c5-f06a-42c7-b4d5-a6a2cb8bed1a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8a9ee429-8a97-47e2-b0c7-cf38d69e86f7','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8a9ee429-8a97-47e2-b0c7-cf38d69e86f7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8aae9cd0-972d-482e-b097-a16abdabc095','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8acbe449-b9ba-48ec-a083-c6809dd38852','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8ad3d4a7-064e-487a-b70a-1fbf94c33c33','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8ad82620-425a-454e-8db4-fc4ea69aa71c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8ad946ec-c0d3-43ae-984b-73732ad3700c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8adbbb2a-fae9-475d-90db-b2ec443b2495','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8ae7d6ff-4751-4fd9-bf15-1f5a42f8c21d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8af00ba9-5a4b-44f1-a139-28e61be9d236','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8af2689b-1c21-40c5-a07e-453dc7e3e7cf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8b0c0bab-21b9-4ca1-8049-a072e0d42aee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8b16fb8c-bd28-4d7b-8dd1-3952bcbf7649','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8b34abfe-46d1-4989-9c2b-11ba7415b975','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8b3cf40d-3d19-4708-b83f-cc009c6683ca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8b65971f-6223-4db0-98d5-549203b7d18e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8b665110-3c08-43df-9734-5c9493ec8c94','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8b6f1cf5-188f-434c-8efe-e103e8f7b62a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8b6f1cf5-188f-434c-8efe-e103e8f7b62a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8b848759-f5ac-4d9f-8a36-999e2d906af6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8ba9d80b-d084-4fc5-bd39-e0eb63bb94c5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8bb28119-247a-4d9a-a2c7-3eefc89e9f8b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8bbfc7d7-9a43-4fb1-b604-b68d5df53699','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8bc6ece5-6466-4093-b86a-517fca7beafa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8bc99f22-0028-47c9-ab51-3fc56774a453','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8bd721dd-ff79-4f6a-8579-4d23d79fb2c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c04d45e-ddce-4f3c-82c5-17081e561329','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c17735e-e13e-4bae-9d16-cf77f72580d3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c17735e-e13e-4bae-9d16-cf77f72580d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c187818-b8ad-4dbf-85c0-7ed93c565f73','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c1a3342-4198-42f4-a2e2-f77a848d8630','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c2c5596-0ffd-4a65-b916-3f29eed69fcd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c6451ce-8704-4ef2-8b4a-9259b652a15c','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c6451ce-8704-4ef2-8b4a-9259b652a15c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c6836c2-534a-4c1e-abe7-e9d433613b98','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c6836c2-534a-4c1e-abe7-e9d433613b98','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c6f7841-d42e-4896-b730-40bfcc34087f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c85197c-7f79-45b3-a3e9-8a6bd26364ad','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c85197c-7f79-45b3-a3e9-8a6bd26364ad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8c962b98-2db7-4f1c-a79c-d109039c5f0f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8cdcbfcd-7c2b-403b-b1ba-5220906b87d2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8ce849a6-4f90-44f0-b9cb-ac1cbaafaec8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8ceff01e-cda4-4761-9f49-261cfef54a2c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8cf8fe88-8246-46c7-b415-9d61509d0927','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8cf8fe88-8246-46c7-b415-9d61509d0927','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d0fbfa3-d674-4f03-86bd-d7c5784c9e5a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d0fbfa3-d674-4f03-86bd-d7c5784c9e5a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d171b94-a999-4f64-a6d3-c89469d0ea0c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d4c9db9-1b97-4bcc-a45c-26e7bcb33299','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d60755f-729b-4643-b974-9cdd14c8cd84','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d885f0c-0e8f-4203-a427-faf30bb8b8b3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d885f0c-0e8f-4203-a427-faf30bb8b8b3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d91d335-956e-48c4-af2c-2605e02974d0','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d91d335-956e-48c4-af2c-2605e02974d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d95b8ca-a1b7-42b6-abf5-2de6ad0c4e3c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d972dae-38e2-48f8-8d16-ccc94709125c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8d972dae-38e2-48f8-8d16-ccc94709125c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8daff274-1d35-48c2-a553-d127f93c6613','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8db72034-9e7c-4d87-ac4e-695a0eee7712','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8de0ebc5-eac9-4597-938f-b803acfae5aa','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8de0ebc5-eac9-4597-938f-b803acfae5aa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e1b38f0-a1d9-4ccd-ba3e-38a2a65e62d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e1f33a2-8cac-48ad-8431-2d67c9fc37be','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e1f33a2-8cac-48ad-8431-2d67c9fc37be','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e226f8a-cf7d-41e2-b9b4-0ba9d1337776','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e41ed53-6cd5-449c-8fd6-c27d504e9307','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e4461f6-38f2-4f11-bb21-0893b8eb90c5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e465ee9-91da-423d-9587-7c7bfb0f175e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e78f8e3-2616-4ad8-a948-8cd733b1bbe2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e7cd60f-99fc-4d62-a4d6-3cb113d14089','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e864d34-c850-437d-8b33-5647c640192d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8e9d6e8b-3846-428e-8fe7-5299969f7673','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8ea4d3bd-621d-4bac-b43b-d2b105d55b3b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8eb82032-e786-42de-8f12-f5dbf49cecbc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f1b2730-dc85-41db-a6ad-36d401e56411','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f352371-ad34-4318-bd12-ba8e3bc7881a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f48aa42-29c9-40a5-9101-74b4496fbd85','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f53b05a-1a1d-4b68-8d03-adfcec78e930','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f54b0cb-09ed-4f12-ac5f-53fd0bee8641','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f58d4e6-99ff-4096-8c80-17f6f2d5d9bf','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f58d4e6-99ff-4096-8c80-17f6f2d5d9bf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f61f746-6a12-40ec-99eb-2e3aa38cee28','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f68bd06-185f-468e-b961-688cf6349fea','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f68bd06-185f-468e-b961-688cf6349fea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f6a1499-f490-4a18-809b-f062f0a8d540','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f6ea5bb-faea-457b-861b-1cca6dd74391','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8f88bcdc-c51b-492b-bd78-dd2b1c748cc5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8fcd32a4-88ab-483b-b89f-35807b859b04','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8fd7c3b0-6984-4a3e-833f-101d689987ba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('8feaaff9-9b49-43f7-aa1a-bb4f946b081c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('900293ab-0284-4c8d-8bf2-844582b5cbb2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9007e52e-14c8-499d-8003-6cc66e4ceab5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('90183da2-4bb4-4fff-999d-3c7d19f0daa4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('90183da2-4bb4-4fff-999d-3c7d19f0daa4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9019098f-d8a8-4311-959e-e39acfea05c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9025d768-fe64-4517-a741-d9ab9d49679d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('905ff852-ffae-47f4-9932-16f915dd2823','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9064074c-5341-4812-a75c-3617dab22ba6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9064074c-5341-4812-a75c-3617dab22ba6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('906ed48f-b400-4f39-8bac-4cc52449a39d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('90771790-cacf-4a3d-b8c7-b51873310acc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('907d4058-ef69-4b26-969b-b54961d78079','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9084c446-9f07-4cd7-b2b8-e95fc02c592a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9084c446-9f07-4cd7-b2b8-e95fc02c592a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9085be95-c762-45be-a942-1645c0a258eb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('908b16d1-8bab-46fe-be27-c5fc5bba400f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('90903409-f53c-495b-af19-43d41572906e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('90bc216f-7779-4812-9b5d-0729c471a72f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('90bd38fe-f37d-4c2b-9bf9-1df3020bd02b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('90dd8f44-9dc6-4d6a-a5ec-ed30e6b14426','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9114ecfe-48fa-412f-9743-888dc0fccebf','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('911dd5b9-4200-40f1-899d-9c5ba8fe319d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('91445be5-08d9-4c57-9b6e-368d1da430e9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('914da13c-39ed-4cf5-9163-ab485029fe08','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('91503c7c-574d-472a-955a-88050cfd4d3b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('915aecf7-f956-4393-8396-4eb673f93005','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('915aecf7-f956-4393-8396-4eb673f93005','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('91781f44-5ac3-4ca2-944d-747720754fcb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('91c23bae-a70f-4ab6-b978-c47046d52085','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('91c3eb80-0d42-4f6b-be7b-68dcf0b14cce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('91de2112-79b9-4a67-9530-0d4a81ca2952','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('921727ae-9323-4e00-8a6b-60e15e1635d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('923c37f2-be4f-4b74-8460-859e8ca47f0b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('923c37f2-be4f-4b74-8460-859e8ca47f0b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('926ee8ea-76aa-429c-a682-f44fb801b799','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9283986f-45be-4130-ab6a-ce4a73490104','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('929aff7d-f62b-4ec2-a5f0-a7d9169b45d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('92af625b-a06a-4f9b-a99a-82f5c0037630','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('92af625b-a06a-4f9b-a99a-82f5c0037630','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('92b557e9-9298-4729-bf9a-b6cbfc58ece7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('92b8811f-dc60-4436-8e55-3e5fa5726d4e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('92bf1322-cd29-4720-84ad-b23892b9da78','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('92c448d2-26de-49bb-ab0e-7c361cf40d6e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('92c6c157-0c92-45da-8aa6-c64c9d6096de','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('92d5e3a1-0d78-42d8-818c-17e2858f3c1e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('92f6d3a4-455e-413a-8c05-0ac3482b1591','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('92f6d3a4-455e-413a-8c05-0ac3482b1591','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9328d6a5-3715-4368-869d-5c686d2a0c6f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9328d6a5-3715-4368-869d-5c686d2a0c6f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('936004a0-15a4-44ad-b54e-5d999784485a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('936eedba-569a-4561-b592-194419913305','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9377d103-e93b-4377-b49b-6476c8236f60','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9377d103-e93b-4377-b49b-6476c8236f60','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9379e94e-756f-42f3-bb9f-9e2c107df24f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('939591ac-2b38-4ceb-a1ea-93276087f4f7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('939f797e-24ef-4b75-9f65-123bab10714e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('93bf7d09-6701-4edf-8ed8-118ed5e68b50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('93c1b276-2e28-446d-94a6-f42295e6209b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('93c210c1-d764-4330-ab61-2680d8b2200e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('93c210c1-d764-4330-ab61-2680d8b2200e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('93ce4ecb-fcb9-4a9e-81a0-fecce6c910f5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('93ce4ecb-fcb9-4a9e-81a0-fecce6c910f5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('93ce619c-a9d6-4ad5-a619-e573545819d8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('93e15551-4cf9-40f9-b675-8b4ec708ac60','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('93fb5912-7cc9-4c70-b845-fb883755ee19','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('93fb8e02-85be-4aa1-bca4-936e133ab9b4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('94223e9a-c365-4875-9a13-724ae466c833','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('94275b6f-73a9-419d-8869-d5e671b15f76','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('944dcfeb-462e-490e-b702-98b2d7d80122','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('944dcfeb-462e-490e-b702-98b2d7d80122','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('945d070b-58d8-4453-b2b0-b51db0cfada5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('946e11a2-e65b-41d6-ac15-fdb1cbb9d577','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('946e11a2-e65b-41d6-ac15-fdb1cbb9d577','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9492b10c-c862-4e85-ae28-847b0ac567d0','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9492b10c-c862-4e85-ae28-847b0ac567d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('949f801c-c28a-4576-a0a3-31d770fb67fe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('94a5f7e7-9231-4211-99ee-d79c2311949a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('94b73904-f7c8-4b4d-834f-18e4fcefbdd1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('94cd1527-faf4-4e49-b7f1-ca6de07a4aa8','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('94cd1527-faf4-4e49-b7f1-ca6de07a4aa8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('94e95ca5-7f39-4feb-b158-3b215fc6c779','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('94e95ca5-7f39-4feb-b158-3b215fc6c779','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('94ed77eb-009f-4f94-853a-302afd530e9c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9502f1a1-cae2-4b77-967e-0c191e5ad40e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('95082509-fe48-42f1-a9ac-8ba511b2b0c3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('95082509-fe48-42f1-a9ac-8ba511b2b0c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('950f62b2-827b-47d3-ae1d-619e71baeb00','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9516797b-187b-4e26-9db1-74da9eb69f50','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9516797b-187b-4e26-9db1-74da9eb69f50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('952209f2-c8f4-455b-996b-bd90de60dbad','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('952209f2-c8f4-455b-996b-bd90de60dbad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9524893c-ccee-44fe-b4db-4cf810bb6c1e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('952a3f92-f3c7-4758-a417-d64af0162903','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('95329a72-d6c9-48b9-ba69-749c562248ea','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('95343e88-7837-43d0-b853-44f97141424e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('953683d7-1194-43f4-a0e0-1610264f0b1e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('954f2aea-be66-4bdc-8e14-6bf1a5947e1b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9550f2d8-645c-495f-a8f4-47596f0cc473','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('955787b8-9462-4423-99ef-9eb522e42c0c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('956686eb-2d1e-442a-b35a-2e4a576a4343','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('956686eb-2d1e-442a-b35a-2e4a576a4343','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('956c7fc7-1a1a-42fd-86e6-d4ba1c3aa7f1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9578dc89-08dd-4fbd-9c07-96ad331ad20d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('957f3d27-c76a-4677-af90-fb5ea36a7afe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('959588bc-63e3-40fb-bd3a-11ae2c2a234b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('95af82b4-898a-4190-b00b-51701cfca63d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('95af82b4-898a-4190-b00b-51701cfca63d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('95b5fdce-f322-455f-b45a-45ba1973c586','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('95b6d310-9881-47d6-b5c7-24f4b91a8dd1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('96080f2b-5d75-4f61-b861-65addc5cd3a7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('962204f2-b761-442b-94fe-6202ef062868','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('962204f2-b761-442b-94fe-6202ef062868','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9623869c-eaf4-4138-99b5-50f74dbe9d00','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9623bc79-64fc-4a9e-9fb2-770a148164af','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('962d8496-088d-4818-bf10-93ba786ba470','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9640554b-af1c-45ae-a3a3-4bdd0c874611','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9684a30a-0164-435d-9675-53cfb7c8de3a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9684a30a-0164-435d-9675-53cfb7c8de3a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('968b7bae-f1a4-4a72-a5b4-2085aa879afc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('96963361-a359-4208-ade3-802ba4cb61c3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('96963361-a359-4208-ade3-802ba4cb61c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('969b98f8-105b-45c7-8714-d478595afdbd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('969ffe67-205f-4780-b657-2136b44dcb2c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('96d9b6e2-21a1-493c-9ea0-12b44a5fcfdb','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('96d9b6e2-21a1-493c-9ea0-12b44a5fcfdb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('96e79981-98ef-4d4f-a78f-81a7f3124ce9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('971bda92-445c-47dc-b73b-6783b6f49430','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('97221bb5-270a-4eb3-86ad-34648e2798a6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('973297a6-5bdb-4d80-896e-60781c8db2d7','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('973297a6-5bdb-4d80-896e-60781c8db2d7','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('973297a6-5bdb-4d80-896e-60781c8db2d7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('97430637-a26e-4614-81d8-de9bca2ab84a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9759c1b9-cca2-4970-a22a-742190a34c28','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('975f9a55-1d16-4703-9127-6269e22c56c9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('976e085c-9ce4-4bb4-b950-e488b18be015','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('977d894c-f382-44c5-a0c0-5bf238d061fc','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('977d894c-f382-44c5-a0c0-5bf238d061fc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('977e5e0e-1c7a-41e0-bbf6-2dbb5878421f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('97869d9f-a8d3-4df5-bd12-04cf659aaf11','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('979d9880-b414-4bf5-8590-dbba29eef87a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('97be4eda-03fd-408c-b513-45a2668de3f4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('97cfe208-dbc7-40a8-b67e-ca69b029e3d7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('97e21678-1b21-47d8-a447-d4166f9f6906','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('97fbd1e4-16fb-43c5-8371-760089da5496','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9838f222-2372-413a-8989-5796d9af5b9a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('984433e5-947b-4aed-81a2-7ae5f897ce22','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('985d2168-86c4-41ae-ab20-4bf209018340','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('985d2168-86c4-41ae-ab20-4bf209018340','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('98781b52-aab5-4d8b-b344-9fd285d21830','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9895d3ed-eb8e-4751-94db-799dd1c83f04','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('98994ee8-4cbb-4851-a384-3cd08a55ce86','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('98acec55-4c82-4598-8404-ab84cf69916b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('98ad603f-98be-40a4-b947-9e911a48d984','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('98fe9a8e-7500-4d45-a8f8-eeea986c2287','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9912c3ac-e56b-4cbb-bcbe-7c2ac53160af','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('993e4d44-250a-47d3-9f46-d5c4656e659a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('993e4d44-250a-47d3-9f46-d5c4656e659a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('993e4d44-250a-47d3-9f46-d5c4656e659a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('994002ff-49dc-47d0-aec5-5fedae61723b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('997cbe85-ce63-4091-9bad-fe761c885fbd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('99b23aca-b81f-4e04-80d7-fd60d610ec03','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('99c94f54-7894-4d1a-b5f7-aecffdd8dc8a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('99c94f54-7894-4d1a-b5f7-aecffdd8dc8a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('99dc1fb2-412b-4f19-a377-ae894a7882fb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('99f2039d-1254-41d4-b718-5e3fe1a03929','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9a17c960-a5ce-4a5f-a5fe-1d52a73f8bb0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9a651a87-52ec-485d-b973-d9e4243a2bbd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9a6e360e-49a5-43a7-b9f9-99ae39dccb1a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9a6e360e-49a5-43a7-b9f9-99ae39dccb1a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9a720105-fbfa-4f54-83a5-dadd71cd781f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9a852c90-0ada-48a6-9e63-0d21cdbd687a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9a8d700a-b8d9-43bb-bbb0-0169dee2a124','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9aa31cda-22d0-4c13-93bb-1b6d28131d61','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9aee5ba4-f3bc-49f5-8449-e938f69f3acc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9b06367a-6397-48db-a3bb-836a33d731ee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9b5580cf-b56b-4656-8e1e-86cf256bebec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9b7c59b8-bb3e-421d-9d61-e91a018db28f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9b7c59b8-bb3e-421d-9d61-e91a018db28f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9b838157-138b-4464-aa67-537b23cc1352','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9b848f55-cee4-48bd-bbc6-ce19bc12cb18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9b91538f-773a-42ae-9e5d-d8c87232a0af','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9b9dedb9-9ed7-44af-91b2-e1027396b664','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9bab7e3f-8d13-41f0-b4f1-3ef4f929db36','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9bbc1d8e-2411-4a42-b580-b4927fa39673','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9bc2cc77-990a-4340-b0ca-0e707e80e891','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9bcb13d2-8ca9-4a7b-8a77-54f9168a04c5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9be20235-7253-41dd-9b0f-28432c16486a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9bf19538-4c65-41d7-8c92-d97991057890','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9bf5df69-1381-4cc8-90a2-9a8abc6a9697','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9bf5df69-1381-4cc8-90a2-9a8abc6a9697','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9bf87876-dc4a-4231-8c8f-77a7cd6fa9d4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9bfcca78-bcc7-4eb9-bd58-708e926a7177','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c05f7a6-9541-4bde-98b3-eea58911ddd0','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c05f7a6-9541-4bde-98b3-eea58911ddd0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c15c971-f0b9-4232-984a-394f091c2872','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c15c971-f0b9-4232-984a-394f091c2872','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c1a93de-b0ce-497f-9ff3-59da86323e98','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c25c52d-2ead-48b3-8260-3e32892429c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c404563-0a09-4165-a81c-8507a14d42d8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c4684d0-003b-483d-941e-c0f54e6cdf95','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c46d655-a24f-45b3-8d56-b15b3cabd897','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c5b0507-6310-40f3-8a79-bd7153f97994','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c613a81-4e97-421e-9a56-69f4049cc06d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9c6e3d2e-555c-41fe-9d04-d1657f143a59','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9cd19f7b-4bbc-4bf3-9082-5071ff2f338d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9cda57ff-e13f-4053-8989-e1490515118a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9cda57ff-e13f-4053-8989-e1490515118a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9cdd4a7c-8b35-40fa-97aa-9b007725a84c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9ce4b2f5-7ae6-459d-9197-e41a4716f118','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9cecb095-3aa7-468e-bf36-4c8868c8753a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9cf5e28f-cc66-4dec-aaff-1126caf83bb4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9d3a4386-88f1-4765-b497-dfa7f142b219','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9d4c25ba-8ea6-40dc-981a-6c64c7224559','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9d4c6b63-ff0b-42be-acf3-ff75f87a140d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9d50c5aa-5a9d-4370-8e32-b74301e8d421','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9d7db53a-ecde-484e-8157-e8529f5f1ffd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9d7eb284-e9b1-4a66-afb1-405f1cd4db8d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9d93c0b6-469c-4a8b-a092-3415db30668d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9d93c0b6-469c-4a8b-a092-3415db30668d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9dba07a5-d5f4-450e-baed-083cdc0f7b4e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9de6b7e3-954a-4049-90fe-210ef97f8656','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9e0f02cd-660c-44ff-8c0d-226df9e14c2f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9e17b4d0-70ec-48cd-97d7-b5994fd26a39','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9e17b4d0-70ec-48cd-97d7-b5994fd26a39','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9e32190e-e717-4b5b-96f9-2209b8f21e03','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9e76bc28-7a77-4f48-86f4-c8e8162463d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9e843a22-2899-45c9-b5f9-588d41c4fa75','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9e937015-2cae-4e27-b2db-0650e9ecf8d6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9ee4b726-397d-437d-bd8a-d97c7b12e50c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9ee4b726-397d-437d-bd8a-d97c7b12e50c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9eff0399-8fe6-43c9-b9c3-10b8b380f02f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9eff0399-8fe6-43c9-b9c3-10b8b380f02f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f046541-bd71-4208-b2fa-e5548ee332b3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f08bb05-e530-420e-9414-71fb64512545','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f2434b1-24a1-49a8-b41f-2f4190fbb722','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f291e8b-1edc-4e9e-878d-a9075e65e927','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f345503-7175-4544-b111-537909c7f874','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f39d502-6784-42aa-82ea-7a8e6e678ed9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f3f1e8d-0fc1-4773-9fa3-681343d4c2ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f45a7fa-1adf-497c-9ade-3af37e487f5f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f4c78ec-321a-46ee-859c-4f4268661ee4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f4c78ec-321a-46ee-859c-4f4268661ee4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f81ff28-ae21-4bad-a404-1f50377deed8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9f951b6d-260c-4c21-8419-5fee9f404ad6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('9febbb47-12d1-4e83-b6d2-a4bcaa1483e6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0048b9e-8dfb-44fa-9713-6bffc672f6c8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a02cd98f-174d-41a7-bdc2-8ee2e027f1a0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a03ed860-191c-4da5-b550-8056962b5332','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a03f9e3f-b325-484c-bb85-df68c363bd08','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a04e6dfc-1dab-42f1-8c7f-84ff5512d66d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a056cc94-0f9b-46cc-b6c3-c3294c9607f5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a061d623-106b-4041-9b92-a265db06f43c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0725a13-eac3-46a8-8559-9e9cbfeccee1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a07579c2-d019-4fe0-8985-3e4c1c933365','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a07579c2-d019-4fe0-8985-3e4c1c933365','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a07579c2-d019-4fe0-8985-3e4c1c933365','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0793622-9c0e-4957-892e-5e52937ccbf2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a09ec5fe-0a87-462d-be9c-836a04f95d15','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0a34dc1-18ff-4b1f-a5b2-47fe02c78f73','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0baf5a1-64f6-464e-8542-b8d1f00e8a3f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0c541a9-93ec-4e1c-a53d-5256948df15a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0d7073a-91bc-44ca-aeae-8d3bd2114a81','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0d7073a-91bc-44ca-aeae-8d3bd2114a81','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0db61e9-2c52-479e-bc89-e9fab29efecd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0db61e9-2c52-479e-bc89-e9fab29efecd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0dbe6aa-c6e7-48b0-b708-dfd2baad764f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0e21a2d-378c-488c-90e9-fd4326e3f848','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0e21a2d-378c-488c-90e9-fd4326e3f848','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0e88941-16cb-4d2b-bc2e-6d9c6823649e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0ed1cdf-4a98-4ac8-bab6-309bce065a6f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0ff786e-6a4f-48e8-ae19-3684b7e7b665','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a0ff786e-6a4f-48e8-ae19-3684b7e7b665','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a106e1db-975f-4905-90df-e5f2a939e815','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a11c3bc6-b83d-43f6-a962-f3ed83b0c89b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a11d9526-22d9-4e41-8dd9-1de9ef16ebb3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a120b10d-a90d-4819-9298-f43f579969c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a14214e0-6085-40e1-be94-e87d1cec1329','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a14eb499-fe4b-4719-9533-3ccb1f66e274','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a152d243-53de-40fe-bf89-8292ae353e44','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a173bc43-f284-4eba-b628-0ba3f47c8642','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a1787985-46ab-4e73-be19-6e715d37b576','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a1787985-46ab-4e73-be19-6e715d37b576','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a17e3328-b781-4690-b698-4dc7cf3f44df','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a17e3328-b781-4690-b698-4dc7cf3f44df','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a1828f43-e511-4565-92d5-02ae9a084d9a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a185ec22-ceb9-455b-aa0a-52b736e6e980','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a187bf83-6233-410a-a9cb-65c08991fd14','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a18e13cd-f9dc-48c8-9ba8-b0845365903c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a198d69a-c181-494c-ad5f-f7051aeee191','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a199de2e-3385-4935-bcff-44f71a294191','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a214eb2e-2c84-4279-8270-885f8aeb946a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a221828b-74a5-44bb-90bf-63399b97d4e6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a221828b-74a5-44bb-90bf-63399b97d4e6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a243dd3e-18c8-4c67-8ba5-7a8fd027cf3c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a27a170f-99f4-446c-bb80-b496fb1f3305','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a27fa172-19b8-4944-92c8-3b5266cbcad4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a287242f-d95c-49b3-ad2f-7053812e47f5','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a287242f-d95c-49b3-ad2f-7053812e47f5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a287242f-d95c-49b3-ad2f-7053812e47f5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a29efebc-dde1-4f8a-85a8-f6a83f722459','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a29fef30-d8cf-42bc-8e5c-1fefaa1e1bc4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a2a9ad62-1f1e-4d6f-91e6-90d3656ac088','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a2c821d1-19eb-4555-8de5-2c6b59733917','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a2d2d284-4725-4257-a01c-921ab7d37ee1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a2e1a666-34bf-4ff9-8c15-00cc1bfff1c6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a2e1a666-34bf-4ff9-8c15-00cc1bfff1c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a3017a95-3f34-4f7e-ab08-578942a33d47','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a3017a95-3f34-4f7e-ab08-578942a33d47','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a3017a95-3f34-4f7e-ab08-578942a33d47','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a31c7656-a46d-4686-9ef7-27de0a6da742','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a342659f-e34f-4d42-8207-8c5184dd4733','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a346646b-c60e-432d-960d-d03d0096b3b9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a348ac33-0d88-4286-9a05-ec6cd6a43af3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a34e22d3-59e1-4d55-8856-73f5775525f4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a357f61e-97cf-4ae5-aa8c-f30a4a2b16be','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a3599765-8336-402a-917e-cf0d5c0747cf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a37d8eb6-c874-4ebf-9728-5fea97e1755f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a3c1a1f7-abe2-4233-9231-22ded0e8a418','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a3c9a988-e196-464e-9a23-846ec4f94a6e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a3dd92c2-ce26-46e1-968e-9a05868cc2f2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a40f9bf5-51f9-4157-9349-c081bcc676ea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a41ea92f-6395-4a54-b39f-0ff966e95a64','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a41f1b4b-f7ee-4e27-9a50-5c867ffb238e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a426a023-5ec6-4cd9-9e1c-ef572267050e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a4477856-94bb-49ef-9aa6-4de8ded02799','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a4529ef4-06a1-492f-a38b-874c5591ed1e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a45ac97d-b400-4e40-a6ca-4a99263f26f4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a48fce90-2b54-446b-ac69-6ffa3b61e0f9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a48fce90-2b54-446b-ac69-6ffa3b61e0f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a4d629c3-5b02-4642-9922-180a219b9607','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a4ec9318-1ced-4633-95ba-54b5e244a885','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a4f06126-a925-42fd-87a0-3d8f25e05884','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a50a1513-7653-4dbd-a335-26e8bd402b87','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a512afbd-2b67-41ff-a925-a86cfb9db553','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a52813df-11ce-44be-8901-1276f957bc1d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a55f26f5-ec7a-4c51-ae96-da9ad99c3172','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a55faf7e-d700-448e-b29a-9130aaae12d2','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a55faf7e-d700-448e-b29a-9130aaae12d2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5652176-a872-49c7-9c4b-52e641ec3412','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a56d277b-1f85-4dfb-8c07-cd2f8a8fc44c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5785d1b-e652-4234-aa4e-aff5ffdc9d30','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5821bcc-6cae-4aae-914b-4bd3691df324','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5821bcc-6cae-4aae-914b-4bd3691df324','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5821bcc-6cae-4aae-914b-4bd3691df324','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a58d5af4-c32b-4fcc-bb23-a156a0d49ddd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5941c55-1799-47b9-88cb-47fb5dbcaa39','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5b6de5b-4c39-472d-94e5-1c29c991ea81','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5be3593-4e69-4865-a7e7-146a64b0259f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5be3593-4e69-4865-a7e7-146a64b0259f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5be3593-4e69-4865-a7e7-146a64b0259f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a5e66ab3-0aa4-4178-8d9c-54fa596a0d9c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a625ea16-4efa-44fb-9395-88c6ced48fc2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a628cf35-6517-4ec1-9e73-5f67be59d06b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a628cf35-6517-4ec1-9e73-5f67be59d06b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a64eb6c3-cfc5-479b-825e-05bd4a113fb8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a652adf5-4e96-4c58-8777-6ac78ce3b0b2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a65b1cbf-e24f-4994-a2ec-06e092eb16d7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a65df1eb-5cc7-4a34-b1c0-94db45206340','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a695882f-a0cd-4de7-b59c-e1fd391bea3e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a6c2f6c2-6fd1-4a2c-b841-e7907890766b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a6cba0f5-f5d8-4b25-ac6f-0e965b8ebe72','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a6e28875-9435-4cdb-9b95-39a08020e97d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a6f05569-6667-48f2-8704-27cdff5efaec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a6f552eb-82c2-4b03-bc54-bfcd6ced1c63','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a71733bc-f2b6-4df9-9f61-5f105c4dac47','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a71733bc-f2b6-4df9-9f61-5f105c4dac47','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a718c633-ef40-433b-93a1-a0c93f9d964c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a723d2c5-7945-4544-b45d-a2b9562ed2a5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a73ede32-319f-4ea3-8810-a7ab4901cb60','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a7490cab-0ee4-4d3b-85b3-2ba6c50f538c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a752db76-883c-41a2-b75d-6dbc01fe7aa1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a784cc15-26da-4e73-9e37-eddd6c360f46','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a784cc15-26da-4e73-9e37-eddd6c360f46','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a784cc15-26da-4e73-9e37-eddd6c360f46','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a78f601f-74c4-428c-8db9-1e01646153c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a792e36c-34b2-43a1-99c6-33998bcec5a8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a797782c-0c1b-48ad-b87a-f615c23c76a8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a79bc185-301a-439d-89e5-890b8f2cb2f4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a7ab06b2-83f1-4301-b5dc-dcb8d4d12d99','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a7ab06b2-83f1-4301-b5dc-dcb8d4d12d99','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a7ca044b-4964-456d-9653-65fcfe8e5355','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a7d7aebd-3805-4a70-93ad-738f7e5c5009','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a7d7aebd-3805-4a70-93ad-738f7e5c5009','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8226f35-d5e1-4f32-bc4d-8db02e0f40ed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8287a26-de59-416a-a680-ef3e6331cf29','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8336aa4-31a2-4238-b73c-0522ffa0ff0e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a83b254b-d1fc-43aa-97f5-40ff0536cd5e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a83ec971-04a5-4dbc-a18f-7120ca4e04a8','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a84b241d-ef14-4758-af01-5572c50d0c7b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a85f5a11-1175-4347-ba6f-28dffd75fc6e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8774ac9-bed8-41c0-844e-4aa6e95860c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8b656db-5e48-4061-bf41-b2e552272705','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8b656db-5e48-4061-bf41-b2e552272705','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8bcbcce-8700-4813-9111-900097353e2c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8c744b2-028d-4747-b5ce-3aa452f30f21','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8c744b2-028d-4747-b5ce-3aa452f30f21','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8cdb906-0c0a-4d20-801c-d9d1fd2e6e67','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8ce7e4b-5e80-4b2c-8ae7-a63d0e75eab2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8e17f2d-cd17-4673-b3da-3eb2193b3833','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8e728f2-f60a-4d35-9246-2092ecce2ad8','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8ede09b-d84e-437c-8809-5762312453b2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a8ff9414-ddf7-4e52-809a-4c9f4a67a444','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a904fe5e-3159-49d8-91f9-b3754146e2c2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9097b72-bb98-4f9c-bf8e-7c64b3bf932d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9097b72-bb98-4f9c-bf8e-7c64b3bf932d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9558d58-2587-471e-82c8-35d891b7d5da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a95d2360-a604-4a1c-848d-2268cccd2c01','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9637ecb-0fe8-4bec-9c24-d3efe6643c34','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a96ac9d4-93b1-409b-ab15-d554253eca59','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a97d72e3-80d9-4f92-868d-630c24083008','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a992fe99-b9aa-4060-b10d-36be4318dc08','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9aa03eb-a383-4fa4-8ac0-e1d9b2247bf7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9b8a0b2-90b5-4a07-b5ea-2bb01ea0cee7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9bf53f9-cb6a-478e-a029-e9c6fcd5d686','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9bf53f9-cb6a-478e-a029-e9c6fcd5d686','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9cd068d-a40f-40fe-b716-cdb98b82bee4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9e365b2-2254-43b0-842b-28f74cc3b39f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9f0d621-88e1-4213-9442-3f0e23967d92','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('a9fe0a1a-e744-4de3-802b-0a22b2e87a8e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa000cd6-3354-40bb-8eab-0f2cf310519a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa0d5331-da03-4219-9c15-998d44ce7e0f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa0f82bb-c710-4852-af07-56fc5db230b6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa1d204c-2578-48e1-997f-350e29a7012e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa315c1d-ee47-4567-9f58-47ec3d470c58','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa405227-9d68-4424-89e2-185f848ab038','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa54a3b9-2da2-4837-b752-4d395c36116c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa55d32b-436b-4cc5-86b1-df05ead0346d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa72e215-11fa-493b-84e0-f1c862865f78','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa8aa5e3-83a6-4439-a6d8-428bc22b747a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa92fc69-c7e4-432f-b6a0-29268a0d5f78','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa9845fb-b75e-4383-a661-66603e24f1b7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa9a0660-658e-48f4-95e9-9594aac75693','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aa9a0660-658e-48f4-95e9-9594aac75693','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aaabf65c-b023-49ab-a72d-b08921ca3e6d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aace5108-8ada-40d0-ab4a-e18deed8a5de','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aaf06332-ac37-41dc-86c3-fb87e7febeff','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aafd34b0-f4b4-4ff6-a914-c006a918a991','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aafd34b0-f4b4-4ff6-a914-c006a918a991','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab24a863-f9b9-4a6a-a8bf-6fd8dbb59056','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab3003d8-e296-4c58-9589-b68ae340d81a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab423575-7df1-45d4-940a-f75f162f3294','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab44b45e-52eb-4f77-9f76-3afb931976d4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab49ec6c-09fc-4d80-b15e-9a1aa971c05f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab5e96f6-e622-40fd-bedc-7a98c809858d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab708b23-d9e5-415d-9cfe-540e6b1342e7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab7947f6-c928-4629-92e0-4a3b8857be5f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab85023c-4728-41fa-bbc8-d3c11e79eb78','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab8990ba-f6c0-4827-a20e-f3da520f0110','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ab99bd65-4f22-4cfe-8415-30878043c0d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aba2e10a-d438-4bb9-bfca-b110f752d7e9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aba36719-7553-4890-bea4-16231118c35f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('abbe13ac-43ee-49f5-90f2-79ac1d379d52','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('abdded43-7bb9-4d77-a3fa-6fcd5cfe2ff6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('abe20431-2197-4b0f-9918-c801e631d01f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('abf62162-e815-4339-b9cb-353263e4b4b5','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('abf62162-e815-4339-b9cb-353263e4b4b5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ac0272b2-0fc0-473b-906d-0272900d8f41','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ac26d453-db3e-4122-a75b-8422a0a732c2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ac3c7e90-f961-4b1f-a7a0-db2694edb73e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ac5dad94-5667-44ed-94f3-3f69ee33bf87','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ac6948b3-739b-40ce-ac7f-2edb6de9a516','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ac7db5a5-431f-477b-b590-38145677451f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('acb51885-656d-4b76-9876-370169861858','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('acb69589-b376-4387-8357-1d07fca55352','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('acc9656b-63ca-4a42-ad17-ec739e367d55','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aceefe6b-d683-4a1f-b577-ec1ad6f6eedd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('acf69f8a-766d-4542-8af8-af93ead45c3f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad00c45e-23c5-46c2-8031-45915f96d3a7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad19e291-1c5e-4048-b5ad-c4bc0be5a464','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad1de2e6-8548-4040-b115-5ee679d3ccd7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad262242-89b3-449d-b72e-6ae85d95dfb6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad4ba5ae-1b3a-487b-92c9-ab6e392130cf','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad4ba5ae-1b3a-487b-92c9-ab6e392130cf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad685de4-f681-4735-9b63-72006f958f60','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad6c19a4-999c-43ab-8426-ba66afdc01f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad7dd2ae-a8a8-4bb8-90bc-1cca5b99bfce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad817ea8-5fab-42bb-b483-95f8794c06b0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ad8eae6c-79c8-4dea-943f-307883c9386e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ada22281-9711-4387-bc43-6c8140d89f45','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('adc82abd-3c75-460e-b34f-9c31673bfe52','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('adcd0f31-483a-4501-8fae-c4b73d8f751f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('adf6dc2c-e00a-4fdf-9ed2-636f4110bde9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('adf7008f-684c-4a9a-93c6-3208c2e9d057','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('adfd5e60-648c-4889-89f6-000077c18552','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ae08c626-5ec9-4881-9de2-ad6ff05bc5cc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ae104b95-f75c-42a8-bd74-1e710fe42375','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ae42c74c-27aa-45b5-8598-d52928b068aa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ae5477b8-b47f-41db-beda-ea9c6d209983','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ae549c7d-e6cf-47b1-9da0-e988aedf2b94','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ae56f6be-192d-45d2-bd42-f1b7bff05362','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ae56f6be-192d-45d2-bd42-f1b7bff05362','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ae5b6390-2eba-4b37-91f1-6ba78ce4bbbc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ae8e0956-fab5-4942-9797-22fe7e8a0dca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ae9fe37b-a149-4d67-b50c-d1a1ab8b0deb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aeba22f9-69e1-4eae-a7c2-78dd74ba4844','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aed15f6e-d954-4917-949e-4039fdd961b8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aee238a2-3375-4a3e-a97e-799512c1a139','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aee7f783-42fd-42e3-8a5d-5e9fe482ed97','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aef7773b-c186-4628-b2d1-eb480af41758','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('af044a4a-c01a-45fe-b78e-21775a93c42d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('af078d2e-658a-4882-a1fe-b267389ba091','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('af5838c5-b4ec-4bb8-a436-98111e633503','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('af5838c5-b4ec-4bb8-a436-98111e633503','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('af58d7b4-b18d-45ce-ba7b-391aeb4d16df','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('af5bb435-a7bf-4608-8638-ea262e7da99a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('af5da90b-c04a-43f1-a100-842895ed7177','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('af6d1480-2d8d-49e4-9fb9-a58901a20659','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('af96f2a2-3af1-4e53-9c96-feac1902e3c8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('afaec117-dadf-445c-9061-282e0a0ca617','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('afbda9ed-637b-48f3-bc26-950039d1e412','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('afd35f94-e600-4f2d-a3c7-7ee4b66c3672','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('afe1633c-0c6f-42d7-b88e-af82a84ceb18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aff370ff-2c27-4a9e-abee-3c9f205e69ad','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('aff370ff-2c27-4a9e-abee-3c9f205e69ad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b00277c0-6c16-4739-9975-1e9626b53886','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b009ecce-5005-4b30-82c4-0d6047097f20','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b009ecce-5005-4b30-82c4-0d6047097f20','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b02d4f6b-e394-4395-abd2-b26b2e9454a8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b03c62af-5f24-4d20-b187-a81338e430b3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b053ff1c-7593-41f7-afc8-f20f71882e73','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b05aca12-c980-4e4b-8e76-a258c55ac682','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b063e6c8-3523-4759-8d73-92c0ac76a651','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b07ada14-b4cf-48e3-8753-7f1690d60ad1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b07dc56c-5988-4c36-bdac-e1542299099b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b083e66c-51d2-4427-a03c-d97a1407b7bd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b0872b22-5475-4428-861c-621befd68e72','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b0917755-a785-444a-babc-f849ae0616d7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b0be1a78-74c5-4c0d-8d44-6ce376588150','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b0c194d2-6de8-45c9-a120-173fa03f4eb1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b0c194d2-6de8-45c9-a120-173fa03f4eb1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b0d289cb-85e1-4bd9-9552-a0c770eebb65','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b0e1ca66-6615-4df2-b21b-8d5f23ea7bac','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b11c2f97-fda0-4a81-a78e-076f7f8d6273','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b11dce52-4e9f-4e8c-b745-8ecaf689f203','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b125b5aa-bfa0-4eaa-ab76-44a410516c37','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b13e6454-2bf0-4a96-98f4-85531aa6ae5f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b1434ddb-b32e-483a-a54f-b2ebddbde778','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b146268f-8bca-4538-af3b-5a212231e6a5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b1584466-ac42-4c41-b760-625807fcfaa5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b15997de-a6e9-4c8e-844d-b1a539a859e1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b15997de-a6e9-4c8e-844d-b1a539a859e1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b174df28-8d97-4f43-9176-d741c607b3b7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b1907b5a-336b-4273-bfab-59f7ec53686d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b19125ee-0220-4b79-9b2a-bfdd47b9d043','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b1b3d794-62ba-4367-b577-380eadc7c2d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b1c7c242-841f-421a-b7d2-caca59a8f5bf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b1e35afc-0c9a-4817-bcc3-916145781133','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b1ed6730-dd01-486d-a9df-5c076888fc13','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b22a8f3f-2d63-4c09-b749-ec13364a16c1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b22a8f3f-2d63-4c09-b749-ec13364a16c1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b264f740-9a69-4b88-a61f-ec36a6b10e4a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b274f647-48c5-4b27-b4de-65322e82957a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b290db45-9e96-4454-8b55-c1a6428e413d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b290db45-9e96-4454-8b55-c1a6428e413d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2931612-2f1b-4f5e-bc21-03696e0d54ef','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b29c1a77-98fd-4fcb-9685-108a0ab667b7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b29ca4dc-cb7a-4caa-8f05-e9a50b495fd0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2a54113-be86-4f90-b996-80f58b2500e9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2a9c993-76b6-421d-b877-5dea3f8fecb4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2a9c993-76b6-421d-b877-5dea3f8fecb4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2aeea96-dfc9-4101-a2d0-fe7593f01e98','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2b08491-2388-4adb-b482-d83815a3b44a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2b8d484-4324-4720-99ea-fd1029972f07','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2dc9db2-f994-4b91-a435-00c18471c1c4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2dc9db2-f994-4b91-a435-00c18471c1c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2e4f90e-ce65-4ada-bf3e-b8aa3f3ad401','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2e62db6-cf98-4947-81d4-99d6d797ef54','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b2e62db6-cf98-4947-81d4-99d6d797ef54','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b307a4d7-4e69-4fd7-bd36-0699b52c21e6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3094661-ecf2-48d9-9f5b-fe0fbf3a274d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b30f97c9-a6f1-4e9d-8872-c790bc8c9de3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3187693-3d05-4906-a29b-037fedda4c3f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3187693-3d05-4906-a29b-037fedda4c3f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b32b420d-064a-463a-9a96-9dadf9b5e0b3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b355ee95-e594-41d7-8d03-caa33fb01b35','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b357a85c-723d-48e5-aafe-99087de13485','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b357a85c-723d-48e5-aafe-99087de13485','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3819b4e-acab-4b13-a18b-2965e67df545','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3819b4e-acab-4b13-a18b-2965e67df545','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b386e337-08ee-4980-a4a3-b961a1113194','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3a5fb90-3ac0-49cf-9892-0d845be05694','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3b468ca-7714-49b4-9cba-af87d3e3314b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3b468ca-7714-49b4-9cba-af87d3e3314b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3bb0a80-ceac-4efb-8465-e90953bf8a04','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3c3515e-56fb-4139-a754-c8e96a0475c5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3c3515e-56fb-4139-a754-c8e96a0475c5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3c531ea-6638-43b8-b74c-6a4b47db5f0b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3e06f9e-546c-47cb-9c4a-b849434917db','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b3fce518-6072-42da-8f27-894cde087f9f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b404f317-69e8-4f93-b58a-a90037b6a755','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b4247eb5-fe56-4161-938f-bee84d47eb5f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b425abcc-d3e3-490a-9713-ac48d98bd65f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b425abcc-d3e3-490a-9713-ac48d98bd65f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b42f6db5-5ce9-4c06-9ae0-197a8ae0e994','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b42f6db5-5ce9-4c06-9ae0-197a8ae0e994','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b438283f-283f-4022-ac5f-02586fd1fd6d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b453e7d5-df7d-424c-b77f-967be4133ffd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b4704185-3c83-4889-9bb0-18339e98b5ce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b47a8b4d-4f2e-495d-8179-729dbaf6167c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b47a8b4d-4f2e-495d-8179-729dbaf6167c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b481cef9-e3c8-4b08-ab5e-15c0520c7584','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b4945548-c759-4a6d-a01f-adc13192da71','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b49fdae5-8999-43ea-bc73-40674a1bc21d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b4a973d7-6b61-452b-b9ff-5f53f1bce8c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b4cc91b3-a067-49ec-9286-819d4fc57a62','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b4d31e69-54fc-4af0-9543-3338d89e14fa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b4e014e7-5035-4c8e-81e0-bb8127c310bd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b4f6e7c6-a1f7-4af4-be56-b67c76b621df','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b50bcc69-230a-4948-ba49-89e93b9a8e11','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b511a056-2a2e-4c2b-a43d-628e3036cb05','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b511a056-2a2e-4c2b-a43d-628e3036cb05','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b527901e-e701-4e5a-ba14-48e3edc7169d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b52a7f9f-4aca-4c11-b8e9-b4a4ee7f3641','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b541b370-8a87-4a43-bb8c-dabf1b69d49a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b54543c7-28d9-4e27-b7ec-1104aa76ac87','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b55ead45-430b-4996-ab20-e0b57f72236f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b560fff8-a3a2-499e-ba51-75edc5f865aa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b5724d1f-9104-4ecb-be60-21c0736e4a68','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b57ddeba-6367-4cbe-a104-e36c14a49ed0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b58e5f2c-83f9-4cb1-aa06-d98d3750ac07','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b58ec728-bfd8-454b-9c49-63b2bffd2c8b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b597572c-af03-4998-b364-112b199624e9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b598f565-5ddd-49fa-a916-34ba9d14ccd1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b598f565-5ddd-49fa-a916-34ba9d14ccd1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b59d0c57-dab3-43e0-b5fb-aef37553d60f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b5a9f30c-128c-4691-88fe-997342e495eb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b5b8785f-20c0-4278-be4f-9dbe54fefc43','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b5bf066d-1e73-46dc-9f40-3b108328a79d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b5dbd784-386a-4f74-9045-2d4f0974f148','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b5f3aba4-dda4-422f-80d1-13778b8d2f87','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b5f5360c-d8c1-416b-99b2-2aa1d49d08d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b603abde-a96c-4319-b2d0-7230b874ab77','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b60af613-b17a-41e8-95fb-3240aafc98e5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b6380d93-69ff-4f06-a678-d0640fcd8f3e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b6485b87-d5f4-4a0e-9aa0-a1f311976546','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b68c77ce-020c-4aa4-974c-9e01779d8224','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b6b6764f-1dad-4885-bd5a-c7951b937da7','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b6b6764f-1dad-4885-bd5a-c7951b937da7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b6b9b84d-e096-4fef-897f-e9d1f3006c3b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b6c10faf-777c-4405-bc4f-3ad3addb9e43','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b6ea8ebb-ad60-43e1-9efb-512e158afb3b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b6fbf0a6-9f18-4d0d-a2ab-929356612444','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b6ffcc48-463e-4f98-a8e9-b4e783bea029','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b7221d7f-1b8a-4506-bc69-477c35992244','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b7354a54-ff7a-45c6-84d9-af37ab31442b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b74887ed-13b9-40cb-85cf-00fb4bf96e76','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b75d61f2-7d41-45de-9538-4f9d35402d2d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b762ffc9-6836-4887-9eaf-fbfea9d100d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b7837818-7c4e-48d5-8fec-f044a8e19d17','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b7983f04-1bf9-49c9-88a9-f8ce8015cbdc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b7aa7c6b-2f36-4fec-92ba-4ece80f7e1a2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b7b41421-82c0-422c-a414-b8a82eda9335','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b7c42179-cd86-40b3-af8f-ebd8caa83203','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b7fa1538-359c-4827-9985-8678d9669a78','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b7fa1538-359c-4827-9985-8678d9669a78','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b7fa1538-359c-4827-9985-8678d9669a78','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8236aa6-c8e3-4cb6-bc58-4a8a258bc138','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8236aa6-c8e3-4cb6-bc58-4a8a258bc138','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b83ac921-d7b2-43ea-b85b-802ad5b88bde','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b84bbb5f-4225-4387-97f1-7f88030dfb4c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b84bbb5f-4225-4387-97f1-7f88030dfb4c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b851b7ba-d162-46c8-b03d-099371aab5f6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b855c78e-8f91-4dca-a135-9ee717e1bba4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8578116-ccda-4a19-a3ea-aedd17831385','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b86ab288-42d7-4b15-ada2-27e2c260796c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b86c85a4-f6b7-406d-8bcb-87250ad98b5e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8a13529-dda3-49c5-b076-cfdf08f27a02','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8af8097-19f2-4e29-9fe9-8d5ed87a62dd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8af8097-19f2-4e29-9fe9-8d5ed87a62dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8b27478-e747-427f-b9f8-0bfa87438640','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8d7087f-2dc0-449e-8db5-c86e2ce4d5cf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8e1989a-c0b8-46b0-9dda-3ed510c23b03','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8eff561-c5ef-4676-9910-25e159158520','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8f7ca34-8be4-4df3-a662-faf64ff811d9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8f962fe-45ce-41e1-bb16-ae204c664439','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8fed6d7-4ec8-416f-b271-57f588a25467','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b8ff1491-189a-44a8-8a7a-7e00a6a737b4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b9025a5b-8e0c-4020-a1bf-d3da04f3b910','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b909a785-133e-43d7-a430-eae7835a36e2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b9176329-4288-493a-a444-893e0681926e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b933e048-d85b-47a5-9a12-94113f579039','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b937e93f-5fc5-41c9-b2a4-5b9e96a12451','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b94318d5-5cf2-4983-9102-49130c90e321','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b94318d5-5cf2-4983-9102-49130c90e321','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b9794e3a-49ac-4702-99ce-32d457c8e15c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b97d22d0-d5c5-4595-825f-fbeecf7cef58','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b98a2836-60a8-4130-a153-91f6089a1b41','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b98a8f54-7218-4a10-8a19-d7b6694b592f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b99ba99f-bb45-4ea0-99ad-f1fd362f9205','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b9a6b936-fb8f-43c4-8638-00d624ba7505','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b9b6a207-b97d-46e6-8c20-da12a61ac5d9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b9bacca8-b7c2-4257-9a2d-68305598a1da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b9c4f21c-2c66-48d5-8da5-fdc4baa51373','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b9d03bf3-f0ff-4a53-a27d-c7e7ead922a5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b9eda3d7-821a-4b73-85d1-bd5e42f421ae','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('b9eda3d7-821a-4b73-85d1-bd5e42f421ae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ba1dff8a-a455-4f43-b64d-7441816e3344','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ba368306-cc0b-4e73-845f-3751396df2b8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ba7d5725-ca1e-48a2-8b3f-31bc52feb9a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ba853cf6-d081-43f0-aeec-a30c3dd15908','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ba8544cb-bb75-4bec-b25b-f546939ef49b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ba8544cb-bb75-4bec-b25b-f546939ef49b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('baac19a6-f388-48e5-8c2a-c3eee59dd45e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('baaf6008-3fde-4a49-b66d-a779f6cb8ec2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bae5828a-0a49-4eab-afe4-b60ca4e50ade','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bb1cea71-8d22-4a66-b080-79ba92957346','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bb1fda1b-09e4-4256-b71d-82144f35c674','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bb337aec-6ddf-41ca-96c0-f7a9fa374984','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bb3867dd-a29b-4109-bb18-4382d6fda725','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bb3867dd-a29b-4109-bb18-4382d6fda725','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bb3d71e3-366c-49e3-8ce7-b7adefd64e27','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bb494109-5d91-445f-8553-1e4e2380462a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bb598836-f38d-4258-960c-3a03432d9d41','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bb5f3520-606a-4a4c-9576-44edfe594e8c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bb7b1a03-81fc-4640-9c0a-cbcc88932337','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbb2375e-64fe-404c-b252-ae296d68fc06','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbbac1cf-ff94-4f14-bae2-f8ddbe6e0ec7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbbdb049-430d-4db2-a7df-bd54ffae393c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbbdb049-430d-4db2-a7df-bd54ffae393c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbd57065-65e2-4ee3-8476-7ecfd04a0853','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbd57065-65e2-4ee3-8476-7ecfd04a0853','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbd8db52-976c-4d7c-ba16-a6fa46afc4cd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbe02705-330a-4195-81d9-0974a845a9c4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbe02705-330a-4195-81d9-0974a845a9c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbe47e4a-a19e-401c-be2b-6f71ecf0f6f0','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbe47e4a-a19e-401c-be2b-6f71ecf0f6f0','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbe47e4a-a19e-401c-be2b-6f71ecf0f6f0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbfd2f88-5127-4a61-9267-5bc383594461','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bbfd2f88-5127-4a61-9267-5bc383594461','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bc28598f-b6d9-4607-ae20-5b46e79be840','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bc2b1ca7-6d32-445a-b8cc-8a8abd88f9ba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bc2b5b9f-d91f-4108-bdff-6f5c2ca6a374','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bc31ac5c-cfd4-43ba-81b4-cea36da712c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bc51a43b-399f-47b9-ab00-bcfb77b6245a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bc5b4124-8554-4540-92d8-59a6405defab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bc7832f9-61a5-4ddb-ad8e-1681121e0aa0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bc81801a-ba11-434e-99c3-812920e6dbd7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bc8a8d1b-000e-48a1-8576-da9340ed59be','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bcc91cf0-4cd9-4a91-8cf1-b0239f80cd7a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bcc91cf0-4cd9-4a91-8cf1-b0239f80cd7a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bccadc9b-5f86-4b5e-9566-a72954ad32c4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bcda99f0-5a78-4b45-97b7-16e54dfcec63','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bce2e387-15cf-4e71-9aa1-3d70ee9f8c9c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bd20f16c-ef1b-4c4e-bad6-e5d3c5a33cd1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bd2c63bb-6639-4f2c-8f2d-11cdcc8e9a7f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bd47c90b-abcf-4514-bea0-332f11c72154','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bd54c178-899f-4ffa-adcd-3837df8d0735','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bd8d784f-7e96-4011-b6ea-22682116b479','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bd9e4723-57b3-428f-a305-de3355bca0de','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bddc027d-5f52-4237-9643-0d0851b5e578','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bddc027d-5f52-4237-9643-0d0851b5e578','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bde6d10c-35ad-4973-90a8-c5840f5a6b74','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bdf02333-81a5-4145-ad79-622d42bc7f2f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bdf02333-81a5-4145-ad79-622d42bc7f2f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('be17df76-bef0-4e55-91c0-7288b4f17513','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('be33d3cd-2f45-4d5e-bf91-962a78c679a0','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('be33d3cd-2f45-4d5e-bf91-962a78c679a0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('be3aa53e-09e6-40c1-a1f9-28d345690ca2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('be983bed-9c70-4291-b743-8e7803938316','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bead8ebb-f29d-4fd0-ae3a-7c7c6ad8e0eb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bedbeb73-6eff-4d8c-9024-e6fecb2703e8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bedf8382-d7d5-4034-bb2f-c3832d257dae','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bedf8382-d7d5-4034-bb2f-c3832d257dae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bee2fee5-cb2a-46af-b140-29ecd06d954e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('beebc2e5-a57d-4fa7-96fb-c1c0caa6971a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('beee88a2-0798-4582-880b-ed34b3c5e688','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bef23795-dc1b-4271-a08b-d44deee08bc4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bf01ce6e-80e6-46b8-8f6d-51ede072b1ad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bf0e7868-c151-4115-be79-ad78ebac5a09','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bf0e7868-c151-4115-be79-ad78ebac5a09','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bf10654e-83d1-4bed-b781-108d8d1d1876','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bf169661-81e3-45fa-8677-4fd70c6f3ec8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bf4cb4c0-a4b7-4af3-8de2-75c26577184d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bf5ffe20-408d-4230-9b7a-6b68ab8e635c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bf60b209-1114-445e-b1a6-81fb62705e1f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bf86cf4f-736e-482d-a187-7f7498a2fe68','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bfa01730-ed7a-4d27-9e64-24edd86c2557','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bfa34b94-470b-4c95-a86b-c961019dcd0d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bfcda3ff-3db2-42a5-acd8-df40e48acb98','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('bfeecf46-af78-4f34-9753-593969a1a6f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c005fa15-babd-465e-8b40-65f63f4a4881','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c0115b22-59c1-433c-819b-d2a184eebe0c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c0115b22-59c1-433c-819b-d2a184eebe0c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c0356dad-6560-4a82-91a7-6ca35184155e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c040a466-62a7-40d5-a8cd-cb5db2a5fa5d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c04b3790-f682-4890-9b79-ded6ce72bd67','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c04cf9da-7dd9-4c06-b9d3-613b1bb29274','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c04dc036-ec03-498f-b832-5e6c91aaeb86','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c050d130-a9fa-4632-a014-f89a9f07dac3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c07f056f-1365-49a6-8358-61bc121efeed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c08d1639-3e78-466f-9253-daee39fee7e1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c08d45d5-22fe-406d-b516-e372c7b30504','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c094c85f-3dda-4bac-a73f-ab14c3c5186f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c0ec3006-946a-43f5-a594-e890542b60e6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c108f944-92c7-4a73-b42f-7db092b559c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c11cc1ce-785b-4321-b2ba-c835ed04ef04','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c11cc1ce-785b-4321-b2ba-c835ed04ef04','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c11da255-f19d-480a-a6f4-87cb4fe14a1f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c12461bc-4abe-4b08-b238-144fbc3d87d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c12b42c5-0a53-4763-864a-270cd70a5138','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1521956-11f6-4b15-afe3-be3e6b062706','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1780bb1-4512-4b7b-bbb4-b4c955441129','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c179a255-509d-4acd-bf95-41f4a14defa0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c18651df-8e11-4236-92ab-3775977ce28b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c186db2c-178c-4082-86d0-fa85c8c49194','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1870551-dc26-4a89-9d3d-ae83c546db84','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c18e1a56-19b2-4530-b85e-76f82edc01d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c193d229-8a1b-47f4-a437-6a23b2da1899','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1943958-07d0-4355-8543-aa7fa62514eb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1a65e0b-faa3-43bb-ac6c-8a7e953156da','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1a7fe28-0e0c-4c5d-ac05-eb28d33dad62','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1ca928e-aaad-491a-9670-4b652ba0d86a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1ce5573-62a7-4b22-bda2-19aca04aa82f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1d54920-8f1f-4d4c-9e7e-8521f8cfc2f7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1ec3ba5-7310-4464-8581-f5bc0fbb11fb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1fc561a-a1e8-4064-9191-e368bdbeb35a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1fcf72b-cf73-4208-bad5-08fe84d9746a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c1ff1e2c-f78a-472a-8730-8fbbe59e2319','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c201bad0-24ec-47bd-8c80-5dcdcbeb9c44','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c21b0fb0-ba9a-4ce4-b1fc-e7936da0fc95','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c22ccfa2-53e4-4470-8b48-82c2fa3fffef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c235309e-5862-41e3-b75d-8bc62e5ed298','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c235309e-5862-41e3-b75d-8bc62e5ed298','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c23b9503-3527-48b6-8d1a-8b09346e0a7e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c25e8e51-fe21-4710-b23b-94fe5dd4ccde','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c26980d3-c2cd-42c0-a98b-ae27057736a7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c26bbba4-3fe4-4b84-89db-ed86d4b83300','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c282b045-dbf7-486d-9f5f-ae4d43883576','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c2878fb9-d330-4dee-8a62-0819df8fad9e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c2878fb9-d330-4dee-8a62-0819df8fad9e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c2ad28e9-0805-4930-9e00-6f9bb2b30e13','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c2ad28e9-0805-4930-9e00-6f9bb2b30e13','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c2c7a638-b386-4698-a8cf-860af3ee1aec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c2d3cf45-7666-478e-831f-ba156dfb94bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c2e490ed-a08e-4da1-ac78-93917111897a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c2fa94ef-d510-4809-bc26-9deed9e5a9e4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c309ffc0-a9cb-409f-8b63-f3abfad4a38a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3300b23-18c5-4b10-8acb-09444440ad1d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c34713ca-3579-49cd-9d49-2f1258b0b528','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3545639-8646-46bd-a149-fc97ea3c7e58','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c39471af-1ec4-4380-b42d-350a537f435e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3a02acd-4ae3-4e92-a283-97c4d7120594','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3a02acd-4ae3-4e92-a283-97c4d7120594','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3a9e7d8-0d9d-4c47-be39-22b2db24e547','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3b8a1a4-8a31-4482-b323-a1b485b520a5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3bd9bdd-6ae0-4267-a388-4781ccb1caca','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3bd9bdd-6ae0-4267-a388-4781ccb1caca','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3bd9bdd-6ae0-4267-a388-4781ccb1caca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3c0eab0-6fb6-46a0-8299-6dbd0dded4d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3c7c521-e3d8-4fb6-9bc1-4096ffdab102','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3cb0f78-8898-44a2-984e-f2f206a73399','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3f5f2c9-1ac8-4dc4-8055-0ad92b230175','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c3fc98a8-6592-4a02-a187-d8c0a57fd649','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c4007a73-8fac-47e7-b391-c379cc31a789','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c41586f1-87d1-44f6-9942-6edf53b0882f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c43778ba-1069-4cde-96b7-f9656d50879f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c43f372e-8275-4e4c-86d1-0c26a22a77e7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c49ce35c-7a72-4028-898c-a73e74a0b693','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c49ce35c-7a72-4028-898c-a73e74a0b693','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c4a52838-8cdf-4397-ae77-19f3fd5c9071','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c4b68bf3-581b-4c78-8cba-41d806918189','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c4b6a05b-acfa-4848-b3dd-afe0483a18c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c4c1bd8c-285d-42ba-b859-35153c2e7fe4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c4c1bd8c-285d-42ba-b859-35153c2e7fe4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c4c1bd8c-285d-42ba-b859-35153c2e7fe4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c4ef22b5-19eb-4b09-98c1-5a54f2177fe0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c50810dd-2431-4841-b665-2b99795e54ff','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c50dfffe-7fcd-430d-afff-959448ee30e9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c517a21c-bc8b-46ac-9ad1-8a31727aeb9b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c524bff1-1103-4d7a-b4af-541df05f93cb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c539e01b-51dd-4847-bb36-e0d58181b02d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c53cf072-ce1d-4a07-9e53-d864ffeafa29','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c53fe477-5afc-4f81-ab14-2e483cad4708','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c547891f-970b-44c9-a459-ea3ea293855a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c54f4051-e61e-4822-85d6-2632f13cad5b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c5511b4c-a92c-4ae1-b6f1-d75b60d31c93','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c579920b-46be-4969-ad14-de835d620858','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c5814b6b-a691-4b63-8cec-4a4626f28eb7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c5957793-d058-44e0-8886-f8f1e9648543','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c5aae401-4359-48e1-9e02-f8bf6de870f1','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c5aae401-4359-48e1-9e02-f8bf6de870f1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c5d326ea-6528-4b4a-b49e-504f0aad477f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c5de6b9b-75a6-4891-a09c-e9167384bdd0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c5e84f1e-09e7-4f12-b431-95266013792d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c5f1638a-e525-451c-a2e5-414d7a19f4c2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c61db879-fe40-48df-a3b3-2544948c0508','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c62b3b56-986e-40e5-8b12-2014e422618f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c66e50b2-7c4c-47bf-afc5-5668ec49b132','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c6995da6-b41e-40e3-b7a8-30479864e234','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c6af269f-e572-4024-8b05-476542fdd4cc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c6af269f-e572-4024-8b05-476542fdd4cc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c6baf767-2659-4662-bb5f-0b5ad36948d2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c6c0e4b7-e47a-4627-ac9b-c14376d5cb37','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c6fcdeea-cd31-43a3-a751-b4fbe6302412','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c720e3ea-4275-4524-8477-a17bd758f8f1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c7394a58-bbd5-4f25-87a5-f1b15d356fc8','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c7394a58-bbd5-4f25-87a5-f1b15d356fc8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c73d5b6a-233e-45c5-903e-7f0ccfc84423','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c73d5b6a-233e-45c5-903e-7f0ccfc84423','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c73ece44-7b02-4374-ae9f-426550fd5b2d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c74ddcc2-98c3-4b15-b434-1cd761bae237','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c7776f69-bbbd-4d39-8749-f4ff0a5e2350','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c79a63d1-edba-4595-bd97-8e86be74727f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c79b08bf-f66f-4256-9022-c6e38b3ca258','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c79b08bf-f66f-4256-9022-c6e38b3ca258','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c7aaae91-9a22-4fac-ab3c-7b9587727170','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c7c61589-ee5a-4e27-9ed9-1a54cc496772','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c7e53bb5-b89f-476e-acee-3292c01237c5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c80008a2-079d-43b1-8128-128234c77b38','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c81368ff-d0a5-4385-b39a-30626d1aabd7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c8313048-63d8-4ded-9a30-e7aed7ea2b48','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c84b73cf-8846-4ff1-8f54-6e533e7a4fdd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c8695bf6-98ea-4055-8b63-30de5c2e055a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c8695bf6-98ea-4055-8b63-30de5c2e055a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c8695bf6-98ea-4055-8b63-30de5c2e055a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c869bfcb-ed1f-4e67-9c8e-ad4a22054b18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c87098da-20b0-4586-8dc4-ef1ab97a38f3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c877e25e-d1c9-490e-b704-3c6beefa4497','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c87ee848-93ce-47a5-91c2-51282e0d31b1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c8994171-69cd-4aa9-8aeb-0eeb53eab8e1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c8a01b1f-cc33-4356-9495-188a2a728f18','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c8bc3985-4731-497a-8ed7-563466873990','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c8c4a4fb-93a2-400f-b8d7-19a1cac89596','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c8c63483-f1ca-45ef-93fa-9d2b3f6fa113','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c8edc265-0089-4c59-87aa-49262d6ffce6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c909eee1-b285-44f1-bb31-f8d39537ec0b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c90b248c-06c3-4e7f-be36-1667963ad3a1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c936d7e0-d9c3-44de-9976-ce4a9c326b62','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c93c521e-60cd-416c-bdb9-352ed9c88dd4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c93c521e-60cd-416c-bdb9-352ed9c88dd4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c94dd719-780c-4a72-80b0-3b7c8697a153','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c95c35e2-8035-4cfb-9400-cede00e2a58d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c9849366-fb44-4790-bfdd-009b3e9530ab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c997bf4e-0a7b-4b2d-bf7e-fe0dd2ecdcab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c9a5b809-4078-4932-ba05-1a77c6cfed0c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c9acef8f-3551-4441-aa3c-223a52ca1348','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c9cc7334-1ad9-47b7-82f3-e6030c4f2374','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c9cdfac1-f70a-4f6c-afce-f4a9f42b531d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c9cdfac1-f70a-4f6c-afce-f4a9f42b531d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c9e8eb24-ff13-481a-80c7-0812e6f501a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c9f9378d-bc9b-4086-a4c6-b4a858a4d143','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('c9f9378d-bc9b-4086-a4c6-b4a858a4d143','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca24b938-293b-43bd-89ac-aa78f994df8c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca365484-e419-46ba-9f56-11006f3a5ef2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca3b5837-ea48-448a-bef9-65a90c4909d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca5e2995-0ea6-4697-844e-95d313bc1f04','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca5e2995-0ea6-4697-844e-95d313bc1f04','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca6c8331-e1ad-47ae-8604-d5ad4c704560','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca73b294-0c3d-48d0-af87-c7e219b724e6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca73b294-0c3d-48d0-af87-c7e219b724e6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca746f40-1a50-4d25-b905-892b9ebedcc2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca74a354-a1bc-499f-bfc2-24682249f5a5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca76327b-5baa-45ef-9c1e-6562502bbde9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca853f45-e0eb-4038-a05a-c7a2f444d773','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ca853f45-e0eb-4038-a05a-c7a2f444d773','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('caa452ea-f805-414e-b535-fdbbe77b3ad0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cab830b1-6215-431d-8a5a-14b2bf9e2ad0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cabdc4e9-d948-40c6-a56a-ea532bac88f2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cae504cc-6bdc-4e6e-a10f-fe169423a584','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cb067832-9a0a-4643-8715-ac833b7f8030','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cb4d8a9a-0a54-4636-b83d-262804475904','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cb507cd3-e082-4e3f-a7aa-f0c94a757391','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cb507cd3-e082-4e3f-a7aa-f0c94a757391','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cb507cd3-e082-4e3f-a7aa-f0c94a757391','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cb59fc06-d8f0-479a-bde7-a812c4c42bb5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cb65bfc5-bc14-44f6-8cdf-3fb0cbdcfa06','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cba550ac-b6f5-42b4-9945-11292eb771b6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cbe3f070-275f-4514-8dbe-824501c66489','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cbe6004b-6805-4d3a-a27b-52262bd652b9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cbe757f8-964c-44c0-8071-559c58f63124','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cbf3066b-eb4c-41d3-9013-a1457c031c6c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cbfd4a51-086f-4392-ae9c-6bd72efdf6c6','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc07043f-49a3-434a-8b9d-b1fddc504977','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc07043f-49a3-434a-8b9d-b1fddc504977','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc07fe33-43c5-4e88-8f26-6a562cc206d6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc1e72cb-ea16-4c9b-a073-a3366fbe3fbe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc34d547-8b3f-4a79-a54c-df386ea323ec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc3def33-c50c-4555-a2bb-cb63480fe293','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc3def33-c50c-4555-a2bb-cb63480fe293','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc6b41e4-6110-4202-91b8-28bb2601da21','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc6fd2cf-e11f-4e8d-a7d2-f3c230295065','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc706236-dfc7-46a2-bcf0-b77893f8ead6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc971fea-7c09-4d50-b9e0-771fdea9c28a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cc971fea-7c09-4d50-b9e0-771fdea9c28a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ccb46d21-e808-4745-8a13-b71cf1850c9e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ccb7e0d4-9742-4642-8000-59d632332f7c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ccc44e99-d652-4d0c-b532-34b77bb893c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ccccd25c-361a-4eba-81c2-d15d01fc5b51','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cce1dc8a-7c5a-443a-a83c-a3b06fd6bb3f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ccee2fa1-a67d-4046-b645-657e35d604f6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cd13f1b3-8df4-45f3-bb36-8f86915dd494','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cd335fdb-6054-4e65-b7b3-15e05d8a35e3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cd3f245c-5909-405d-80e4-51cec184a455','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cd9c5d56-61a4-4349-a153-65838d0a920c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cd9f2a61-051a-4058-8afe-d44f0e823ace','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cdaec979-8130-45a4-9e3e-b8a336781bec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cdbb9dea-df3c-412a-9082-2ec173fb451e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cdcd463e-36c6-46ea-b67d-d32831a1348e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cdcd463e-36c6-46ea-b67d-d32831a1348e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cdecc560-17dd-458b-b186-c270ca054bb2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cdef5b40-5a78-466b-bbe1-7ac5c130529f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cdf7bfe9-6a6b-4506-b5e9-fd3f4cf29aee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cdfe1078-1b14-4021-8225-4c1b3c7ce683','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce0069e9-79a4-4a2b-9b39-263c51dca168','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce0069e9-79a4-4a2b-9b39-263c51dca168','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce0ef0cc-9dc3-4f85-a8fe-b864cd524500','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce0f2622-2b84-4e0b-bfe1-5395d652e41e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce223ed0-733c-415a-9981-7de75e019189','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce3fc24b-5b45-494d-88f0-2d045714c617','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce3fcfcc-7f4e-4e81-a5a9-0024fa0927d4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce536100-3d74-4f3f-bf3b-14d72694bf8d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce56a024-14ec-4624-9593-3a33c684776b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce571c7e-ab06-45c4-8fbf-422bb243fca4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce5beddb-4e5b-4d3f-89a1-d967c235be07','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce5ff27d-4a01-42c5-8b29-a6952f98253d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce62a9b8-b834-4636-b3fa-8460df72f6be','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce70577f-6130-4d92-a979-3d57c7bc23b0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce72b331-9c62-4b1f-819e-015035d65e76','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ce8b1faf-0775-4268-9368-ec094b0882ca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cea769a1-5918-43ab-b42a-41ed968cff94','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cea9cb00-9ef7-496d-9b0e-cffb4f924fba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cec8a9c2-3586-4a44-8330-465616bfef9f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cec9bce0-0464-4f1e-ad2a-f56bc28e1bde','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ceca8fd9-1be0-4914-ab2a-a79abf84a34c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cecbab89-df17-4359-8db4-a9e9bf77cf82','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cecd397c-40f0-4b87-a528-759819bc4ca3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ced91b7f-b9c7-466e-8dde-404cf7d66cd7','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ced91b7f-b9c7-466e-8dde-404cf7d66cd7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf14637e-3a7f-45d4-b154-a79b4959d1c3','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf14637e-3a7f-45d4-b154-a79b4959d1c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf1f13a6-21af-40c7-b78e-0b16bdc98fe0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf20c8c8-d882-48e2-a678-d88b235bdb2e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf34d1c0-564b-46f5-9377-4e86de55a950','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf57a491-1396-4eec-a032-195fbd346cfd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf58ff80-b711-409e-8d3c-834b635266c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf68f3f6-d85a-40d8-915c-1a8a3a32133f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf68f3f6-d85a-40d8-915c-1a8a3a32133f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf76795a-45e1-4129-bc4d-c69165133162','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf79fc8f-da73-4a0e-893a-8d1bc7e71433','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cf8d5b4d-a076-4dde-9a4c-533a33824c10','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cfa1898b-3940-4053-929f-4dbf63e8cb17','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cfa1898b-3940-4053-929f-4dbf63e8cb17','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cfae5da6-f048-46c1-9f2a-ca3d76aec625','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cfc2121a-ebce-4332-af02-3659ab8bb974','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cfdd31ff-394a-4e43-aeb9-8fef9b9c1fa9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cfdfe849-d375-4970-a4f8-896d48211420','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('cfe56adf-d5fa-4036-a1c6-3065f0e0b2ee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0155d5b-46d9-4f82-928f-450177165e93','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0268a22-3431-4815-b492-1f1eafa6f21b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d02e9c17-6cc0-4495-b122-d82ed4cde0b5','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d02e9c17-6cc0-4495-b122-d82ed4cde0b5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d03d5a5f-b379-4121-84d3-8b57def93a1c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d047c763-ddd6-410f-b311-567622d924d9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0568625-856a-4e9d-9a84-5f60bc344329','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0568625-856a-4e9d-9a84-5f60bc344329','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d05b579c-760d-4c27-b215-fb25159f6b5c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d077860c-e0af-47df-959a-0d0ff6818741','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d07a2a48-6c68-40ce-9ffb-245da5966aa3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d09814ea-7835-4a3f-b530-5b84c91ce3c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0a46d78-ba59-4ecc-bf2a-38a93e6ec52c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0aeeb95-4d97-4570-9f74-92b7266b4082','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0bb7622-3a58-47f4-bdb8-104506b54e56','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0bb7622-3a58-47f4-bdb8-104506b54e56','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0cf5721-fdb4-4831-8882-ae2af8705734','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0cf5721-fdb4-4831-8882-ae2af8705734','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0cf9310-da08-43b5-ba26-2658e542ce07','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d0fd535a-9ea6-4565-84c9-105954fb4f41','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d10a8b30-4c50-479a-90be-621e6157d024','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d10a8b30-4c50-479a-90be-621e6157d024','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d10a8b30-4c50-479a-90be-621e6157d024','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d123d52c-2192-42c7-a887-e22b260c3cf4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d15768a7-a624-4ce8-8937-df4ad80e7a63','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d18872a0-553e-4c23-8530-d85998c27dd9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d18872a0-553e-4c23-8530-d85998c27dd9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d199e418-ac11-43fd-b48d-386c3c396e70','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d1b1477c-5d57-4f3b-8b2f-70d791c570e8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d1b35bcb-466c-49b2-8083-d5dcb6a78d96','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d1daa9eb-7c00-4b93-9e4c-c1901d2ae8c3','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d1daa9eb-7c00-4b93-9e4c-c1901d2ae8c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d1dc398f-bb41-46d9-8a8b-a8fd57bb072b','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d1f41a12-283b-488e-b235-b43a605ec6ad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d1f7e77c-ca74-401b-9c61-6558c6d5fc4f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d200c485-9fd7-4987-86dd-fe7969ad3085','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2194ae3-42d7-4781-b187-d10946a06f98','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2196a76-98fa-4aa7-a02d-7b4b95aa1c5b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2225ad7-11b2-4bd3-95b7-ce2f5dfdd4a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d24cee1a-3415-4dc7-9f57-6ba992b4a522','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d256e41b-6cd7-4522-8f1e-4db160d90327','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2682fb5-e1f5-4ff6-8ce8-815afc3afdd8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d269bc0b-0766-4e37-af19-893ba74e4ae7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d281ab14-087e-40cc-ae32-e3efae726858','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d29e0069-c9f8-4d27-825e-d387b560f1e4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2b6d843-4d1d-4f84-8811-1f94eb8b6243','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2bd2f1e-329d-4f24-8229-4220761134ad','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2c0e9fd-b53a-426e-9a25-9f8c3e03f231','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2c0e9fd-b53a-426e-9a25-9f8c3e03f231','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2c0e9fd-b53a-426e-9a25-9f8c3e03f231','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2dfefa5-d39b-4356-b9bd-f048ce36c764','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2ec6587-0169-480f-959b-9aa91ce6eaeb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2f76558-cb85-4f2f-b140-a61791d92ba0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d2ff55e1-7299-4a84-a21a-f8d935621989','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d309fa30-12fd-47ce-9258-19dde727dcc5','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d309fa30-12fd-47ce-9258-19dde727dcc5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d329510d-e9b3-472b-834d-6f24d7398d02','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d32ad70d-5bcc-4b2f-81c1-4e6e202ffcea','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d32ad70d-5bcc-4b2f-81c1-4e6e202ffcea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d33eb45c-b878-4c7f-8659-1c1cb8625570','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d3466b0e-188a-421c-8db6-f177afd88936','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d3760cc4-a2e3-4885-8de8-4b225c12f281','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d376cc2e-1918-4b13-8ccd-aeb57968f85d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d37f9aef-1a22-40a1-94c4-7abc27548399','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d3b4e44f-e53e-404d-8ae5-984a1d793feb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d3c667b5-e2de-4778-9612-c90248edb777','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d3c843a3-42d6-4c7a-92f3-af3cda320750','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d3dcc3d2-1ee9-42c4-9857-820fddf213b3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d3e7815c-0642-4f5a-b46b-c426b62cbeae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d3e8c979-8ecb-444a-bb59-3a74031cc20a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d44ca9f5-5f1a-41b9-8827-a27b52317708','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d47653a4-8d78-41f7-a1bb-b95fba4ab4f7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d47faa7a-da09-4a1a-b9c4-922fbe7e8d61','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d4933bb6-3071-457a-b940-3e51fcd48e02','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d4ccd834-32b5-445f-917e-ee5d35fdc3a5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d4d1e20b-93ed-4a95-8b87-f5be58a9e24d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d4f3963c-ad95-4cb1-bf63-7719607507fc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d5039483-eade-4be2-a46e-f0b1261c9a0e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d5039483-eade-4be2-a46e-f0b1261c9a0e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d505ed6f-787c-4b43-a818-84c7ee2f90bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d5347218-7b77-4b75-9a93-d132948d7174','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d5348600-af3b-442b-a88c-d788fdf460c0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d53d4208-fc48-4c84-9546-0c36d9a0e8c1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d5569709-6220-4b8b-8f8d-54066b4b9543','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d58ace3f-4d2e-486e-8d38-36d219c8b545','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d58ace3f-4d2e-486e-8d38-36d219c8b545','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d59fbb13-cc35-415e-81d6-ac23ccc74b72','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d5da2f4b-f9f3-45eb-8754-fe71cf61be93','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d5e28655-9007-438c-929a-fdb4f83f361d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d6012ed7-9753-44de-a417-e2bdc5cee333','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d60f24d2-b46c-47f5-832b-8f4cb79574eb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d63eae11-6791-4df8-b871-92c375e9a662','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d64298ac-a390-487e-a5bc-b0bb6f4d4b43','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d64298ac-a390-487e-a5bc-b0bb6f4d4b43','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d648a91a-76c8-49b1-8b26-5a936ac0414b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d66af219-db79-4ec4-a1dc-66554973f2a2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d672a6d5-2e14-4973-bed6-4168f657db0d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d67aa90c-8ea6-4f4b-a3fd-765e965299d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d67b1e0f-2e9a-4d3c-9461-fc53a9533dd3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d68d1a0a-ae38-4843-adc5-db9e52bd0b82','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d6af2be1-27d6-488b-9d18-b10e1b22ced6','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d6af2be1-27d6-488b-9d18-b10e1b22ced6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d6bb481b-1394-4a43-9953-054e6b9e9adc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d6d2babd-9876-4373-bef7-f589a1937126','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d6d2e080-2447-408f-8393-2703d6d8f600','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d6ee300b-9a50-4654-830b-e55eae481644','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d6ee300b-9a50-4654-830b-e55eae481644','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d7139883-411a-47f1-b13f-13202c0b656c','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d71b17c8-53a2-4979-8905-2d7acc41b36b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d734ad40-356c-4904-9f13-e71ff22d22e8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d741e878-311a-4f2e-a642-ffeb080cb5b5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d745b294-c4b0-448f-8c08-7ee4da1dd338','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d7b29007-1074-4876-b787-14e48ad1ff18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d7b89fb2-a2d0-489e-a967-095b51d009ef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d7b97215-d9be-48af-bdf9-dded5ec30512','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d7c436e3-f725-4021-b3d4-5346bf9ead79','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d7d327b9-cbff-4584-a747-098dadedf835','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d7f1ffe5-9610-47ac-90ba-fd8e739c9f73','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d81f8466-29e4-46d0-a51e-ba78043ad028','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d820bc58-945a-4aef-b79e-ee0fd882a49f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d82892d5-6738-4c99-aeea-bc43c28b1481','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d82fd793-1265-4583-b0eb-954d2a41559c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d82fd793-1265-4583-b0eb-954d2a41559c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8804b62-feef-419a-944e-8c2e17545ef3','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d88746fc-37b1-45ef-8ac3-c973b18b62c5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8a7d8b1-c984-4d9a-a01d-afe1533de736','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8a7d8b1-c984-4d9a-a01d-afe1533de736','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8ac627e-85b5-445b-8c9d-c5d8797ed276','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8ac627e-85b5-445b-8c9d-c5d8797ed276','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8ac627e-85b5-445b-8c9d-c5d8797ed276','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8b5d3fb-0449-459b-b394-c50fe51ad4ac','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8b8e2a6-68a5-46f6-acc2-31415f3a0c24','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8bd27be-7f89-49f3-8e29-9bba72d9bee3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8bd6476-497b-4fb4-8485-3699a4af87f6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8c6216c-b997-4aae-b348-cab28e3644ef','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d8c6216c-b997-4aae-b348-cab28e3644ef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d91eef30-a778-4ce2-98b9-f220320fff4f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d9201ec6-869a-4050-9d0f-37134c1220db','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d920f73b-c282-4445-86aa-31fa811ae96c','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d9232310-f83f-4c3f-908b-4fcea14c4a1d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d924958f-d846-4605-bf0b-edcc4bdb3f7a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d9326072-ef0e-479e-b37a-29aa19137c49','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d93dc124-6a57-443f-ab1f-84a912919be2','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d93dc124-6a57-443f-ab1f-84a912919be2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d9470631-50e4-4b22-95fd-0f3dcd2f2805','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d9470631-50e4-4b22-95fd-0f3dcd2f2805','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d96b51e5-d7bc-4ebb-9064-fc0072419049','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d977aa39-c2af-456e-b27f-41939e2ae3f8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d9880e0c-2df3-4d1c-a28c-c781433b0603','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d9bec940-6237-4acb-9a25-5a5fd393c68d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d9bec940-6237-4acb-9a25-5a5fd393c68d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('d9d07d99-a210-4cad-8abc-9d5accf6dfbf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('da3a30ad-17b5-4cd6-8c85-75ddb1b39709','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('da42cddc-3883-4192-a338-bdb1241c6de4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('da4612f9-dcc2-446c-a8c5-7d850ca12ea8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('da4aadc3-fc0e-4087-bae5-f2e778d07f94','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('da544d43-3708-4eb7-9078-11bab774c7e4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('da544d43-3708-4eb7-9078-11bab774c7e4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('da6868a1-3b66-4ad6-b105-f0d8a96c5207','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('da7b070a-f085-4bdf-84de-945ddb04ff4c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('da7b070a-f085-4bdf-84de-945ddb04ff4c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('da80ceff-241b-42ff-bbbf-79925c6e699a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('daafc59a-ae66-4ceb-b944-3889093e8dc3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('daca9fbe-423e-48b3-b32c-9d5f19a9062a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dada30c6-5789-485a-9362-7ed9462fe1a9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dae0ea77-ffb4-4d42-9024-ae24b2d97835','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dae3e3a3-4a10-4d3d-9669-4ea9bbd86a7c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('daf997d8-696a-4128-b57b-90212aa8ec0b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('db2ca043-8a87-484d-93d0-0ef101964c7e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('db359bfe-9d9f-4ff5-b14e-7f8898630a34','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('db386cae-f204-4e31-a7e0-61e5fa4061ba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('db916b5a-2c45-4d7d-b03e-ed4c25c7ac6f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('db916b5a-2c45-4d7d-b03e-ed4c25c7ac6f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dbde1bc1-a0cb-4944-b91f-cccc0473ba6b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dbf17d02-e2d7-4b9e-aaa3-48d02fc27c02','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dc1a6391-d696-45a0-b9a3-74eecdbf405a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dc1ef4f2-89e4-4cdb-bbe9-4840ff0abcc5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dc3ac2c1-f44b-4a9b-afb7-0be095ab1598','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dc4bd185-59a4-45ab-8bab-c29faa36d84d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dc758662-7048-41fb-ae25-52e036be0539','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dca6dfeb-9192-4f2e-9849-1db1d96ee8d2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dcba7998-542b-4264-94b4-15efdaf30fca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dcbff4dd-fc17-4ccb-aac8-cc29b69b3cd7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dce4d61c-dac2-43ee-8c2d-efe7b8b7bd49','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd1a6a8a-fc61-4d1a-9317-18348b53dbe7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd2b8502-08d7-471c-8114-5a13c1182629','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd3ba497-6099-4a64-81a3-2a724d0c9237','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd3f7348-039f-45d6-89e0-e8129cfc0e49','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd481f64-4154-4578-9870-0c9e81a943d2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd56078d-dfbe-4ead-a988-e52a16e75c04','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd5c763c-7610-4a3f-9d07-afe04d76e153','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd5c763c-7610-4a3f-9d07-afe04d76e153','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd604a0d-59fd-4bbe-a819-f059c09e9874','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd7191a9-1e44-42b2-afa2-9cab12caf34e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd812705-b16a-4305-9de6-a3b0f59ec3a5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd824413-832e-4d60-b389-90d9728d0a37','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dd83d0a9-5086-4ddf-839d-4bc717a8f75d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ddb87149-9391-408b-8d9d-34d2e474e46b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ddfe44b2-106a-4b0b-b9c7-ce0b3f2238b6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de11c4a9-3d92-45c8-84cf-8e0163b97448','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de145193-6408-4e8f-afce-db78e0770e62','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de17c85c-8103-42c7-80d4-b593fd7f9601','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de459597-9832-402f-85e8-7f749fff4379','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de4f5451-8768-4482-b7b4-93b7a0db16f5','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de4f5451-8768-4482-b7b4-93b7a0db16f5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de68380a-81f1-4635-8243-b3c2eb845622','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de7bf4fb-df54-49b0-8985-81d82cc794d2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de7f2b53-7fed-427f-a80e-6e95069a8895','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de83dad4-0e32-4af0-a0c3-da319f425455','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('de8825e0-ba42-4adb-816b-9f795d2b1ea5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dea7a205-2dd5-408c-9b0e-90a20afa93cf','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dea7a205-2dd5-408c-9b0e-90a20afa93cf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('deaf208f-6259-40de-a7e9-6981d2e99aed','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('deaf208f-6259-40de-a7e9-6981d2e99aed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('deb3c40c-5963-40a4-817f-593aeea8c5ea','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('deb662b5-1632-4baf-acd2-d17ac9e09e73','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('deee036f-9345-4bd3-a1d5-32c61c07aedd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('def0ac5a-cf35-4dec-b8d0-e53a683625f9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('def0ac5a-cf35-4dec-b8d0-e53a683625f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('df21efad-1266-4449-8607-b9afaf110d8a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('df21fe61-054f-44cf-bd02-ed13849609d5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('df64aca1-4400-4894-8630-c1555bd22056','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('df6c8200-711a-498c-b551-88dff375f95a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('df8cbe4f-b8f6-40c4-8a5d-a5127d4bc82b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('df913fe4-6894-45b2-8603-9a5d144c20c4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('df978c7b-03ec-44f5-9826-8ccb467717d4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dfb80a46-2998-47a7-9823-311d72a02b02','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dfd99bc2-1b1c-40ed-8b93-e5eb7ab488f1','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('dfdd8cf4-a881-4274-b2df-f54f2280d76c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0054493-3825-4d1d-9349-2a9f6ec8c7eb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0092d20-5800-4463-af25-043f039455aa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e00f1e3f-2be0-4983-af37-e4beb4f4db60','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e00f1e3f-2be0-4983-af37-e4beb4f4db60','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0280457-8b80-46fa-9136-f5b0522ffebc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e034804b-57d1-424a-ad64-e649b38ee36d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e038ec71-c588-4e83-b14d-b40a6e482a26','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e038ec71-c588-4e83-b14d-b40a6e482a26','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e039e605-ceb8-4f58-829f-3078b484e640','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e03de012-35ca-4c1e-882a-6b8b914c78ed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e043bdb5-2069-4f55-9120-355ca5083b58','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e05c990a-69cd-4855-bc61-f0f926f9a5dd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e074c33d-d637-4ba9-8f35-b621e2beb933','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0789506-6257-470f-a180-eabe64aeb110','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0789506-6257-470f-a180-eabe64aeb110','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0991e64-cff2-445d-8603-d924f86f0ed9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0991e64-cff2-445d-8603-d924f86f0ed9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0ab1585-abb9-46d6-8cd7-f8c99270141b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0ab1585-abb9-46d6-8cd7-f8c99270141b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0c3b618-25ef-40d1-8ea8-1ff907ce887c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0c3b618-25ef-40d1-8ea8-1ff907ce887c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0c746c4-eb6e-428b-913b-bcef1fe64785','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0c9e0cf-8435-48cc-b8ae-08dac19d30ec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0eeff3b-6b4f-43a1-a618-034d8e030701','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0fc10ce-1bbc-4d14-9b8f-052b255e8a2c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e0fc10ce-1bbc-4d14-9b8f-052b255e8a2c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e109d0d1-bb3c-4fa7-b122-767eeeca04f0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e10fe9fc-61b2-4a2e-90e3-fbcc7a1fba5a','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e11629ca-a5d4-43d7-a8eb-ea554f9e2546','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e11b5bdb-3211-4ca0-b634-64d6217843af','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e12f432d-d9b7-41ff-8308-e7519b9d1740','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e12f432d-d9b7-41ff-8308-e7519b9d1740','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e12f6051-ab37-4252-a457-bb8664cc1634','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e13140b5-667c-4c4d-b8a8-a004e2f0a6c0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e1325c90-a08d-43f4-ba5e-0707c594a32f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e156d18d-0413-4568-9c13-f89bcfb05b1b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e156d18d-0413-4568-9c13-f89bcfb05b1b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e18db11a-ccdb-4ab1-b7d4-12bfdf1b38d0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e196efa7-b329-46b2-8042-962bc0b7c21f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e196efa7-b329-46b2-8042-962bc0b7c21f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e1ad4eb9-3a4f-460a-989f-d347b4f66bc6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e1b33ddd-e847-40b0-89a1-ae99413f21f4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e1e71df6-6cdf-41d4-8f21-ef59e829d927','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e1fe445a-48c7-43a1-ad4a-93a0c1250248','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e216a2cf-1dc3-464b-a2ad-46c096aa7b9a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e245ab20-4878-41f3-8045-6ad0ff4125e0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e250451b-fe36-4085-a6a5-72e0ec30200f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e2551a98-1c71-4602-b232-1d352d5b5c37','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e27f39b2-db2e-45d6-997a-e0af8e3fb68c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e2aee875-5d86-4df7-9599-177a6d8284eb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e2c01648-bbe9-4d7b-9ff3-13f081015735','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e2e1ff5e-1c9c-4edc-8d88-3689dd4f707e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e320b7a5-2426-4d35-8cd8-d59b7e0718c3','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e320b7a5-2426-4d35-8cd8-d59b7e0718c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e324dcfd-e50f-4826-84c0-979e28ee4cd5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3339675-4eb5-49a3-8f26-7e05f042e468','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3488abd-a73b-4eee-a6ce-40a30ad412d8','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e34e5bb7-db37-4578-8f2b-723181a06c82','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3586922-cb57-4fbb-a803-2c18fdfd1ed3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e359584e-b698-4f76-ba39-720fd51b1e4a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e35bad75-5c3a-4389-a1dd-fbff53b1d22a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e35c4d71-8356-4120-966f-f81c5ececbec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3694d70-cf4f-4e8c-941e-9757e7c60de4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e37891aa-ff51-4295-b6d4-480d61bb5537','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e37891aa-ff51-4295-b6d4-480d61bb5537','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e383d182-ee5d-4dd7-97f2-99e4820b23f1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3a0810d-df4d-463c-90c5-90895eef1ff1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3a866b0-0c3c-4d6b-b344-5b1bbf2723ec','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3a866b0-0c3c-4d6b-b344-5b1bbf2723ec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3a99a2a-7978-40a8-bd23-dc104528d937','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3b1deb0-a886-4b0f-9b23-b26d5e44aef9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3b6c4db-f97b-4417-87da-42e51b8af4c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3be2c88-11db-4a35-be73-d75f1ee2c486','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3bf28a9-b555-4358-ab21-6b047f5b2694','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3efec9f-bb1b-46d4-a325-721650ffe6c7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e3fd2d0f-a35f-4f9f-9367-93e881a31ef1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e400422c-18d1-4fe8-b11f-431e4bc96e9f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e400422c-18d1-4fe8-b11f-431e4bc96e9f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e42a2a01-fea3-421c-88fb-ae7ae046ccb6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e461306d-b807-42dc-b001-4d03c6fec047','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e4629756-0753-4cae-8ed5-4e42f6c183d7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e47fb9d8-4f02-4c63-9164-506c0cba58e0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e48e113c-d183-4d0f-b95f-5c2379ab08f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e4a1b954-f629-49e8-8809-d197b1638592','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e4a1b954-f629-49e8-8809-d197b1638592','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e4cc9aa2-02e0-450a-b28d-913d97fce18d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e4d007b1-a9d7-4671-8e10-8a1862778ed7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e4de1e2d-1f39-4dd3-bf6d-739eeb11d982','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e4ebf486-24ed-464f-805d-b51a12ac3ec5','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e4ebf486-24ed-464f-805d-b51a12ac3ec5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e4ff6aa4-b0a0-4885-a725-d7a7b531a882','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e5018f71-a477-45b1-9bf2-77a11cad7fe9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e5018f71-a477-45b1-9bf2-77a11cad7fe9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e50299c8-b528-4f99-a6dc-2f901784a9de','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e5136081-fc01-4d46-97a7-e5e3475d78c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e51b9919-a891-4900-ab79-31bcf52fec9f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e548d89a-615e-440f-beb5-a2786ca8ea22','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e556a3e2-01a6-4aad-b1a7-8cbedbe0112d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e55d7ad5-6b7a-451a-9936-73ef07265427','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e55e54b6-fcef-4e70-95e5-4c9d7dd9cff0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e565ce31-64aa-4f34-8d74-4555de342bcf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e5802f53-f30e-4291-9797-258b7cac103e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e5802f53-f30e-4291-9797-258b7cac103e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e5874522-a2a8-442a-b99a-04348e9f55f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e587837c-87e8-4af1-a1d2-33512efe9813','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e5de5746-b1dd-4311-a963-0ef28f899d24','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e5e566b9-921b-439e-8d9a-3a6d9f6f569b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e5f0aa03-d117-49a2-8993-0494af33cd69','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6025382-601e-4264-9db4-90162aa31c71','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e604fdd7-b44b-4817-8c54-4968b552cc50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e615bd77-976c-4783-bda0-145869429cfe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e636b1d4-bcdc-4011-bc4b-36e170e52a3f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e636b1d4-bcdc-4011-bc4b-36e170e52a3f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e63ec974-ac36-4238-b3b4-41161a1ed6ff','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e66c6ebd-0669-4927-b41a-3f8c3ca27108','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e690c1b0-a79b-4101-9f89-bda1ea6b2165','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e692b8f4-0ede-4c06-89c9-67c0ed1ee5d2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6ade427-e742-438c-85af-183314339c41','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6ae189e-8a05-48b9-b85f-737237062f8c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6ae4a62-1897-4fa6-96fd-9ac8c8f3f0ba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6b073cc-e7d4-4c0a-b9ff-e143d5d179d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6b0d382-1376-4b13-bb73-1531df881b31','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6b5a57b-c4fb-4240-8dcc-940a68760edd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6baeb57-ef76-46d4-9a2f-48f777410c95','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6bcbd7f-8797-48b8-96d4-ebd7ca4f49e8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6cbfafe-c96f-4629-baa5-0fea5ec43b94','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6d7c373-08df-4ddd-8b0e-125cf30b8c4b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6e126b5-58a7-4cef-a1b1-4c6be87e8c0f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6f0829c-4ccb-4a20-9167-ba2b551848f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e6ff9d51-70b1-4a04-b51a-3dfb97c61555','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e70575df-114e-495c-853f-23f6bc6a9546','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e7289992-945f-4812-ab48-d98d45fe6e83','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e72b7743-2f8b-42df-a754-4f1986140d49','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e72bacbc-f14f-4a76-86e4-f9697ecd9b53','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e755f60d-90d4-45bc-8a2b-5c9e86b11a20','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e79b3376-fe86-4940-b48c-f34a288126c5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e79ea1fd-2f56-41ba-8934-361f613dce5d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e7adfe3d-37c3-4c9f-a3e1-faf74c36910f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e7b89d71-87a4-4892-9e4b-21170359f29c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e7c55514-24c1-41a5-8994-f2b53b238824','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e7d55afd-26dc-49e1-b836-573ede74468f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e8087709-b16a-48bb-a7b3-7668eddaf40d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e8087709-b16a-48bb-a7b3-7668eddaf40d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e8193d3d-6b16-42b3-9ee2-b13990e2de4b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e8199749-268f-4003-9108-0c8492fe5321','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e8267892-7d52-4bd1-a067-5f9b526bcb6f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e83956b3-0def-4860-b94d-9931f703be48','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e852b24b-798a-4a55-b2ff-3cd517ed6f6a','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e852b24b-798a-4a55-b2ff-3cd517ed6f6a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e855cbd9-c995-43db-ad0d-3cee33218040','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e855cbd9-c995-43db-ad0d-3cee33218040','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e8625a3c-2045-48bf-8fbd-6fc8a6fe3ae1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e864640c-9fb4-411f-8115-c1f40d7a9872','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e86cc9a1-414d-485d-9104-e1b8e1b5059e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e86cc9a1-414d-485d-9104-e1b8e1b5059e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e875cc2c-20f9-4fa0-bfce-e871298ca928','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e87a6a5f-6f26-46a5-b7a8-4ea2e000d18f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e87b9bbf-806d-448d-9f51-ddaf973c6f26','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e87b9bbf-806d-448d-9f51-ddaf973c6f26','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e88c90dc-8d0c-421e-957e-461e1fed3141','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e8a0e89a-7548-4cc2-9a1e-8c780676029e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e8a0e89a-7548-4cc2-9a1e-8c780676029e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e8a18ea3-2cc0-4e83-98c9-737e60300623','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e8dd3248-d2b7-4452-b9a2-31371be060a1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e900d099-5c48-436e-aa16-16f12119b441','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e91b4666-25c9-4170-9ced-59a0cdef8fa8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e929ab42-26ef-4e60-b5d4-8e2b8e93c4e2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e93d667c-5161-433e-87d0-b3f8f337830e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e940a4f2-c23d-453b-8984-788ae93b0e77','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e985f9ec-790d-4b6a-872e-e541b2ec527a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e9923012-d41d-44f2-add2-67c98f12beb8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e9ab2cae-50ca-4bb6-a2c0-68e6e6094ac2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e9b76513-abda-4c8e-a82b-c457b11a0a2a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e9ed3b88-9e64-423f-9940-eab5a7b5327a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('e9fb96e5-57ce-40e9-bdb3-308cdf6a2415','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ea23234f-4589-47e0-9e99-df483e845f90','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ea31e7d0-abe3-47db-bd42-3a3a0447bc84','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ea65e1e5-6bb0-4872-983b-ded2c0295780','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ea6ed435-72f0-47a2-a8e3-8c8f41cae87e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ea8580e6-ed59-44c4-9f60-07ded0b2a464','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eaa8087e-7c1d-4aae-8874-d23e39c6d3ff','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eab28182-d7a9-4fb8-95d9-0ed3a760ca50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eac0ca04-c808-48e9-86db-2bcb64293d11','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ead23fee-55e8-4d2c-8339-e31b4c3a9782','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ead348d0-4bf7-4276-8e39-0ffaf0ba101f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eaf47c30-347e-47af-a672-45eb017f0518','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eaf6abae-a6bf-46e9-a78e-6c9afcd42759','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eaf6abae-a6bf-46e9-a78e-6c9afcd42759','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eb03415a-aeb2-4af2-9a9c-eb19d202f836','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eb0d485d-b6e2-48db-9f18-d2cc9b040f28','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eb222d7b-26b1-4313-8ac1-8c67c412f588','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eb64120f-5d60-43be-b030-163e734bd38d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eb7f12cc-284d-4944-a6e7-f37115d361a2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eb949f93-e4ec-492e-824e-8e9058cfd1ec','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ebb26d63-4a6b-47f7-b0fc-2bd39b6fdfab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ebb37ae4-6691-4bb5-ac21-f4fbab099c09','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ebce79fd-18b6-4f39-b9f8-0544476127ae','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ebce79fd-18b6-4f39-b9f8-0544476127ae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ebe7138b-ced8-43f9-9246-b324b1e42b8e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ebe7138b-ced8-43f9-9246-b324b1e42b8e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ec13f72c-997e-4fb8-b1f3-47f386241eb2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ec24ef8c-bcf2-476e-a15a-6973f515e017','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ec3fc9c6-635d-4d88-97a5-997a05ddc521','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ec5a9b6c-2467-4dda-b4bd-d22055173d40','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ec5a9b6c-2467-4dda-b4bd-d22055173d40','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ec773690-9890-4353-a26f-e743b5d5f03d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ec7b7e57-7df0-44e1-87f0-387cc41086fc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ec7f75d6-0527-4da9-acb7-cd0936c43fab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ec8c8719-c4c4-4512-b556-a11cdf8b5178','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ec9115c8-8be4-447c-8b58-17477eabab50','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ecccbd00-1eca-45bd-96b9-840d214339ba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ecfc7aa7-5f2a-45bf-8407-00fe1d4ee04f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ed100660-d5b2-4d2d-9620-115bec297434','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ed4505fe-3f43-4431-83b5-e8e757d687b1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ed74c5a9-7f43-41d4-9d39-c04aeae406d5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ed868686-d368-410c-a5a8-86d5285b29d6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ed89b223-7a3b-46b0-8bf1-4efa359999c6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('edc375a1-0469-4cf6-8ff1-d40e78cb0418','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('edc7f7b4-e092-4aa3-bd75-81ab424e0282','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('edc7f7b4-e092-4aa3-bd75-81ab424e0282','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eddb3dbf-54fb-4747-b1e3-d7a812aeba94','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee03cb23-7a45-4483-91e4-4e93ed4ec4cc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee09626b-9b17-46df-a2ea-2b780739958f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee41da71-b7eb-4567-b1c8-f3c358e347ef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee4756c6-6125-4db1-8d81-a1bebf686c13','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee4756c6-6125-4db1-8d81-a1bebf686c13','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee55f487-d32f-49d3-b5c2-0955ba3c998c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee5d3ec1-adc7-4b23-9317-b2b2d5860fb2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee619b4c-99e9-4378-b0ea-3e8fc9e074a2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee6509ed-3e6d-458c-a450-1dc1c5c57507','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee6edf48-045d-4e6e-963b-a45a498720a8','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee6edf48-045d-4e6e-963b-a45a498720a8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee77ccec-2b16-4647-a6c0-1226f0be9b01','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee80c071-3281-4ca2-b0aa-ef67e20edb90','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ee80c071-3281-4ca2-b0aa-ef67e20edb90','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eea47b0b-2088-497c-8e1e-f209fa7724b0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eea61344-861d-4880-9b31-228e05da8683','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eec09584-b50e-4ccc-afa7-71b1e92b809f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eecf8c4d-595c-403f-9328-205d217ace03','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eecf9309-3185-46d3-8c5f-9f95f8becfbc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eedc6549-15c9-4f09-8a36-12c92ce4d561','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef04e1b3-774d-4305-b8f0-059a57b6787d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef17e0fc-d81a-450f-87aa-6f1235d004f7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef53db5d-15f3-466e-9365-add6d5d687d8','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef53db5d-15f3-466e-9365-add6d5d687d8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef57b82d-90c1-4859-a907-873fd629ac95','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef63b484-d9be-49e7-8534-7350e1ebc7f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef662b1e-3946-4c39-a618-382226b28df4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef6c9cdc-578a-41d3-808c-f649f8abb97a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef723ab5-931d-4c91-b09f-fb204496021b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef726552-2611-44a6-abf6-9400f84c7ff7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef88e9cd-e9c1-4ebf-8820-4d81f7bdf463','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef9ef10c-d279-4ffc-bdd2-5574c12fe84e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ef9ef10c-d279-4ffc-bdd2-5574c12fe84e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('efba12f0-bca4-4571-a1d3-7d021989f93c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('efcce61b-de21-419e-983c-44af63eecfd5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('efe69137-52d0-4a58-8e11-702300fad59b','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('efe69137-52d0-4a58-8e11-702300fad59b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('efeffcf8-8939-424b-88e5-60ff6bd0070f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eff55753-13a3-4e3e-a2ca-3a6a83052d3e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('eff6ee72-d076-49a2-b26b-4834186cbbc7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f031ebc9-2adf-4449-878d-68eace98cbf2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f07e380f-1880-4156-beef-d2e455f591f5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f098ffbb-084a-4a74-b416-febb86798f2b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f0a34133-29ae-4b32-8748-10742699edce','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f0ab3949-b0f4-4d9d-9c41-bae77690535d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f0ab3949-b0f4-4d9d-9c41-bae77690535d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f0d48312-78d2-4d62-947b-f2273471d1ab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f0dc9d95-6aec-43cc-9e8e-ef3afde831c3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f0ea8430-00bc-4df3-817d-f737d3419042','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f0f0032a-f204-474d-b9b7-ec5fd8227dba','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f0fc8ab3-8b2c-468f-a698-e1d0ac94fd90','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f12bfaf2-fb7a-4e32-9726-843b190ce8ca','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f1407738-4f30-461a-96e5-736ba9d8a010','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f15251da-34aa-4a1a-a37a-0094df652cde','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f16ca797-4afb-4682-b9ab-a212dd955713','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f176788a-dad0-4340-b633-005449bf62f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f1784d70-172a-40ca-a9c9-b7ac22ace9e5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f1a1c6d7-0b3f-440d-8d5d-7b86557bb742','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f1baa0d8-53c3-42e0-900d-7cb0a27137b2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f1c236c0-1cba-4595-b3b7-944e8c41e600','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f1c29b31-f967-49a0-96d4-3169b40c8f87','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f1e6802b-0ec7-4ec1-9b4b-5bb53dbc5dbd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f1e6802b-0ec7-4ec1-9b4b-5bb53dbc5dbd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f1f9cef8-61d1-49da-b012-e37ece5e6c11','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f209255f-e835-4b64-aa61-153d4f9a690e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f20c3242-ea69-41fa-b878-ef30d7f8717e','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f20c3242-ea69-41fa-b878-ef30d7f8717e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f271d3e5-1b95-4081-8b96-c162fcb7e84e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f28b419d-cc90-405e-a82f-7051e45f587f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f2a1bf59-7a52-4072-b6f4-d071ede0edaa','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f2a1bf59-7a52-4072-b6f4-d071ede0edaa','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f2a33028-77bf-4b2d-8eab-d2968e164de9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f2a33028-77bf-4b2d-8eab-d2968e164de9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f2acfbe9-13fb-46a9-b76c-62d29a10506c','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f2b5ab8c-ce58-4482-baaa-959076232f06','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f2b83d64-efa0-4bd4-b7ce-fcc678476139','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f2b8be95-a2de-41c2-ae63-63106ad8e8a6','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f2c2614b-da35-4e09-a05c-de6adcbd5f61','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f3087506-b4af-411d-953b-a77c9f022f1c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f3087506-b4af-411d-953b-a77c9f022f1c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f31e6125-c000-4b89-9285-a7d6432226ae','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f34868ab-d285-48de-bf65-99aacb6f8735','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f34868ab-d285-48de-bf65-99aacb6f8735','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f370afac-f1ca-477e-b3b4-53381519749b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f3920a4d-1d37-45bd-9fcc-d7ad37d86579','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f394047e-8bd2-4964-8619-772d0f829158','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f3aaf381-b830-4e6b-9c99-40676861bfbd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f3abb336-b08a-477f-837a-3d4b83227cd7','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f3dad202-b3eb-4f8d-bfe0-9cd3ccbe351f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f3e95ed8-9af0-44dc-946e-c7ebac99bf71','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f3fcef14-35a5-4634-8fcc-7691df1a3f8a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f3ffaf29-9e86-4a24-9b40-1afb5165566c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f404754c-c62b-4da4-88c9-545990e30bb4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f4244b3d-7362-4c8e-85d7-75c1b92d9b4c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f42af515-6644-4d41-acd5-b931a1d67052','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f42f681f-d859-4a52-bbd4-e5e304abdeb8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f445c072-12ad-41d7-af92-1da8fb3abdc8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f448e628-04f2-416d-835e-2bc5de5adbd8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f4570b28-d0c9-47b3-8d61-6fc08df7e762','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f47654b9-ad7f-47fe-ba75-b8ffb884f971','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f481e282-4f80-4419-b801-21a0bbf52967','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f481e282-4f80-4419-b801-21a0bbf52967','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f492fb0a-1e20-4219-bd79-d398a34ea564','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f4bd2952-7744-4cc8-9379-c4f601ac7205','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f4c0fa71-131e-429f-80ae-9a195eaadb77','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f4c2bf21-37b2-4200-a935-8a82acd1ecab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f4e06470-ee2d-4ddc-b461-b6c4c0045c3f','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f4e06470-ee2d-4ddc-b461-b6c4c0045c3f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f4f2819f-c864-4f68-98e1-bc6b38ff9bee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5011d5c-3ebe-4db6-aecf-f0035a9389b4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f50ea54e-fda8-4c82-aaa5-6a25ed30e858','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f52366b8-e564-4578-a9a4-9c941cdc79db','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f52366b8-e564-4578-a9a4-9c941cdc79db','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f523ee67-a377-4945-ba52-895c26797af0','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f53a70fd-3bfa-4338-b82c-d8d87319886d','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f53a70fd-3bfa-4338-b82c-d8d87319886d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f540f2a8-5dd3-4ab1-a2a1-e621a2cae8ff','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f540f2a8-5dd3-4ab1-a2a1-e621a2cae8ff','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f55e4dcb-0d17-41d0-93dd-876e19173fa4','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f55e4dcb-0d17-41d0-93dd-876e19173fa4','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f55e4dcb-0d17-41d0-93dd-876e19173fa4','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f570de33-68fd-4296-b20f-a5d9953e32c0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f573b940-f027-48b0-9ee5-9fc87e0d38bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f57b848c-5411-4d45-a18b-e9cdd2224c8e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5b1cfc6-8451-4f51-b847-7ce4d7b05142','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5bf8cc1-946a-48e7-b37a-5b5f5fb23523','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5cf176e-5113-468f-94d1-dd87dcf33116','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5d917b6-69c3-4ebf-91da-b04cc44cd0fc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5d917b6-69c3-4ebf-91da-b04cc44cd0fc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5df245d-d8c3-4a67-88fe-498c4b919fcc','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5df245d-d8c3-4a67-88fe-498c4b919fcc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5ea8cd5-5e52-458f-a3d1-5fa774a1ca9e','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5ea8cd5-5e52-458f-a3d1-5fa774a1ca9e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f5ed5db8-890b-4a6d-9038-cd465e96c679','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f600a358-813d-4b71-b576-51f1bd2c2ee8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f605f13a-4869-4d89-abf1-d1a5d5128942','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f6096a6d-99d8-499e-b0bf-c9f8d04a2593','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f61eabda-e819-4fce-94cd-bbf764a82750','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f62989e7-5d17-4889-bc4f-3e6ec8e5abb0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f6501fed-b3fc-41c6-8c7a-aec74ed63e7a','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f66605a0-bee1-430e-bd8e-88fa2ccbf372','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f676276e-6d67-489d-97f0-51af382c6def','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f676276e-6d67-489d-97f0-51af382c6def','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f677db76-9fd4-4f49-801c-125c9f166947','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f68ce57b-0ec9-4b34-8693-4b3598c1c370','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f6a0dfdc-49b4-4985-9c93-1cdf3e0aa979','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f6ba2687-9eee-4011-b55c-73cdcf474499','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f6be2c47-1c52-4a01-a528-72ba027af9cc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f6cbf2a4-1040-4b47-8e2c-8075a66e51fe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f6da9c93-11c0-4e31-b599-0ef75a0e9e80','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f6e40e95-3a70-4db0-a5ec-e3afcf6b3ed7','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f706cfbd-bbef-40d9-8f69-8d5301ce8399','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f7093384-8108-46a6-98ac-b0c7e57aa26e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f7225170-735e-40a8-a50c-5c5acd26748b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f72ac14c-4eab-4ba9-8565-2ae29a595234','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f747f877-54fb-4cfc-bafa-7006f7e64479','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f75006b7-10e9-474d-800a-17f9630da3cb','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f75006b7-10e9-474d-800a-17f9630da3cb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f75cdde3-617b-4ef5-b158-b35bf802d9f5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f761881a-3d4d-4951-91d1-38c72208dffd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f761881a-3d4d-4951-91d1-38c72208dffd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f76c3237-4f89-4be0-bd90-edea5cb14602','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f76dc58e-bf31-4839-9896-03737272b712','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f7e32e15-430f-4307-a800-976b093e2c04','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f7e32e15-430f-4307-a800-976b093e2c04','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f7e73683-c7e5-491d-8e1e-3a2abdb719de','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f804edd4-baf1-4a85-b8fd-d26e9dd0972f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f83c7b07-eb00-48cc-998c-593f37a53052','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f85e8805-7de6-4ba1-9794-eb76509c022b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f880b348-c16e-4d16-8787-7f9804fe8f8e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f8a5b46d-0d02-404f-94cb-068e4ae48433','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f8b536f1-d442-4ca8-902d-6420f47e893e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f8c961ba-65af-4423-bb6b-8a366669809d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f8f82b59-5efc-4209-b31a-414da084cfab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f8f9aa65-7f2c-4779-b542-b3dc06d257db','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9083f61-70c4-4d42-a304-9030d738c03d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9143ea4-7e68-4dfd-bae6-ee007c5b3613','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f914bece-65be-4213-87b0-31fb5ea4bd8c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f92dfcee-40b8-4b78-94f2-7de79d5a8703','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f93f3321-b168-487b-8094-c1166dbe9494','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9659d0d-1893-4acd-8d6a-6da930565f2e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9669fb0-617b-433a-8f69-7739220aa2bc','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f977411f-3bd8-4f7f-9b9f-9d614cc6b130','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f97a8d1f-4863-44c0-8ee8-9af1f00b0cd9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f982a5df-e307-4135-bcb0-e666e6dec37b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f984a094-1e1d-49f5-897e-df364a3ee490','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9922d5d-4bf7-4fce-b5ba-bc1be3e1664b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9ae8195-cc29-4236-8eef-2e8da38f52eb','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9b02514-5400-4421-b107-712d2735d402','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9b1a42f-1ce9-4349-95b3-9d1f8db4cdeb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9cc10e8-941d-453a-a012-f69b837ae3d3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9e71a96-1884-44c7-96dc-b1c9defc4e8d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('f9f2075c-680a-43aa-886e-f94d23bcc8a0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa00eec7-9732-4fe7-bc37-9b1b6bb4ce0d','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa10b092-22df-4575-8600-591e6b45a86d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa36535d-86fd-4215-bce0-c0421c30facd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa3af4a0-23e4-428f-a8dd-ee793b10a918','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa3af4a0-23e4-428f-a8dd-ee793b10a918','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa3af4a0-23e4-428f-a8dd-ee793b10a918','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa50219b-c46c-40ae-8c07-195f95d91d5f','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa50219b-c46c-40ae-8c07-195f95d91d5f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa5dd9ba-f75b-41f8-b953-563faaf1d197','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa5e003d-05ab-4016-af46-e0d608f1d348','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa6189ce-52e6-479d-93e8-fca8252619a8','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa7af60c-3579-436c-b446-f73323ca952f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa84ace0-8671-4292-8aaa-6c7b3aac3741','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa8f5e7d-f309-4bc9-848a-f668d1a6b662','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fa9a3029-f7e6-4ef2-abbf-3552ba3d7060','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('faad8ee4-3f6f-4e6f-8e0e-5ea3b1317ded','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fac5722f-c4e4-4587-a39f-748701c5f2b5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fad262af-f2a8-4dd7-a451-8cfb48c64a87','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fad96845-f36c-4bff-84f0-bdf49641baf5','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fad96845-f36c-4bff-84f0-bdf49641baf5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fb006841-59b9-45df-b9ac-279490862bd3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fb035c07-b752-45a1-9590-32ff38d72163','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fb24fec5-b992-4bbc-a18a-7615b433b006','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fb58176e-4a85-474e-b39a-df50cd321a28','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fb6d9edd-2b06-4cf1-897d-1825abac2e68','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fb7accb6-f41a-47e2-96f2-417973ca9af5','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fb7accb6-f41a-47e2-96f2-417973ca9af5','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fbb94347-6750-402d-af65-a230142c415f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fbcf4a82-cec3-45f8-991e-eb4ac88b54e2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fbf9c996-c677-4893-902a-25f5767bbc18','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc0f3ee6-6a45-4af5-ac47-16f2e7737f27','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc12c422-e714-4bc7-855a-b9e0770239a9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc535c27-aed5-49e5-90f6-6b18b406f714','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc535c27-aed5-49e5-90f6-6b18b406f714','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc637c89-207e-4ed6-9416-c746a6594a01','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc637c89-207e-4ed6-9416-c746a6594a01','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc8630b6-590a-43dd-b642-fd0ecd8639fe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc8cf0b6-7a73-4160-9af2-659e35ff4bdf','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc8cf0b6-7a73-4160-9af2-659e35ff4bdf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc961ae4-c814-468e-93eb-b6f71b9f4edf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fc9955f6-cc6b-4c29-9989-565646b497f2','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fcb331ca-4f21-4edb-922d-a5290de82e61','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fcb5ad8d-eb38-48ce-8ae1-1ce01ec3468c','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fcb5ad8d-eb38-48ce-8ae1-1ce01ec3468c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fccbe986-89da-4710-8732-5f5ac5a9fe7d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fcf169c3-a4eb-4f34-8871-f2658c12fc65','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fcf169c3-a4eb-4f34-8871-f2658c12fc65','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fcf169c3-a4eb-4f34-8871-f2658c12fc65','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd0013cc-0e98-46ba-b8bd-9aa6dbc0c68b','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd1c6080-5037-4445-8250-e4e1e9943587','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd308a9b-1f9c-4f51-a729-835a04765d6c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd46e3e2-debb-4f4f-bcf3-91e4ff70b4bd','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd46e3e2-debb-4f4f-bcf3-91e4ff70b4bd','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd4c5cb7-cb69-4985-b04b-c904045fbab0','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd4c5cb7-cb69-4985-b04b-c904045fbab0','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd4c5cb7-cb69-4985-b04b-c904045fbab0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd544558-c591-4716-a115-c4b4deca0c87','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd6b478f-c875-4d00-b00d-787b69639133','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd6b478f-c875-4d00-b00d-787b69639133','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd6f5e4f-c878-4df3-a28b-fbe24e0c339e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd82dfff-27f9-4543-8880-1eb0020ca5ef','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd82dfff-27f9-4543-8880-1eb0020ca5ef','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fd8e8e4f-d97d-434b-8a13-54e1f6d3da0d','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fda0cf14-1c3f-4a15-91b2-acedbfed0175','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fdab4a8c-c5fb-4757-9d68-15c07afa3700','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fdaf305d-8e70-41b5-9e29-a05cb1d9f416','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fdd9e883-6a8a-4a58-808a-2a3d86cfca35','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fdd9e883-6a8a-4a58-808a-2a3d86cfca35','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fdde0135-b27b-4176-a39a-7e66a6bbfadb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fdec3111-c4f9-4d45-9283-17fef15a2aab','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fe11b3f5-4100-48f6-ade5-cd1ebdb423d1','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fe443e03-890d-4d57-b91d-39ae56cbb5c0','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fe443e03-890d-4d57-b91d-39ae56cbb5c0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fe48ed9e-8c12-4769-9989-1eb9b56d418f','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fe63304a-8450-4d57-96f3-6e8c4496d7d9','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fe63304a-8450-4d57-96f3-6e8c4496d7d9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fe8b7c70-ee8d-4b27-b195-659d11220c66','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fe971a37-fd4d-4865-b148-b77817e54dbf','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('feb0dc38-c4ce-4fb0-9dea-e489219b40ee','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('feb8a015-9f0b-464f-b813-3188c6c142ed','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('febde861-2abe-456f-9e3a-8e3038a9dfa0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fed12ce1-41a8-4ace-96f9-c416a4752ef9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fed4cb5b-f453-4e66-adaf-5f6520c47283','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fed5f14b-416c-43c6-9b2f-c0f97e82c910','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fede0a8f-7b99-4d6b-b035-ecabc7c86e9e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fee2a6c8-43eb-4e21-bca1-2f1dcad949a3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fee62a85-9b7a-48cc-8f91-f29bd6bc7bfe','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fefe495d-806b-48af-8856-18bb37ce9629','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff00bfab-56c5-4b4d-9edd-6126e1efd0f9','kuali.lui.type.activity.offering.discussion')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff00bfab-56c5-4b4d-9edd-6126e1efd0f9','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff13018c-73f1-4ebc-903c-c287f049d276','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff25b7c9-9a35-4311-9000-ae49c984154c','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff6236c0-f38c-40bc-a837-8480dd8d9c57','kuali.lui.type.activity.offering.lab')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff6236c0-f38c-40bc-a837-8480dd8d9c57','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff76ec7f-03e2-4340-82f7-656ced694681','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff7cab08-0a2a-40b1-9253-dd3005a2a699','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff83c30b-8573-4fcc-8a07-9bb1a85921fb','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff89252e-d083-4a1e-8c23-91b7e7ba0f84','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ff8fd7b9-209d-43d4-a5fb-34ec74b15f1e','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ffa3b190-f196-4f38-a909-efa58ad3f2e3','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ffa46991-5ac9-432a-9b41-72f06ba43d32','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('ffb2c247-92ef-49f2-9452-54a75a1b5ef0','kuali.lui.type.activity.offering.lecture')
/
INSERT INTO KSEN_LUI_RELATED_LUI_TYPES (LUI_ID,RELATED_LUI_TYPE)
  VALUES ('fffda8eb-7959-4347-9f5b-fd1befd246e3','kuali.lui.type.activity.offering.lecture')
/
